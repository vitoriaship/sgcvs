@extends('main')

@section('title', 'RDA/CO-RDA | SGCVS - Sistema de Gestão Comercial Vitória Ship')

@section('navbar')

    @section('module')
        <h3 class="white">
            <span class="glyphicon glyphicon-floppy-save" aria-hidden="true">
            </span> Records <span class="font-l">/ R.D.A e CO-R.D.A</span>
        </h3>
    @endsection

    @include('navbar')

@endsection

@section('content')

    <div class="container p-t1">
        @include('messages')
        <div class="panel panel-default">
            <form action="{{ route('rda.store') }}" method="post">
            {!! csrf_field() !!}
                <div class="panel-body p-t7">
                    <div class="row form-horizontal p-t4">
                        <div class="col-sm-6">
                            <div class="form-group row">
                                <label for="name" class="col-sm-3 control-label">
                                    <span class="red">*</span> Name
                                </label>
                                <div class="col-sm-9">
                                    <input name="name" type="text" class="form-control" value="{{ old('name') }}">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="email" class="col-sm-3 control-label">E-mail</label>
                                <div class="col-sm-9">
                                    <input name="email" type="text" class="form-control" value="{{ old('email') }}">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="discount_1" class="col-sm-3 control-label">Discount 1</label>
                                <div class="col-sm-9">
                                    <select name="discount_1" class="form-control">
                                        <option value="0" selected>No discount 0%</option>
                                        @for($i=5; $i <= 30; $i += 5)
                                            <option value="{{ $i }}">{{ $i }}%</option>
                                        @endfor
                                        <option disabled>-----------------------</option>
                                        @for($i=1; $i <= 30; $i++)
                                            <option value="{{ $i }}">{{ $i }}%</option>
                                        @endfor
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group row">
                                <label for="type" class="col-sm-3 control-label">Type</label>
                                <div class="col-sm-9">
                                    <select name="type" class="form-control">
                                        <option value="1">SIGN</option>
                                        <option value="0">COB</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="payment_terms" class="col-sm-3 control-label">Payment Terms</label>
                                <div class="col-sm-9">
                                    <input name="payment_terms" type="number" class="form-control" value="{{ old('payment_terms') }}">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="discount_2" class="col-sm-3 control-label">Discount 2</label>
                                <div class="col-sm-9">
                                    <select name="discount_2" class="form-control">
                                        <option value="0" selected>No discount 0%</option>
                                        @for($i=5; $i <= 30; $i += 5)
                                            <option value="{{ $i }}">{{ $i }}%</option>
                                        @endfor
                                        <option disabled>-----------------------</option>
                                        @for($i=1; $i <= 30; $i++)
                                            <option value="{{ $i }}">{{ $i }}%</option>
                                        @endfor
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2 col-md-offset-10 col-xs-12">
                            <input class="btn btn-primary btn-block loading" type="submit" value="SAVE" >
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <div class="container">
        <div class="table-responsive">
            <table class="table table-bordered table-condensed table-hover data-table">
                <thead>
                    <tr class="active">
                        <th>Name</th>
                        <th>E-mail</th>
                        <th>Type</th>
                        <th>Disc. 1</th>
                        <th>Disc. 2</th>
                        <th>Payment Terms</th>
                        <th class="opcoes">Options</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($rdas as $rda)
                    <tr>
                        <td>{{ $rda->name }}</td>
                        <td>{{ $rda->email }}</td>
                        <td>{{ ($rda->type == 1 ? "SIGN":"COB") }}</td>
                        <td align="center">{{ (float) $rda->discount_1 }}%</td>
                        <td align="center">{{ (float) $rda->discount_2 }}%</td>
                        <td>{{ $rda->payment_terms }}</td>
                        <td>
                            <div class="btn-group btn-group-justified" role="group" aria-label="...">
                                <div class="btn-group btn-group-xs" role="group">
                                    <a href="{{ route('rda.edit', $rda->id) }}" class="btn btn-default iframe2 fancybox.iframe border-right-n">
                                        <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
                                    </a>
                                </div>
                                <div class="btn-group btn-group-xs" role="group">
                                    <a href="{{ route('rda.destroy', $rda->id) }}" class="btn btn-default"
                                        data-token="{{ csrf_token() }}"
                                        data-request=""
                                        data-title="Are you sure?"
                                        data-message="Are you sure that you want delete {{ $rda->name }}?"
                                        data-button-text="Yes, delete it!">
                                        <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
                                    </a>
                                </div>
                            </div>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            <div>Showing {!! $rdas->count() !!} of {!! $rdas->total() !!} results</div>
            {!! $rdas->render() !!}
        </div>
    </div>

@include('footer')

@endsection
