@extends('main')

@section('title', 'Edit RDA/CO-RDA')

@section('content')

<div class="container p-t1">
    <div class="panel panel-default">
        <div class="panel-body">
            <div class="row form-horizontal">
            <form action="{{ route('rda.update', $rda->id) }}" method="post">
                {!! csrf_field() !!}
                <input name="_method" type="hidden" value="PUT">
                <div class="col-sm-5">
                    @include('messages')
                    <div class="col-sm-4">
                        <div class="form-group row">
                            <label for="nome_rda" class="col-sm-4 control-label">
                                <span class= "red">*</span> Name
                            </label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" name="name" value="{{ $rda->name }}" maxlength="45">
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-3 p-t4">
                        <div class="form-group row">
                            <label for="email_rda" class="col-sm-5 control-label">
                                <span class= "red">*</span> E-mail
                            </label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" name="email" value="{{ $rda->email }}">
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-3 p-t4">
                        <div class="form-group row">
                            <label for="tipo_rda" class="col-sm-5 control-label">
                                <span class= "red">*</span> Type
                            </label>
                            <div class="col-sm-7">
                                <select name="type" class="form-control">
                                    <option value="1" {{ ($rda->type == 1 ? "selected":"") }}>SIGN</option>
                                    <option value="0" {{ ($rda->type == 0 ? "selected":"") }}>COB</option>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-3 p-t4">
                        <div class="form-group row">
                            <label for="payment_terms" class="col-sm-5 control-label">
                                Payment Terms
                            </label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" name="payment_terms" value="{{ $rda->payment_terms }}">
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-3 p-t4">
                        <div class="form-group row">
                            <label for="discount_1" class="col-sm-5 control-label">
                                Discount 1
                            </label>
                            <div class="col-sm-7">
                                <select name="discount_1" class="form-control">
                                    <option value="0" selected>No discount 0%</option>
                                    @for($i=5; $i <= 30; $i += 5)
                                        <option value="{{ $i }}">{{ $i }}%</option>
                                    @endfor
                                    <option disabled>-----------------------</option>
                                    @for($i=1; $i <= 30; $i++)
                                        @if($rda->discount_1 == $i)
                                            <option value="{{ $i }}" selected="selected">{{ $i }}%</option>
                                        @else
                                            <option value="{{ $i }}">{{ $i }}%</option>
                                        @endif
                                    @endfor
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-3 p-t4">
                        <div class="form-group row">
                            <label for="discount_2" class="col-sm-5 control-label">
                                Discount 2
                            </label>
                            <div class="col-sm-7">
                                <select name="discount_2" class="form-control">
                                    <option value="0" selected>No discount 0%</option>
                                    @for($i=5; $i <= 30; $i += 5)
                                        <option value="{{ $i }}">{{ $i }}%</option>
                                    @endfor
                                    <option disabled>-----------------------</option>
                                    @for($i=1; $i <= 30; $i++)
                                        @if($rda->discount_2 == $i)
                                            <option value="{{ $i }}" selected="selected">{{ $i }}%</option>
                                        @else
                                            <option value="{{ $i }}">{{ $i }}%</option>
                                        @endif
                                    @endfor
                                </select>
                            </div>
                        </div>
                    </div>


                    <div class="col-sm-2">
                        <input class="btn btn-primary loading" type="submit" value="SAVE" >
                    </div>
                </div>
            </div>
            </form>
        </div>
    </div>

<script type="text/javascript">document.getElementsByTagName('body')[0].className+='nopad'</script>

@endsection
