@extends('main')

@section('title', 'Edit Line-up Date | SGCVS - Sistema de Gestão Comercial Vitória Ship')

@section('content')

    <div class="container p-t1">
        <h3>
            <span class="font-l">Alter Line-up date</span>
        </h3>
        <form action="{{ route('config.lineupPost') }}" method="POST">
            {!! csrf_field() !!}

            @include('messages')



            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="row form-horizontal">
                        <div class="col-sm-4">
                            <div class="form-group row">
                                <label for="eta" class="col-sm-3 control-label">Date</label>
                                <div class="col-sm-9">
                                    <input name="date" type="text" class="form-control date" value="{{ Carbon\Carbon::now()->format('d/m/Y') }}">
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4 p-t4">
                            <div class="form-group row">
                                <label for="etb" class="col-sm-3 control-label">Hour</label>
                                <div class="col-sm-9">
                                    <input name="time" type="time" class="form-control" value="{{ Carbon\Carbon::now()->format('H:i') }}">
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="panel-footer clearfix p-t1">
                    <div class="col-sm-12 p-t4 text-center">

                        <div class="col-sm-2">
                            <input class="btn btn-primary loading" type="submit" value="UPDATE" >
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>

    <script type="text/javascript">document.getElementsByTagName('body')[0].className+='nopad'</script>

@endsection
