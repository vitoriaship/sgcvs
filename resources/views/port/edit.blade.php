@extends('main')

@section('title', 'Edit Agency')

@section('content')

    <div class="container p-t1">
        <div class="panel panel-default">
            <div class="panel-body">
                <div class="row form-horizontal">
                <form action="{{ route('port.update', $port->id) }}" method="post">
                    {!! csrf_field() !!}
                    <input name="_method" type="hidden" value="PUT">
                    <div class="col-sm-4">
                        @include('messages')
                        <div class="form-group row">
                            <label for="nome_port" class="col-sm-4 control-label"><span class= "red">*</span> Name</label>
                            <div class="col-sm-8">
                                <input name="name" type="text" class="form-control" value="{{ $port->name }}" uppercase>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-3 p-t4">
                        <div class="form-group row">
                            <label for="uf" class="col-sm-5 control-label"><span class= "red">*</span> UF</label>
                            <div class="col-sm-7">
                                <select name="state_id" class="form-control">
                                    @foreach($states as $state)
                                        <option value="{{ $state->id }}" {{ ($port->state_id == $state->id ? "selected":"") }}>{{ $state->abbr }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-3 p-t4">
                        <div class="form-group row">
                            <label for="estiva" class="col-sm-2 control-label"><span class= "red">*</span> Stevedore</label>
                            <div class="col-sm-4">
                                <select name="stevedore" class="form-control">
                                    <option value="1" {{ ($port->stevedore == 1 ? "selected":"") }}>YES</option>
                                    <option value="0" {{ ($port->stevedore == 0 ? "selected":"") }}>NO</option>
                                </select>
                            </div>
                            <label for="tem_lancha" class="col-sm-2 control-label p-t5"><span class= "red">*</span> Boat</label>
                            <div class="col-sm-4 ">
                                <select name="boat" class="form-control">
                                    <option value="1" {{ ($port->boat == 1 ? "selected":"") }}>YES</option>
                                    <option value="0" {{ ($port->boat == 0 ? "selected":"") }}>NO</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <input class="btn btn-primary loading" type="submit" value="SAVE" >
                    </div>
                </form>
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript">document.getElementsByTagName('body')[0].className+='nopad'</script>

@endsection
