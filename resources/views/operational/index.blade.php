@extends('main')

@section('title', 'Programação')

@section('navbar')

    @section('module')
        <h3 class="white">
            <span class="glyphicon glyphicon-time" aria-hidden="true"></span>
            <span style="display: inline-block;">Programação</span>
            <div style="font-size:14px; margin-top:10px; display: inline-block;">
                @include('lineup-last-updated')
            </div>

        </h3>
    @endsection

    @include('navbar')

@endsection

@section('content')

    <meta http-equiv="refresh" content="60" />
    <style>
    .bg-sgcvs {
         padding: 0 0 8px 0;
     }
    </style>


    <div class="p-t1" style="padding-top:5px;"></div>
    <div class="container-fluid p-t2">
        <div class="row">
            <div class="table-responsive">
                <table class="table table-bordered table-hover">
                    <thead>
                        <tr class="active">
                            <th>Data de Entrega</th>
                            <th>Invoice</th>
                            <th>NFe</th>
                            <th>Navio</th>
                            <th>Porto</th>
                            <th>Agência</th>
                            <th style="width: 120px;">Ag. Telefone</th>
                            <!--<th>ETA</th>
                            <th>ETB</th>
                            <th>ETS</th>-->
                            <th>Funcionário(s)</th>
                            <th style="width: 7%;">Carro</th>
                            <th>Lembretes</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($invoices as $invoice)
                        <tr @if($invoice->remark->billed) style="background-color: yellow;" @elseif($invoice->remark->has_remarks) class="info" @endif>
                            <td>{{ $invoice->date->supply ? $invoice->date->supply : $invoice->date->estimated }}</td>
                            <td>{{ $invoice->number }}</td>
                            <td>{{ $invoice->nfe }}</td>
                            <td>{{ $invoice->vessel->name }}</td>
                            <td>{{ $invoice->port->name }}</td>
                            <td>{{ $invoice->agency->name }}</td>
                            <td>{{ $invoice->agency->telephone }}</td>
                            <!--<td>{{ $invoice->date->eta }}</td>
                            <td>{{ $invoice->date->etb ? $invoice->date->etb : '' }}</td>
                            <td>{{ $invoice->date->ets ? $invoice->date->ets : '' }}</td>-->
                            <td>
                                <ul class="lista-op">
                                    <li>{{ $invoice->employerOne ? $invoice->employerOne->name : '' }}</li>
                                    @if($invoice->employerTwo)
                                        <li>{{ $invoice->employerTwo->name }}</li>
                                    @endif
                                    @if($invoice->employerThree)
                                        <li>{{ $invoice->employerThree->name }}</li>
                                    @endif
                                </ul>
                            </td>
                            <td>{{ $invoice->vehicle ? $invoice->vehicle->plaque : '' }}</td>
                            <td>
                                <ul class="lista-op">
                                    @if($invoice->remark->nautical_chart) <li>Carta Náutica</li> @endif
                                    @if($invoice->remark->join_request) <li>Solicitação de ingresso</li> @endif
                                    @if($invoice->remark->feedback) <li>Feedback Form/Invoice</li> @endif
                                    @if($invoice->remark->medicines) <li>Medicamentos</li> @endif
                                    @if($invoice->remark->boat_vs) <li>Lancha via VS</li> @endif
                                    @if($invoice->remark->stowage_vs) <li>Estiva via VS</li> @endif
                                    @if($invoice->remark->boat_client) <li>Lancha via agência</li> @endif
                                    @if($invoice->remark->stowage_client) <li>Estiva via agência</li> @endif
                                    @if($invoice->remark->billed) <li>Faturado</li> @endif
                                    @if($invoice->remark->other) <li>{{ $invoice->remark->other }}</li> @endif
                                </ul>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    @if(Auth::user()->group->id == 4)
        <script type="text/javascript">document.getElementsByTagName('body')[0].className+='nopad'</script>
    @endif

@endsection
