@extends('main')

@section('title', 'Report | SGCVS - Sistema de Gestão Comercial Vitória Ship')

@section('navbar')

    @section('module')
        <h3 class="white">
            <span class="glyphicon glyphicon-copy" aria-hidden="true"></span> Report
        </h3>
    @endsection

    @include('navbar')

@endsection

@section('content')

<form action="{{ route('report') }}" method="get">
    <div class="container p-t1">
        <div class="panel panel-default">
            <div class="panel-body p-t7">
                <div class="row form-horizontal p-t4">
                    <div class="col-sm-6">
                        <div class="form-group row">
                            <label for="month" class="col-sm-4 control-label">Month</label>
                            <div class="col-sm-8">
                                <select name="month" class="form-control">
                                    <option value="all" {{ Input::get('month') == "all" ? 'selected':'' }}>All</option>
                                    @foreach(range(1,12) as $index)
                                        <option value="{{$index}}" {{ Input::get('month') == $index ? 'selected':'' }}>
                                            {{ strtoupper(DateTime::createFromFormat('!m', $index)->format('M')) }}
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="year" class="col-sm-4 control-label">Year</label>
                            <div class="col-sm-8">
                                <select name="year" class="form-control">
                                    <option value="all" selected>All</option>
                                    @foreach($years as $y)
                                        <option value="{{ $y->year }}" {{ Input::get('year') == $y->year ? 'selected':''  }}>{{ $y->year }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group row">
                            <label for="rda" class="col-sm-4 control-label">Customer</label>
                            <div class="col-sm-8">
                                <select name="rda" class="form-control">
                                    <option value="all" selected>All</option>
                                    @foreach($rdas as $rda)
                                        <option value="{{ $rda->id }}" {{ Input::get('rda') == $rda->id ? 'selected':''  }}>{{ $rda->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="locale" class="col-sm-4 control-label">Locale</label>
                            <div class="col-sm-8">
                                <select name="locale" class="form-control">
                                    <option value="all" selected>All</option>
                                    @foreach($locales as $l)
                                        <option value="{{ $l->id }}" {{ Input::get('locale') == $l->id ? 'selected':'' }}>{{ $l->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-2 col-md-offset-10 col-xs-12">
                        <input class="btn btn-primary btn-block loading" type="submit" value="Search">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid p-t2">
        <div class="row">
            <div class="table-responsive">
                <table id="report" class="table table-bordered table-condensed table-hover">
                    <thead>
                        <tr class="active">
                            <th>Supply Date</th>
                            <th>Stage</th>
                            <th>Invoice</th>
                            <th>Vessel</th>
                            <th>Port</th>
                            <th>R.D.A</th>
                            <th>CO-R.D.A</th>
                            <th>Total USD</th>
                            <th>Receveid Value</th>
                            <th style="width:8%">Out balance</th>
                            <th>Purch. Value NFe</th>
                            <th>Boat R$</th>
                            <th>Stevedore R$ </th>
                            <th>Freight R$</th>
                            <th>Total R$</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($invoices as $invoice)
                        <tr @if($invoice->value->pending_cost) class="alert alert-success" @endif>
                            <td>{{ $invoice->date->supply }}</td>
                            <td>
                                @if($invoice->stage == 2) Approval Line-up @endif
                                @if($invoice->stage == 3) Billing @endif
                                @if($invoice->stage == 4) Approval Billing @endif
                                @if($invoice->stage == 5) Closed @endif
                            </td>
                            <td>{{ $invoice->number }}</td>
                            <td>{{ $invoice->vessel->name }}</td>
                            <td>{{ $invoice->port->name }}</td>
                            <td>{{ $invoice->rda->name }}</td>
                            <td>{{ $invoice->corda ? $invoice->corda->name : '' }}</td>
                            <td>{{ $invoice->value->total }}</td>
                            <td>{{ $invoice->value->received }}</td>
                            <td>{{ $invoice->value->out_balance }}</td>
                            <td>{{ $invoice->value->purch_value_nfe }}</td>
                            <td>{{ $invoice->value->boat_cost_real }}</td>
                            <td>{{ $invoice->value->stevedore_cost_real }}</td>
                            <td>{{ $invoice->value->freight }}</td>
                            <td>{{ $invoice->value->total_real }}</td>
                        </tr>
                        @endforeach
                    </tbody>
                    <tr class="info">
                        <th colspan="9"></th>
                        <th colspan="3">TOTAL US$</th>
                        <th colspan="3">TOTAL R$</th>
                    </tr>
                    <tfoot>
                        <tr class="info">
                            <th colspan="9"></th>
                            <th colspan="3">00.00</th>
                            <th colspan="3">00.00</th>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
</form>

@include('footer')

@endsection
