<div class="panel panel-primary form-horizontal">
    <div class="panel-heading" data-toggle="collapse" data-parent="#accordion" href="#employers" aria-expanded="false">
      <h4 class="panel-title" aria-label="Click to open">Employers</h4>
    </div>
    <div id="employers" class="panel-collapse collapse" role="tabpanel">
      <div class="panel-body">
          <div class="form-group col-md-6">
              <label class="col-sm-3 control-label">Vehicle</label>
              <div class="col-sm-9">
                  <p class="form-control-static">{{ $invoice->vehicle ? $invoice->vehicle->plaque : '' }}</p>
              </div>
          </div>
          <div class="form-group col-md-6">
              <label class="col-sm-3 control-label">Emp. 1</label>
              <div class="col-sm-9">
                  <p class="form-control-static mb-0">{{ $invoice->employerOne ? $invoice->employerOne->name : '' }}</p>
              </div>
          </div>
          <div class="form-group col-md-6">
              <label class="col-sm-3 control-label">Emp. 2</label>
              <div class="col-sm-9">
                  <p class="form-control-static mb-0">{{ $invoice->employerTwo ? $invoice->employerTwo->name : '' }}</p>
              </div>
          </div>
          <div class="form-group col-md-6">
              <label class="col-sm-3 control-label">Emp. 3</label>
              <div class="col-sm-9">
                  <p class="form-control-static mb-0">{{ $invoice->employerThree ? $invoice->employerThree->name : '' }}</p>
              </div>
          </div>
      </div>
    </div>
</div>
