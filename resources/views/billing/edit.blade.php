@extends('main')

@section('title', 'Edit Billing | SGCVS - Sistema de Gestão Comercial Vitória Ship')

@section('navbar')

    @section('module')
        <h3 class="white"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span> Edit / Billing</h3>
    @endsection

    @include('navbar')

@endsection

@section('content')

    <div class="container m-01" id="app">
    <form action="{{ route('billing.update', $invoice->id) }}" method="post">
        {!! csrf_field() !!}
        <input name="_method" type="hidden" value="PUT">

        @include('messages')

        <div class="panel panel-primary">
            <div class="panel-heading">
                <h3 class="panel-title">Informations</h3>
            </div>
            <div class="panel-body">
                <div class="row form-horizontal p-t4">
                    <div class="col-sm-6">
                        <div class="form-group row">
                            <label for="suply_date" class="col-sm-3 control-label">Supply Date</label>
                            <div class="col-sm-9">
                                <input class="form-control calculateFirstChat" name="supply" value="{{ $invoice->date->supply }}" readonly>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="suply_date" class="col-sm-3 control-label">Hour</label>
                            <div class="col-sm-9">
                                <input class="form-control" value="{{ $invoice->date->hour  }}" readonly>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="number" class="col-sm-3 control-label">Invoice</label>
                            <div class="col-sm-9">
                                <input name="number" type="text" class="form-control" value="{{ $invoice->number }}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="nf" class="col-sm-3 control-label">NFe</label>
                            <div class="col-sm-9">
                                <input name="nfe" type="text" class="form-control" value="{{ $invoice->nfe }}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="groups" class="col-sm-3 control-label"> Groups</label>
                            <div class="col-sm-9">
                                <input name="groups" type="text" class="form-control" value="{{ $invoice->groups }}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="vessel_id" class="col-sm-3 control-label">Vessel</label>
                            <div class="col-sm-9">
                                <select name="vessel_id" class="form-control">
                                    <option disabled>Select...</option>
                                    @foreach($vessels as $vessel)
                                        <option value="{{ $vessel->id }}" {{ $vessel->id == $invoice->vessel_id ? 'selected':'' }}>{{ $vessel->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="port_id" class="col-sm-3 control-label">Port</label>
                            <div class="col-sm-9">
                                <select name="port_id" id="port" class="form-control">
                                    <option disabled>Select...</option>
                                    @foreach($ports as $port)
                                        <option value="{{ $port->id }}" {{ $port->id == $invoice->port_id ? 'selected':'' }}>{{ $port->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="agency_id" class="col-sm-3 control-label">Agency</label>
                            <div class="col-sm-9">
                                <select name="agency_id" class="form-control">
                                    <option disabled>Select...</option>
                                    @foreach($agencies as $agency)
                                        <option value="{{ $agency->id }}" {{ $agency->id  == $invoice->agency_id ? 'selected':'' }}>{{ $agency->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group row">
                            <label for="eta" class="col-sm-3 control-label">ETA</label>
                            <div class="col-sm-9">
                                <input name="eta" type="text" class="form-control date" value="{{ $invoice->date->eta }}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="etb" class="col-sm-3 control-label">ETB</label>
                            <div class="col-sm-9">
                                <input name="etb" type="text" class="form-control date" value="{{ $invoice->date->etb ? $invoice->date->etb : '' }}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="ets" class="col-sm-3 control-label">ETS</label>
                            <div class="col-sm-9">
                                <input name="ets" type="text" class="form-control date" value="{{ $invoice->date->ets ? $invoice->date->ets : '' }}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="rda_id" class="col-sm-3 control-label">R.D.A</label>
                            <div class="col-sm-9">
                                <select name="rda_id" id="rda" class="form-control">
                                    <option disabled>Select...</option>
                                    @foreach($rdas as $rda)
                                        <option value="{{ $rda->id }}" {{ $rda->id  == $invoice->rda_id ? 'selected':'' }}>{{ $rda->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="corda_id" class="col-sm-3 control-label">CO-R.D.A</label>
                            <div class="col-sm-9">
                                <select name="corda_id" id="corda_id" class="form-control">
                                    <option value="">Select...</option>
                                    @foreach($rdas as $rda)
                                        <option value="{{ $rda->id }}" {{ $rda->id  == $invoice->corda_id ? 'selected':'' }}>{{ $rda->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="payment" class="col-sm-3 control-label">Payment terms</label>
                            <div class="col-sm-9">
                                <input name="payment" type="number" class="form-control calculateFirstChat" value="{{ $invoice->date->payment }}">
                            </div>
                        </div>
                        <div class="form-group has-error row">
                            <label for="first_chat" class="col-sm-3 control-label">First chat</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" name="first_chat" value="{{ $invoice->date->first_chat }}" readonly>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h3 class="panel-title">Values</h3>
            </div>
            <div class="panel-body" ng-controller="MainCtrl">
                <div class="row form-horizontal p-t4">
                    <div class="col-sm-6">
                        <div class="form-group row">
                            <label for="gross_1" class="col-sm-3 control-label">Val. Gross 1 (USD)</label>
                            <div class="col-sm-9">
                                <input type="text" name="gross_1" class="form-control money" placeholder="0.00" id="gross_1" value="{{ $invoice->value->gross_1 }}" v-model="gross_1">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="discount_1" class="col-sm-3 control-label">Disc. 1</label>
                            <div class="col-sm-9">
                                <select name="discount_1" id="discount_1" v-model="discount_1" class="form-control">
                                    <option value="1" {{ $invoice->value->percent_1 == 0 ? 'selected':'' }}>No discount</option>
                                    @for($i=5; $i <= 30; $i += 5)
                                        <option value="{{ (1-($i/100)) }}">{{ $i }}%</option>
                                    @endfor
                                    <option disabled>-----------------------</option>
                                    @for($i=1; $i <= 30; $i++)
                                        <option value="{{ (1-($i/100)) }}" {{ $invoice->value->percent_1 == $i ? 'selected':'' }}>{{ $i }}%</option>
                                    @endfor
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="val_discount_1" class="col-sm-3 control-label">Val. Disc. 1 (USD)</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" id="val_discount_1" placeholder="0.00" name="val_discount_1" v-model="val_discount_1 | currency" readonly>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="gross_2" class="col-sm-3 control-label">Val. Gross 2 (USD)</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control money" id="gross_2" placeholder="0.00" name="gross_2" value="{{ $invoice->value->gross_2 }}" v-model="gross_2">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="discount_2" class="col-sm-3 control-label"> Disc. 2</label>
                            <div class="col-sm-9">
                                <select name="discount_2" id="discount_2" v-model="discount_2" class="form-control">
                                    <option value="1" {{ $invoice->value->percent_2 == 0 ? 'selected':'' }}>No discount</option>
                                    @for($i=5; $i <= 30; $i += 5)
                                        <option value="{{ (1-($i/100)) }}">{{ $i }}%</option>
                                    @endfor
                                    <option disabled>-----------------------</option>
                                    @for($i=1; $i <= 30; $i++)
                                        <option value="{{ (1-($i/100)) }}" {{ $invoice->value->percent_2 == $i ? 'selected':'' }}>{{ $i }}%</option>
                                    @endfor
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="val_discount_2" class="col-sm-3 control-label">Val. Disc. 2 (USD)</label>
                            <div class="col-sm-9">
                                <input name="val_discount_2" id="val_discount_2" type="text" placeholder="0.00" class="form-control" readonly v-model="val_discount_2 | currency">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="wo" class="col-sm-3 control-label">USD Val. W/O Disc</label>
                            <div class="col-sm-9">
                                <input name="wo" type="text" class="form-control moneyNegative" id="wo" placeholder="0.00" value="{{ $invoice->value->wo }}" v-model="wo">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="boat_cost" class="col-sm-3 control-label"> Boat Service</label>
                            <div class="col-sm-9">
                                <input name="boat_cost" id="boat_cost" type="text" placeholder="0.00" class="form-control money" value="{{ $invoice->value->boat_cost }}" v-model="boat_cost">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="stevedore_cost" class="col-sm-3 control-label"> Stevedore</label>
                            <div class="col-sm-9">
                                <input name='stevedore_cost' id="stevedore_cost" type="text" placeholder="0.00" class="form-control money" value="{{ $invoice->value->stevedore_cost }}" v-model="stevedore_cost">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="transport" class="col-sm-3 control-label"> Transportation</label>
                            <div class="col-sm-9">
                                <input name="transport" id="transport" type="text" placeholder="0.00" class="form-control money" value="{{ $invoice->value->transport }}" v-model="transport">
                            </div>
                        </div>

                        <?php $mode='edit'; ?>
                        @include('invoice.components.extravalue')
                    </div>
                    <div class="col-sm-6 p-t4">
                        <div class="form-group row">
                            <label for="total" class="col-sm-3 control-label">Total</label>
                            <div class="col-sm-9">
                                <input name="total" id="total" type="text" placeholder="0.00" class="form-control" readonly v-model="total | currency">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="received" class="col-sm-3 control-label">Received value</label>
                            <div class="col-sm-9">
                                <input name="received" id="received" type="text" placeholder="0.00" class="form-control money" value="{{ $invoice->value->received }}" v-model="received">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="out_balance" class="col-sm-3 control-label">Out Balance</label>
                            <div class="col-sm-9">
                                <input name="out_balance" id="out_balance" type="text" placeholder="0.00" class="form-control" readonly v-model="out_balance | currency">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="reference" class="col-sm-3 control-label">R$ Reference</label>
                            <div class="col-sm-9">
                                <input name="reference" id="real" type="text" placeholder="0.00" class="form-control money" value="{{ $invoice->value->reference }}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="received_value_rs" class="col-sm-3 control-label">Received Value R$</label>
                            <div class="col-sm-9">
                                <input name="received_value_rs" id="received_value_rs" type="text" placeholder="0.00" class="form-control money" value="{{ $invoice->value->received_value_rs }}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="commission" class="col-sm-3 control-label">Commission</label>
                            <div class="col-sm-9">
                                <input name="commission" id="commission" type="text" placeholder="0.00" class="form-control money" value="{{ $invoice->value->commission }}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="first_charge" class="col-sm-3 control-label">1ª Charge</label>
                            <div class="col-sm-9">
                                <input name="first_charge" type="text" class="form-control date" value="{{ empty(old('first_charge')) ? $invoice->date->first_charge : old('first_charge') }}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="second_charge" class="col-sm-3 control-label">2ª Charge</label>
                            <div class="col-sm-9">
                                <input name="second_charge" type="text" class="form-control date" value="{{ empty(old('second_charge')) ? $invoice->date->second_charge : old('second_charge') }}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="third_charge" class="col-sm-3 control-label">3ª Charge</label>
                            <div class="col-sm-9">
                                <input name="third_charge" type="text" class="form-control date" value="{{ empty(old('third_charge')) ? $invoice->date->third_charge : old('third_charge') }}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="payment_date" class="col-sm-3 control-label"> Payment date</label>
                            <div class="col-sm-9">
                                <input type="text" name="payment_date" class="form-control date calculateFirstChat" value="{{ $invoice->date->payment_date }}">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="commission_paid" class="col-sm-3 control-label">Commission paid</label>
                            <div class="col-sm-9">
                                <input name="commission_paid" type="hidden" value="0">
                                <input name="commission_paid" type="checkbox" class="form-control" value="1" {{ $invoice->value->commission_paid ? 'checked':'' }}>
                            </div>
                        </div>


                        <div class="form-group row">
                            <label for="payment_remarks" class="col-sm-3 control-label">Payment Remarks</label>
                            <div class="col-sm-9">
                                <textarea name="payment_remarks" class="form-control" cols="30" rows="3">{{ $invoice->value->payment_remarks }}</textarea>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="lawyer" class="col-sm-3 control-label">Lawyer</label>
                            <div class="col-sm-9">
                                <input name="lawyer" type="hidden" value="">
                                <input name="lawyer" type="checkbox" class="form-control" value="1" {{ $invoice->lawyer ? 'checked':'' }}>
                                By checking this option, the commercial manager will follow along with the billing process.
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="panel-footer clearfix p-t1">
                <div class="col-sm-12 p-t4 text-center">
                    <label class="checkbox-inline">
                        <input type="checkbox" name="nautical_chart" value="1" {{ $invoice->remark->nautical_chart ? 'checked':'' }}> Carta Náutica
                    </label>
                    <label class="checkbox-inline">
                        <input type="checkbox" name="join_request" value="1" {{ $invoice->remark->join_request ? 'checked':'' }}> Solicitação de ingresso
                    </label>
                    <label class="checkbox-inline">
                        <input type="checkbox" name="feedback" value="1" {{ $invoice->remark->feedback ? 'checked':'' }}> Feedback Form/Invoice
                    </label>
                    <label class="checkbox-inline">
                        <input type="checkbox" name="medicines" value="1" {{ $invoice->remark->medicines ? 'checked':'' }}> Medicamentos
                    </label>
                    <label class="checkbox-inline">
                        <input type="checkbox" name="boat_vs" value="1" {{ $invoice->remark->boat_vs ? 'checked':'' }}> Lancha via VS
                    </label>
                    <br>
                    <label class="checkbox-inline">
                        <input type="checkbox" name="stowage_vs" value="1" {{ $invoice->remark->stowage_vs ? 'checked':'' }}> Estiva via VS
                    </label>
                    <label class="checkbox-inline">
                        <input type="checkbox" name="boat_client" value="1" {{ $invoice->remark->boat_client ? 'checked':'' }}> Lancha via agência
                    </label>
                    <label class="checkbox-inline">
                        <input type="checkbox" name="stowage_client" value="1" {{ $invoice->remark->stowage_client ? 'checked':'' }}> Estiva via agência
                    </label>
                    <label class="checkbox-inline">
                        <input type="checkbox" name="billed" v-model="billed" {{ $invoice->remark->billed ? 'checked':'' }}> Faturado
                    </label>
                    <label class="checkbox-inline">
                        <input type="checkbox" name="other_remark" v-model="other" {{ $invoice->remark->other ? 'checked':'' }}> Outros
                    </label>
                    <div class="p-t1" v-if="other" transition="expand">
                        <textarea name="other" cols="30" rows="1" class="form-control" style="resize:none" placeholder="Tip your remark">{{ $invoice->remark->other }}</textarea>
                    </div>
                    <div class="p-t1"></div>
                    <textarea name="obs" id="info" cols="30" rows="3" class="form-control" placeholder="Observations">{{ $invoice->obs }}</textarea>
                    <div class="p-t1"></div>
                </div>
            </div>
        </div>
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h3 class="panel-title">Cost Values</h3>
            </div>
            <div class="panel-body">
                <div class="row form-horizontal p-t4">
                    <div class="col-sm-6">
                        <div class="form-group row">
                            <label for="purch_value_nfe" class="col-sm-3 control-label">Purch. Value NFe</label>
                            <div class="col-sm-9">
                                <input name="purch_value_nfe" type="text" placeholder="0.00" class="form-control money" v-model="purch_value_nfe" value="{{ $invoice->value->purch_value_nfe }}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="boat_cost_real" class="col-sm-3 control-label"> Boat Service</label>
                            <div class="col-sm-9">
                                <input name="boat_cost_real" type="text" placeholder="0.00" class="form-control money" v-model="boat_cost_real" value="{{ $invoice->value->boat_cost_real }}">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="stevedore_cost_real" class="col-sm-3 control-label"> Stevedore</label>
                            <div class="col-sm-9">
                                <input name="stevedore_cost_real" type="text" placeholder="0.00" class="form-control money" v-model="stevedore_cost_real" value="{{ $invoice->value->stevedore_cost_real }}">
                            </div>
                        </div>

                        <?php $mode='edit'; ?>
                        @include('invoice.components.cost_extra_value')

                    </div>
                    <div class="col-sm-6 p-t4">
                        <div class="form-group row">
                            <label for="freight" class="col-sm-3 control-label">Freight</label>
                            <div class="col-sm-9">
                                <input name="freight" type="text" class="form-control money" placeholder="0.00" v-model="freight" value="{{ $invoice->value->freight }}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="total_compra" class="col-sm-3 control-label">Total</label>
                            <div class="col-sm-9">
                                <input name="total" id="total" type="text" placeholder="0.00" class="form-control" v-model="total_approval | currency 'R$'" readonly>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-3 control-label">Remarks</label>
                            <div class="col-sm-9">
                                <div class="checkbox">
                                    <label>
                                        <input type="hidden" name="pending_cost_obs" value="">
                                        <input type="hidden" name="pending_cost" value="">

                                        <input name="pending_cost" type="checkbox" v-model="pending_cost"
                                        value="1" {{ $invoice->value->pending_cost ? 'checked' : '' }}>
                                        Pending Costs
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row" v-if="pending_cost" transition="expand">
                            <label for="obs_values" class="col-sm-3 control-label"></label>
                            <div class="col-sm-9">
                                <input name="pending_cost_obs" type="text" class="form-control" value="{{ $invoice->value->pending_cost_obs }}">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        @include('operational-updates')

        @if($invoice->stage == 3 || ($invoice->stage==2 && $invoice->rda->type == 0))
            <div class="panel panel-info">
                <div class="panel-heading">
                    <h3 class="panel-title">Conclusion</h3>
                </div>
                <div class="panel-body">
                    <div class="row form-horizontal p-t4">
                        <div class="col-sm-10">
                            <div class="form-group row">
                                <label for="conclusion_id" class="col-sm-3 control-label">Settled?</label>
                                <div class="col-sm-9">
                                    <select name="conclusion_id" class="form-control">
                                        @foreach($conclusions as $conclusion)
                                            <option value="{{ $conclusion->id }}"
                                                {{ $conclusion->id == $invoice->conclusion->id ? 'selected' : '' }}>
                                                {{ $conclusion->name }}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel-footer clearfix">
                    <div class="col-sm-12  text-center">
                        <strong>SUBTITLE:</strong>
                        <br> NO - Continue on your list for payment settlement.
                        <br> OTHER OPTIONS - out of the collection list and goes for approval by the commercial manager.
                    </div>
                </div>
            </div>
        @endif

        <div class="container">
            <div class="row">
                <div class="col-sm-10"></div>
                <div class="col-sm-2">
                    <input class="btn btn-primary btn-lg loading" type="submit" value="UPDATE">
                </div>
            </div>
        </div>
    </form>
    </div>

@include('footer')

@endsection
