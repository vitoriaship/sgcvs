@extends('main')

@section('title', 'Billing | SGCVS - Sistema de Gestão Comercial Vitória Ship')

@section('navbar')

    @section('module')
        <h3 class="white"><span class="glyphicon glyphicon-usd" aria-hidden="true"></span> Billing</h3>
    @endsection

    @include('navbar')

@endsection

@section('content')

    <div class="container p-t1">
        <div class="panel panel-default">
            <div class="panel-body p-t7">

                <div class="row form-horizontal p-t4">
                    <form action="{{ route('billing.index') }}" method="GET">
                        <div class="col-sm-6">
                            <div class="form-group row">
                                <label for="month" class="col-sm-4 control-label">Date</label>
                                <div class="col-sm-5">
                                    <select name="date" class="form-control">
                                        @foreach($dates as $date)
                                            @if($date->first_chat == \Carbon\Carbon::now()->format('Y-m-d'))
                                                <option value="{{ $date->first_chat }}" selected="selected">{{ \Carbon\Carbon::parse($date->first_chat)->format('d/m/Y') }} ({{ $date->total }})</option>
                                            @elseif($request->date != 'all' and $request->date == $date->first_chat)
                                                <option value="{{ $date->first_chat }}" selected="selected">{{ \Carbon\Carbon::parse($date->first_chat)->format('d/m/Y') }} ({{ $date->total }})</option>
                                            @else
                                                <option value="{{ $date->first_chat }}">{{ \Carbon\Carbon::parse($date->first_chat)->format('d/m/Y') }} ({{ $date->total }})</option>
                                            @endif
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <input class="btn btn-primary btn-block loading" type="submit" value="Search">
                        </div>
                        <div class="col-sm-3">
                            <input class="btn btn-primary btn-block loading" type="button" value="All dates" onclick="window.location='?date=all';">
                        </div>
                    </form>
                </div>

                {{--
                <div class="row form-horizontal p-t4">
                    <div class="col-sm-12">
                        <div class="form-group row">

                            <div class="col-sm-5">
                                <span class="alert alert-success" role="info">
                                    <span class="glyphicon glyphicon-usd" aria-hidden="true"></span>
                                    Pending Costs
                                </span>
                                <span class="alert alert-info" role="info">
                                    <span class="glyphicon glyphicon-flag" aria-hidden="true"></span>
                                    Charge under process
                                </span>
                                <span class="alert alert-info" role="info">
                                    <span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span>
                                    Lawyer action
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
                --}}

            </div>
        </div>
    </div>


    <div class="container-fluid p-t2 m40">
        <div class="row">
            @include('messages')

            <div class="table-responsive">
                <table id="billing" class="table table-bordered table-condensed table-hover data-table">
                    <thead>
                        <tr class="active">
                            <th style="width:1%;">
                                <span class="glyphicon glyphicon-flag"></span>
                            </th>
                            <th style="width:1%;">
                                <span class="glyphicon glyphicon-eye-open"></span>
                            </th>
                            <th style="width:1%;">
                                <span class="glyphicon glyphicon-usd"></span>
                            </th>
                            <th type="date">Supply Date</th>
                            <th>Invoice</th>
                            <th>NFe</th>
                            <th>Vessel</th>
                            <th>Port</th>
                            <th>R.D.A</th>
                            <th>CO-R.D.A</th>
                            <th>Remarks</th>
                            <th>Payment Terms</th>
                            <th>Locale</th>
                            <th type="date">Due Date</th>
                            <th class="opcoes">Options</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($invoices as $invoice)
                        <tr @if($invoice->value->total == 0) class="alert alert-success" @endif
                            @if($invoice->value->pending_cost) class="alert alert-success" @endif>

                            <td class="{{ $invoice->date->first_charge ? 'dropdown' : '' }}">
                                @if($invoice->date->first_charge)
                                    <span class="glyphicon glyphicon-flag"> </span>
                                    <ul class="dropdown-menu p-t8">
                                        <li>F.C {{ $invoice->date->first_charge }}</li>
                                        <li>S.C {{ $invoice->date->second_charge }}</li>
                                        <li>T.C {{ $invoice->date->third_charge }}</li>
                                    </ul>
                                @endif
                            </td>
                            <td>
                                @if($invoice->lawyer)
                                    <span class="glyphicon glyphicon-eye-open"> </span>
                                @endif
                            </td>
                            <td class="{{ $invoice->value->pending_cost ? 'dropdown' : '' }}">
                                @if($invoice->value->pending_cost)
                                    <span class="glyphicon glyphicon-usd"></span>
                                    <ul class="dropdown-menu p-t8">
                                        <li>{{ $invoice->value->pending_cost_obs }}</li>
                                    </ul>
                                @endif
                            </td>
                            <td>{{ $invoice->date->supply }}</td>
                            <td>{{ $invoice->number }}</td>
                            <td>{{ $invoice->nfe }}</td>
                            <td>{{ $invoice->vessel->name }}</td>
                            <td>{{ $invoice->port->name }}</td>
                            <td>{{ $invoice->rda->name }}</td>
                            <td>{{ @$invoice->corda->name}}</td>
                            <td class="dropdown">
                                @if($invoice->value->payment_remarks)
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                        <span class="glyphicon glyphicon-circle-arrow-down" aria-hidden="true"></span> See
                                    </a>
                                    <span class="dropdown-menu p-t8">
                                        {{ $invoice->value->payment_remarks }}
                                    </span>
                                @endif

                            </td>
                            <td>{{ $invoice->date->payment }}</td>
                            <td>{{ $invoice->locale->name }}</td>
                            <td>{{ $invoice->date->first_chat }}</td>
                            <td>
                                <div class="btn-group btn-group-justified" role="group">

                                    <div class="btn-group btn-group-xs" role="group">
                                        <a href="{{ route('billing.dates', Hashids::encode($invoice->id)) }}" class="btn btn-default iframe2 fancybox.iframe">
                                            <span class="glyphicon glyphicon-time" aria-hidden="true"></span>
                                        </a>
                                    </div>


                                    <div class="btn-group btn-group-xs" role="group">
                                        <a href="{{ route('billing.edit', Hashids::encode($invoice->id)) }}" class="btn btn-default border-right-n loading">
                                            <span class="glyphicon glyphicon-pencil" aria-hidden="true">
                                        </a>
                                    </div>

                                    <div class="btn-group btn-group-xs" role="group">
                                        <a href="{{ route('invoice.destroy', Hashids::encode($invoice->id)) }}" class="btn btn-default"
                                            data-token="{{ csrf_token() }}"
                                            data-request=""
                                            data-title="Are you sure?"
                                            data-message="Are you sure that you want delete this Invoice?"
                                            data-button-text="Yes, delete it!">
                                            <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
                                        </a>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>

            <span class="alert alert-success" role="info">
                <span class="glyphicon glyphicon-usd" aria-hidden="true"></span>
                Pending Costs
            </span>
            <span class="alert alert-info" role="info">
                <span class="glyphicon glyphicon-flag" aria-hidden="true"></span>
                Charge under process
            </span>
            <span class="alert alert-info" role="info">
                <span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span>
                Lawyer action
            </span>
        </div>
    </div>

@include('footer')

@endsection
