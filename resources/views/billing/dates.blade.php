@extends('main')

@section('title', 'Edit Dates | SGCVS - Sistema de Gestão Comercial Vitória Ship')

@section('content')

    <div class="container p-t1">
        <h3>
            <span class="font-l">Vessel:</span> {{ $invoice->vessel->name }}
            <span class="font-l">/ Invoice:</span> {{ $invoice->number }}
        </h3>
        <form action="{{ route('billing.datesUpdate', $invoice->id) }}" method="post">
            {!! csrf_field() !!}
            <input name="_method" type="hidden" value="PUT">
            <input name="form" type="hidden" value="dates">

            @include('messages')


            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="row form-horizontal">
                        <div class="col-sm-4">
                            <div class="form-group row">
                                <label for="first_charge" class="col-sm-3 control-label">First Charge</label>
                                <div class="col-sm-9">
                                    <input name="first_charge" type="text" class="form-control date" value="{{ $invoice->date->first_charge }}">
                                </div>
                            </div>
                        </div>

                        <div class="col-sm-4">
                            <div class="form-group row">
                                <label for="second_charge" class="col-sm-3 control-label">Second Charge</label>
                                <div class="col-sm-9">
                                    <input name="second_charge" type="text" class="form-control date" value="{{ $invoice->date->second_charge }}">
                                </div>
                            </div>
                        </div>

                        <div class="col-sm-4">
                            <div class="form-group row">
                                <label for="third_charge" class="col-sm-3 control-label">Third Charge</label>
                                <div class="col-sm-9">
                                    <input name="third_charge" type="text" class="form-control date" value="{{ $invoice->date->third_charge }}">
                                </div>
                            </div>
                        </div>


                    </div>
                </div>
                <div class="panel-footer clearfix p-t1">
                    <div class="col-sm-12 p-t4 text-center">

                        <div class="form-group row">
                            <label for="payment_remarks" class="col-sm-3 control-label">Payment Remarks</label>
                            <div class="col-sm-9">
                                <textarea name="payment_remarks" class="form-control" cols="30" rows="3">{{ $invoice->value->payment_remarks }}</textarea>
                            </div>
                        </div>



                        <div class="p-t1"></div>
                        <div class="col-sm-2">
                            <input class="btn btn-primary loading" type="submit" value="UPDATE" >
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>

    <script type="text/javascript">document.getElementsByTagName('body')[0].className+='nopad'</script>

@endsection
