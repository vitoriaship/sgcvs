@extends('main')

@section('title', 'Edit Vehicle')

@section('content')

<div class="container p-t1">
    <div class="panel panel-default">
        <div class="panel-body">
            <div class="row form-horizontal">
            <form action="{{ route('vehicle.update', $vehicle->id) }}" method="post">
                {!! csrf_field() !!}
                <input name="_method" type="hidden" value="PUT">
                <div class="col-sm-5">
                    @include('messages')
                    <div class="form-group row">
                        <label for="model" class="col-sm-3 control-label">
                            <span class= "red">*</span> Model
                        </label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" name="model" value="{{ $vehicle->model }}" uppercase>
                        </div>
                    </div>
                </div>
                <div class="col-sm-5 p-t4">
                     <div class="form-group row">
                        <label for="plaque" class="col-sm-3 control-label">
                            <span class= "red">*</span> Plaque
                        </label>
                        <div class="col-sm-9">
                            <input type="plaque" class="form-control plaque" name="plaque" value="{{ $vehicle->plaque }}" uppercase>
                        </div>
                    </div>
                </div>
                <div class="col-sm -2">
                    <input class="btn btn-primary p-t6 loading"  type="submit" value="SAVE" >
                </div>
            </form>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">document.getElementsByTagName('body')[0].className+='nopad'</script>
<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
<script src="assets/js/ie10-viewport-bug-workaround.js"></script>

@endsection
