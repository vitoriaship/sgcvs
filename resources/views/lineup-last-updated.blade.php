<div style="color:#fff; margin-top:-10px;">
    @if(isset($lineup->value))
        {{ Route::currentRouteName() == 'operational' ? 'Atualizado em' : 'Updated in' }}
         {{ Carbon\Carbon::parse($lineup->value)->format('d/m/Y H:i') }}
    @endif
    @can('update-lineup-date')
        <a href="{{ route('config.lineup') }}" class="iframe2 fancybox.iframe" style="color:#fff;">
            <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
        </a>
    @endcan
</div>
