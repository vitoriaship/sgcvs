@extends('main')

@section('title', 'Login')

@section('content')
    <div class="container">
        <p class="text-center p-t3"><img src="/assets/images/logotipo.png" alt=""></p>
        <div class="row">
            <div class="col-md-6 col-md-offset-3">
                @include('messages')
            </div>
        </div>
        <div class="row">
            <form class="form-signin" method="POST" action="{{ route('auth.login') }}" accept-charset="UTF-8">
                {!! csrf_field() !!}

                <label for="username" class="sr-only">Username</label>
                <input name="username" id="username" class="form-control" placeholder="Username" value="{{ old('username') }}" autofocus>

                <label for="inputPassword" class="sr-only">Senha</label>
                <input type="password" name="password" id="password" class="form-control" placeholder="Password">

                <div class="checkbox">
                  <label>
                    <input type="checkbox" value="remember-me" name="remember"> Remember me
                  </label>
                </div>

                <button class="btn btn-lg btn-primary btn-block loading" type="submit">Login</button>
                <a href="{{ route('password.email') }}"><small>Forgot password?</small></a>
            </form>
        </div>
    </div>
    <script type="text/javascript">document.getElementsByTagName('body')[0].className+='bg-footer'</script>
@endsection
