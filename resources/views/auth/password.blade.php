@extends('main')

@section('title', 'Passoword Reset')

@section('content')

<div class="container">
<p class="text-center p-t3"><img src="/assets/images/logotipo.png" alt=""></p>
    <div class="row">
        <h4 class="text-center">Provide the e-mail related to your account.</h4>
        <div class="col-md-6 col-md-offset-3">
            @include('messages')
            <form class="form-signin" method="POST" action="{{ route('password.email') }}">
                {!! csrf_field() !!}

                <label for="email" class="sr-only">E-mail</label>
                <input type="email" name="email" id="email" class="form-control" placeholder="E-mail" value="{{ old('email') }}" autofocus>

                <button class="btn btn-lg btn-primary btn-block loading" type="submit">Send Password Reset Link</button>
            </form>
        </div>
    </div>
</div>

@endsection
