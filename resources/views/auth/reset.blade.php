@extends('main')

@section('title', 'Password Reset')

@section('content')
<div class="row">
    <div class="col-md-6 col-md-offset-3">
        <h4 class="text-center">Inform your new password.</h4>
        <form method="POST" action="{{ route('reset') }}">
            @include('messages')

            {!! csrf_field() !!}
            <input type="hidden" name="token" value="{{ $token }}">

            <label for="email" class="sr-only">E-mail</label>
            <input type="email" name="email" class="form-control" placeholder="E-mail" value="{{ old('email') }}" autofocus>

            <label for="password" class="sr-only">Password</label>
            <input type="password" name="password" class="form-control" placeholder="Password">

            <label for="password" class="sr-only">Confirm Password</label>
            <input type="password" name="password_confirmation" class="form-control" placeholder="Confirm Password">

            <button class="btn btn-lg btn-primary btn-block loading" type="submit">Change password</button>
        </form>
    </div>
</div>
@endsection
