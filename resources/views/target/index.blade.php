@extends('main')

@section('title', 'Target | SGCVS - Sistema de Gestão Comercial Vitória Ship')

@section('navbar')

    @section('module')
        <h3 class="white">
            <span class="glyphicon glyphicon-floppy-save" aria-hidden="true">
            </span> Records <span class="font-l">/ TARGET</span>
        </h3>
    @endsection

    @include('navbar')

@endsection

@section('content')


    <div class="container p-t1">
        @include('messages')
        <div class="panel panel-default">
            <form action="{{ route('target.store') }}" method="post">
                {!! csrf_field() !!}
                <div class="panel-body p-t7">
                    <div class="row form-horizontal">


                        <div class="col-sm-4 p-t4">
                            <div class="form-group row">
                                <label for="locale_id" class="col-sm-3 control-label">
                                    <span class="red">*</span> Locale
                                </label>
                                <div class="col-sm-9">
                                    <select name="locale_id" class="form-control">
                                        @foreach($locales as $locale)
                                            <option value="{{ $locale->id }}" {{ old('locale_id') == $locale->id ? 'selected' : '' }}>{{ $locale->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>


                        <div class="col-sm-4">
                            <div class="form-group row">
                                <label for="month" class="col-sm-4 control-label">
                                    <span class= "red">*</span> Month
                                </label>
                                <div class="col-sm-8">
                                    <select name="month" class="form-control">
                                        @foreach(range(1,12) as $index)
                                            <option value="{{$index}}" {{ old('month') == $index ? 'selected':'' }}>
                                                {{ strtoupper(DateTime::createFromFormat('!m', $index)->format('M')) }}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4 p-t4">
                            <div class="form-group row">
                                <label for="year" class="col-sm-3 control-label">
                                    <span class="red">*</span> Year
                                </label>
                                <div class="col-sm-9">
                                    <input type="number" name="year" class="form-control" value="{{ old('year') ?? date('Y')}}" maxlength="4">
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4 p-t4">
                            <div class="form-group row">
                                <label for="value" class="col-sm-3 control-label">
                                    <span class="red">*</span> Value (Dolar)
                                </label>
                                <div class="col-sm-9">
                                    <input type="text" name="value" class="form-control money" placeholder="0.00" value="{{ old('value') }}">
                                </div>
                            </div>
                        </div>



                        <div class="col-sm-2">
                            <input class="btn btn-primary btn-block loading" type="submit" value="SAVE" >
                        </div>
                    </div>

                    <div class="row form-horizontal">



                    </div>
                </div>
            </form>
        </div>
    </div>

    <div class="container">
        <div class="table-responsive">
            <table class="table table-bordered table-condensed table-hover data-table">
                <thead>
                    <tr class="active">
                        <th>Locale</th>
                        <th>Month</th>
                        <th>Year</th>
                        <th>Value</th>
                        <th class="opcoes">Options</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($targets as $target)
                    <tr>
                        <td>{{ $target->locale->name }}</td>
                        <td align="center">
                            {{ strtoupper(DateTime::createFromFormat('!m', $target->month)->format('M')) }}
                        </td>
                        <td align="center">{{ $target->year }}</td>
                        <td align="center">{{ \App\Functions\Number::getBrazillianFormatNumber($target->value) }}</td>
                        <td>
                            <div class="btn-group btn-group-justified" role="group" aria-label="...">
                                <div class="btn-group btn-group-xs" role="group">
                                    <a href="{{ route('target.edit', $target->id) }}" class="btn btn-default iframe2 fancybox.iframe border-right-n">
                                        <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
                                    </a>
                                </div>
                                <div class="btn-group btn-group-xs" role="group">
                                    <a href="{{ route('target.destroy', $target->id) }}" class="btn btn-default"
                                        data-token="{{ csrf_token() }}"
                                        data-request=""
                                        data-title="Are you sure?"
                                        data-message="Are you sure that you want delete {{ $target->name }}?"
                                        data-button-text="Yes, delete it!">
                                        <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
                                    </a>
                                </div>
                            </div>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            <div>Showing {!! $targets->count() !!} of {!! $targets->total() !!} results</div>
            {!! $targets->render() !!}
        </div>
    </div>

@include('footer')

@endsection
