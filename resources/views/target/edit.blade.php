@extends('main')

@section('title', 'Edit TARGET')

@section('content')

<div class="container p-t1">
    @include('messages')

    <div class="panel panel-default">
        <div class="panel-body">
            <div class="row form-horizontal">
                <form action="{{ route('target.update', $target->id) }}" method="post">
                    {!! csrf_field() !!}
                    <input name="_method" type="hidden" value="PUT">
                    {!! csrf_field() !!}



                    <div class="panel-body p-t7">
                        <div class="row form-horizontal">


                            <div class="col-sm-4 p-t4">
                                <div class="form-group row">
                                    <label for="locale_id" class="col-sm-3 control-label">
                                        <span class="red">*</span> Locale
                                    </label>
                                    <div class="col-sm-9">
                                        <select name="locale_id" class="form-control">
                                            @foreach($locales as $locale)
                                                <option value="{{ $locale->id }}"
                                                        {{$target->locale_id == $locale->id ? 'selected' : '' }} >{{ $locale->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>


                            <div class="col-sm-4">
                                <div class="form-group row">
                                    <label for="month" class="col-sm-4 control-label">
                                        <span class= "red">*</span> Month
                                    </label>
                                    <div class="col-sm-8">
                                        <select name="month" class="form-control">
                                            @foreach(range(1,12) as $index)
                                                <option value="{{$index}}" {{ $target->month == $index ? 'selected' : '' }}>
                                                    {{ strtoupper(DateTime::createFromFormat('!m', $index)->format('M')) }}
                                                </option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4 p-t4">
                                <div class="form-group row">
                                    <label for="year" class="col-sm-3 control-label">
                                        <span class="red">*</span> Year
                                    </label>
                                    <div class="col-sm-9">
                                        <input type="number" name="year" class="form-control" value="{{ $target->year }}" maxlength="4">
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4 p-t4">
                                <div class="form-group row">
                                    <label for="value" class="col-sm-3 control-label">
                                        <span class="red">*</span> Value (Dolar)
                                    </label>
                                    <div class="col-sm-9">
                                        <input type="text" name="value" class="form-control money" placeholder="0.00" value="{{ (float) $target->value }}">
                                    </div>
                                </div>
                            </div>



                            <div class="col-sm-2">
                                <input class="btn btn-primary btn-block loading" type="submit" value="SAVE" >
                            </div>
                        </div>

                        <div class="row form-horizontal">



                        </div>
                    </div>
                </form>
        </div>
    </div>

<script type="text/javascript">document.getElementsByTagName('body')[0].className+='nopad'</script>

@endsection
