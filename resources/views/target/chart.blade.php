@extends('main')

@section('title', 'Target Chart | SGCVS - Sistema de Gestão Comercial Vitória Ship')

@section('navbar')

@section('module')
    <h3 class="white">
            <span class="glyphicon glyphicon-floppy-save" aria-hidden="true">
            </span> Target <span class="font-l">/ Chart</span>
    </h3>
@endsection

@include('navbar')

@endsection

@section('content')

    <style>
        canvas{
            -moz-user-select: none;
            -webkit-user-select: none;
            -ms-user-select: none;
        }
    </style>

    <div class="container p-t1">
        <div class="panel panel-default">
            <canvas id="canvas"></canvas>
        </div>
    </div>

    <script src="//cdnjs.cloudflare.com/ajax/libs/numeral.js/2.0.6/numeral.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.6.0/Chart.bundle.min.js"></script>
    <script>
        // load a locale
        numeral.register('locale', 'pt-br', {
            delimiters: {
                thousands: '.',
                decimal: ','
            },
            abbreviations: {
                thousand: 'mil',
                million: 'milhões',
                billion: 'b',
                trillion: 't'
            },
            ordinal : function (number) {
                return 'º';
            },
            currency: {
                symbol: '$ '
            }
        });

        // switch between locales
        numeral.locale('pt-br');

        var config = {
            type: 'line',
            data: {
                //labels: ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"],
                labels: {!! json_encode($labels) !!},
                datasets: [
                    {
                        label: "VITORIA RESULTS",
                        backgroundColor: '#29a329',
                        borderColor: '#29a329',
                        data: {!! json_encode($vitoria_reports) !!},
                        fill: false,
                    },
                    {
                        label: "Vitoria Target",
                        backgroundColor: '#66ff99',
                        borderColor: '#66ff99',
                        fill: false,
                        borderDash: [5, 5], // pontilhado
                        //pointRadius: 15,
                        //pointHoverRadius: 10,
                        data: {!! json_encode($vitoria_targets) !!}
                    },
                    {
                        label: "RIO RESULTS",
                        backgroundColor: '#0059b3',
                        borderColor: '#0059b3',
                        fill: false,
                        data: {!! json_encode($rio_reports) !!},
                    },
                    {
                        label: "Rio Target",
                        backgroundColor: '#66a3ff',
                        borderColor: '#66a3ff',
                        fill: false,
                        borderDash: [5, 5],
                        data: {!! json_encode($rio_targets) !!},
                    }
                ]
            },
            options: {
                responsive: true,
                title:{
                    display:true,
                    text:'Alive Index'
                },

                tooltips: {
                    mode: 'index',
                    intersect: false,
                    callbacks: {
                        label: function(tooltipItem, data) {

                            var titulo = data.datasets[tooltipItem.datasetIndex].label;
                            //var valor = numeral(tooltipItem.yLabel).format('0,0.00'); // com centavos
                            var valor = numeral(tooltipItem.yLabel).format('0,0'); // se, centavos

                            return titulo + ': $ ' + valor;



                            //return tooltipItem.yLabel;
                        },

                    }
                },
                /*hover: {
                    mode: 'nearest',
                    intersect: true
                },*/
                hover: {
                    mode: 'index'
                },

                legend: {
                    position: 'top',
                },

                scales: {
                    xAxes: [{
                        display: true,
                        scaleLabel: {
                            display: true,
                            labelString: 'Month'
                        }
                    }],
                    yAxes: [{
                        display: true,
                        scaleLabel: {
                            display: true,
                            labelString: 'Value'
                        },
                        ticks: {
                            callback: function (value) {
                                return numeral(value).format('$ 0,0')
                            }
                        }
                    }]
                }
            }
        };

        window.onload = function() {
            var ctx = document.getElementById("canvas").getContext("2d");
            window.myLine = new Chart(ctx, config);
        };


    </script>

    @include('footer')

@endsection