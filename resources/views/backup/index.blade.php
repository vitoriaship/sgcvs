@extends('main')

@section('title', 'Backup | SGCVS - Sistema de Gestão Comercial Vitória Ship')

@section('navbar')

    @section('module')
        <h3 class="white"><span class="glyphicon glyphicon-briefcase" aria-hidden="true"></span> Backup</h3>
    @endsection

    @include('navbar')

@endsection

@section('content')

    <div class="container-fluid p-t2 m40">
        @include('messages')
        <div class="row">
            <div class="table-responsive">
                <table class="table table-bordered table-condensed table-hover data-table">
                    <thead>
                        <tr class="active">
                            <th style="width:200px;">Date</th>
                            <th>File</th>
                            <th>Size</th>
                            <th class="opcoes" style="width:100px;">Options</th>
                        </tr>
                    </thead>
                    <tbody>
                        @forelse($files as $file)
                            <tr>
                                <td>{{ $file->date }}</td>
                                <td>{{ $file->name }}</td>
                                <td>{{ $file->size }} KB</td>
                                <td align="center">
                                    <div class="btn-group btn-group-xs" role="group">
                                        <a href="{{ route('backup.download', ['file' => base64_encode($file->path)]) }}" class="btn btn-default"
                                        title="Download file" alt="Download file">
                                            <span class="glyphicon glyphicon-duplicate" aria-hidden="true"></span>
                                        </a>
                                    </div>
                                    <div class="btn-group btn-group-xs" role="group">
                                        <a href="{{ route('backup.mail', ['file' => base64_encode($file->path)]) }}" class="btn btn-default"
                                        title="Send file by e-mail" alt="Send file by e-mail">
                                            <span class="glyphicon glyphicon-envelope" aria-hidden="true"></span>
                                        </a>
                                    </div>
                                </td>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="10">No backup files</td>
                            </tr>
                        @endforelse
                    </tbody>
                </table>
            </div>
        </div>
    </div>

@include('footer')

@endsection
