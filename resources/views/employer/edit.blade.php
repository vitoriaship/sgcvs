@extends('main')

@section('title', 'Edit Employer')

@section('content')

    <div class="container p-t1">
        <div class="panel panel-default">
            <div class="panel-body">
                <div class="row form-horizontal">
                <form action="{{ route('employer.update', $employer->id) }}" method="post">
                    {!! csrf_field() !!}
                    <input name="_method" type="hidden" value="PUT">
                    <div class="col-sm-4">
                        @include('messages')
                        <div class="form-group row">
                            <label for="nome_funcionario" class="col-sm-4 control-label">
                                <span class= "red">*</span> Name
                            </label>
                            <div class="col-sm-8">
                                <input name="name" type="text" class="form-control" value="{{ $employer->name }}">
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-3 p-t4">
                        <div class="form-group row">
                            <label for="cpf_funcionario" class="col-sm-5 control-label">CPF</label>
                            <div class="col-sm-8">
                                <input name="cpf" type="text" class="form-control cpf" value="{{ $employer->cpf }}">
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-3 p-t4">
                        <div class="form-group row">
                            <label for="rg" class="col-sm-5 control-label">RG</label>
                            <div class="col-sm-7">
                                <input name="rg" type="text" class="form-control" value="{{ $employer->rg }}">
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-3 p-t4">
                        <div class="form-group row">
                            <label for="cel_funcionario" class="col-sm-5 control-label">Cellphone</label>
                            <div class="col-sm-7">
                                <input name="cellphone" type="text" class="form-control cell" value="{{ $employer->cellphone }}">
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <input class="btn btn-primary loading" type="submit" value="SAVE" >
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">document.getElementsByTagName('body')[0].className+='nopad'</script>

@endsection
