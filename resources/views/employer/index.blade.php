@extends('main')

@section('title', 'Employer | SGCVS - Sistema de Gestão Comercial Vitória Ship')

@section('navbar')

    @section('module')
        <h3 class="white">
            <span class="glyphicon glyphicon-floppy-save" aria-hidden="true"></span> Records
            <span class="font-l">/ Employers</span>
        </h3>
    @endsection

    @include('navbar')

@endsection

@section('content')

    <div class="container p-t1">
        @include('messages')
        <div class="panel panel-default">
            <form action="{{ route('employer.store') }}" method="post">
            {!! csrf_field() !!}
            <div class="panel-body p-t7">
                <div class="row form-horizontal p-t4">
                    <div class="col-sm-6">
                        <div class="form-group row">
                            <label for="name" class="col-sm-3 control-label">
                                <span class="red">*</span> Name
                            </label>
                            <div class="col-sm-9">
                                <input name="name" type="text" class="form-control" value="{{ old('name') }}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="cpf" class="col-sm-3 control-label">CPF</label>
                            <div class="col-sm-9">
                                <input name="cpf" type="text" class="form-control cpf" value="{{ old('cpf') }}">
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group row">
                            <label for="rg" class="col-sm-3 control-label">RG</label>
                            <div class="col-sm-9">
                                <input name="rg" type="text" class="form-control" value="{{ old('rg') }}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="cellphone" class="col-sm-3 control-label">Cellphone</label>
                            <div class="col-sm-9">
                                <input name="cellphone" type="text" class="form-control cell" data-mask="(99) 99999-9999" value="{{ old('cellphone') }}">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-2 col-md-offset-10 col-xs-12">
                        <input class="btn btn-primary btn-block loading" type="submit" value="SAVE" >
                    </div>
                </div>
            </div>
            </form>
        </div>
    </div>
    <div class="container">
        <div class="table-responsive">
            <table class="table table-bordered table-condensed table-hover data-table">
                <thead>
                    <tr class="active">
                        <th>Name</th>
                        <th>CPF</th>
                        <th>RG</th>
                        <th>Cellphone</th>
                        <th class="opcoes">Options</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($employers as $employer)
                    <tr>
                        <td>{{ $employer->name }}</td>
                        <td>{{ $employer->cpf }}</td>
                        <td>{{ $employer->rg }}</td>
                        <td>{{ $employer->cellphone }}</td>
                        <td>
                            <div class="btn-group btn-group-justified" role="group" aria-label="...">
                                <div class="btn-group btn-group-xs" role="group">
                                    <a href="{{ route('employer.edit', $employer->id) }}" class="btn btn-default iframe fancybox.iframe border-right-n">
                                        <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
                                    </a>
                                </div>
                                <div class="btn-group btn-group-xs" role="group">
                                    <a href="{{ route('employer.destroy', $employer->id) }}" class="btn btn-default"
                                        data-token="{{ csrf_token() }}"
                                        data-request=""
                                        data-title="Are you sure?"
                                        data-message="Are you sure that you want delete this employer?"
                                        data-button-text="Yes, delete it!">
                                        <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
                                    </a>
                                </div>
                            </div>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>

@include('footer')

@endsection
