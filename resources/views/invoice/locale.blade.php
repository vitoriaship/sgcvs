@extends('main')

@section('title', 'Edit Locale | SGCVS - Sistema de Gestão Comercial Vitória Ship')

@section('content')

    <div class="container p-t1">
        <h3>
            <span class="font-l">Vessel:</span> {{ $invoice->vessel->name }}
            <span class="font-l">/ Invoice:</span> {{ $invoice->number }}
        </h3>
        <form action="{{ route('invoice.localeUpdate', $invoice->id) }}" method="POST">
            {!! csrf_field() !!}

            @include('messages')


            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="row form-horizontal">

                        <div class="col-sm-4">
                            <div class="form-group row">
                                <label for="locale_id" class="col-sm-3 control-label">Locale</label>
                                <div class="col-sm-12">
                                    <select name="locale_id">
                                        @foreach($locales as $locale)
                                            @if($locale->id == $invoice->locale_id)
                                                <option value="{{ $locale->id }}" selected="selected">{{ $locale->name }}</option>
                                            @else
                                                <option value="{{ $locale->id }}">{{ $locale->name }}</option>
                                            @endif
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>

                <div class="panel-footer clearfix p-t1">
                    <div class="col-sm-12 p-t4 text-center">
                        <div class="p-t1"></div>
                        <div class="col-sm-2">
                            <input class="btn btn-primary loading" type="submit" value="UPDATE" >
                        </div>
                    </div>
                </div>

            </div>
        </form>
    </div>

    <script type="text/javascript">document.getElementsByTagName('body')[0].className+='nopad'</script>

@endsection
