@extends('main')

@section('title', 'Create New | SGCVS - Sistema de Gestão Comercial Vitória Ship')

@section('navbar')

    @section('module')
        <h3 class="white"><span class="glyphicon glyphicon-plus-sign" aria-hidden="true"></span> New</h3>
    @endsection

    @include('navbar')

@endsection

@section('content')

<div class="container m-01" id="app">
<form action="{{ route('invoice.store') }}" method="post">
    {!! csrf_field() !!}
    @include('messages')
    <div class="panel panel-primary">
        <div class="panel-heading">
            <h3 class="panel-title">Informations</h3>
        </div>
        <div class="panel-body">
            <div class="row form-horizontal p-t4">
                <div class="col-sm-6">
                    <div class="form-group row">
                        <label for="invoice" class="col-sm-3 control-label"><span class="red">*</span> Invoice</label>
                        <div class="col-sm-9">
                            <input type="text" name="number" class="form-control" value="{{ old('number') }}">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="nf" class="col-sm-3 control-label">NFe</label>
                        <div class="col-sm-9">
                            <input type="text" name="nfe" class="form-control" id="nf" value="{{ old('nfe') }}">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="grupos" class="col-sm-3 control-label"><span class="red">*</span> Groups</label>
                        <div class="col-sm-9">
                            <input type="text" name="groups" class="form-control" value="{{ old('groups') }}">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="vessel_id" class="col-sm-3 control-label"><span class="red">*</span> Vessel</label>
                        <div class="col-sm-9">
                            <select name="vessel_id" class="form-control" >
                                <option value="" selected disabled>Select...</option>
                                @foreach($vessels as $vessel)
                                    <option value="{{ $vessel->id }}" {{ $vessel->id == old('vessel_id') ? 'selected':'' }}>{{ $vessel->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="port_id" class="col-sm-3 control-label"><span class="red">*</span> Port</label>
                        <div class="col-sm-9">
                            <select name="port_id" id="port" class="form-control" v-model="port_id" v-on:change="portData" >
                                <option value="" selected disabled>Select...</option>
                                @foreach($ports as $port)
                                    <option value="{{ $port->id }}" {{ $port->id == old('port_id') ? 'selected':'' }}>{{ $port->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="agency_id" class="col-sm-3 control-label"><span class="red">*</span> Agency</label>
                        <div class="col-sm-9">
                            <select name="agency_id" class="form-control" >
                                <option value="" selected disabled>Select...</option>
                                @foreach($agencies as $agency)
                                    <option value="{{ $agency->id }}" {{ $agency->id  == old('agency_id') ? 'selected':'' }}>{{ $agency->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group row">
                        <label for="eta" class="col-sm-3 control-label"><span class="red">*</span> ETA</label>
                        <div class="col-sm-9">
                            <input name="eta" type="text" class="form-control date" value="{{ old('eta') }}" >
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="etb" class="col-sm-3 control-label">ETB</label>
                        <div class="col-sm-9">
                            <input name="etb" type="text" class="form-control date" value="{{ old('etb') }}" >
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="ets" class="col-sm-3 control-label">ETS</label>
                        <div class="col-sm-9">
                            <input name="ets" type="text" class="form-control date" value="{{ old('ets') }}">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="rda_id" class="col-sm-3 control-label"><span class="red">*</span> R.D.A</label>
                        <div class="col-sm-9">
                            <select name="rda_id" id="rda" class="form-control" >
                                <option value="" selected disabled>Select...</option>
                                @foreach($rdas as $rda)
                                    <option value="{{ $rda->id }}" {{ $rda->id  == old('rda_id') ? 'selected':'' }}>{{ $rda->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="corda_id" class="col-sm-3 control-label">CO-R.D.A</label>
                        <div class="col-sm-9">
                            <select name="corda_id" id="corda_id" class="form-control">
                                <option value="" selected>Select...</option>
                                @foreach($rdas as $rda)
                                    <option value="{{ $rda->id }}" {{ $rda->id  == old('corda_id') ? 'selected':'' }}>{{ $rda->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="pterms" class="col-sm-3 control-label"><span class="red">*</span> Payment terms</label>
                        <div class="col-sm-9">
                            <input type="number" name="payment" class="form-control number" value="{{ old('payment') }}" >
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="panel panel-primary" id="values">
        <div class="panel-heading">
            <h3 class="panel-title">Values</h3>
        </div>
        <div class="panel-body">
            <div class="row form-horizontal p-t4">
                <div class="col-sm-6">
                    <div class="form-group row">
                        <label for="gross_1" class="col-sm-3 control-label">Val. Gross 1 (USD)</label>
                        <div class="col-sm-9">
                            <input type="text" name="gross_1" class="form-control money" placeholder="0.00" id="gross_1" value="{{ old('gross_1') }}" v-model="gross_1">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="discount_1" class="col-sm-3 control-label">Disc. 1</label>
                        <div class="col-sm-9">
                            <select name="discount_1" id="discount_1" class="form-control" v-model="discount_1">
                                <option value="1" selected>No discount 0%</option>
                                @for($i=5; $i <= 30; $i += 5)
                                        <option value="{{ (1-($i/100)) }}" {{ old('discount_1') == (1-($i/100)) ? 'selected':'' }}>{{ $i }}%</option>
                                @endfor
                                <option disabled>-----------------------</option>
                                @for($i=1; $i <= 30; $i++)
                                    <option value="{{ (1-($i/100)) }}" {{ old('discount_1') == (1-($i/100)) ? 'selected':'' }}>{{ $i }}%</option>
                                @endfor
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="val_discount_1" class="col-sm-3 control-label">Val. Disc. 1 (USD)</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" id="val_discount_1" placeholder="0.00" name="val_discount_1" readonly v-model="val_discount_1 | currency">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="gross_2" class="col-sm-3 control-label">Val. Gross 2 (USD)</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control money" id="gross_2" placeholder="0.00" name="gross_2" value="{{ old('gross_2') }}" v-model="gross_2">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="discount_2" class="col-sm-3 control-label"> Disc. 2</label>
                        <div class="col-sm-9">
                            <select name="discount_2" id="discount_2" class="form-control"  v-model="discount_2">
                                <option value="1" selected>No discount 0%</option>
                                @for($i=5; $i <= 30; $i += 5)
                                        <option value="{{ (1-($i/100)) }}" {{ old('discount_2') == (1-($i/100)) ? 'selected':'' }}>{{ $i }}%</option>
                                @endfor
                                <option disabled>-----------------------</option>
                                @for($i=1; $i <= 30; $i++)
                                    <option value="{{ (1-($i/100)) }}" {{ old('discount_2') == (1-($i/100)) ? 'selected':'' }}>{{ $i }}%</option>
                                @endfor
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="val_discount_2" class="col-sm-3 control-label">Val. Disc. 2 (USD)</label>
                        <div class="col-sm-9">
                            <input name="val_discount_2" id="val_discount_2" type="text" placeholder="0.00" class="form-control" readonly v-model="val_discount_2 | currency">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="wo" class="col-sm-3 control-label">USD Val. W/O Disc</label>
                        <div class="col-sm-9">
                            <input name="wo" type="text" class="form-control moneyNegative" id="wo" placeholder="0.00" value="{{ empty(old('wo')) ? '' : old('wo') }}" v-model="wo">
                        </div>
                    </div>

                    <?php $mode='create'; ?>
                    @include('invoice.components.extravalue')


                </div>
                <div class="col-sm-6 p-t4">
                    <!-- Esses 2 campos só aparece se o PORTO informado acima, conter a condição SIM em seu cadastro. -->

                    <div class="form-group row">
                        <label for="boat_cost" class="col-sm-3 control-label"> Boat Service</label>
                        <div class="col-sm-9">
                            <input name="boat_cost" id="boat_cost" type="text" placeholder="0.00" class="form-control money" value="{{ empty(old('boat_cost')) ? '' : old('boat_cost') }}" v-model="boat_cost">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="stevedore_cost" class="col-sm-3 control-label"> Stevedore</label>
                        <div class="col-sm-9">
                            <input name='stevedore_cost' id="stevedore_cost" type="text" placeholder="0.00" class="form-control money" value="{{ empty(old('stevedore_cost')) ? '' : old('stevedore_cost') }}" v-model="stevedore_cost">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="transport" class="col-sm-3 control-label"> Transportation</label>
                        <div class="col-sm-9">
                            <input name="transport" id="transport" type="text" placeholder="0.00" class="form-control money" value="{{ empty(old('transport')) ? '' : old('transport') }}" v-model="transport">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="total" class="col-sm-3 control-label">Total</label>
                        <div class="col-sm-9">
                            <input name="total" id="total" type="text" placeholder="0.00" class="form-control" readonly v-model="total | currency">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="received" class="col-sm-3 control-label">Received Value</label>
                        <div class="col-sm-9">
                            <input name="received" id="received" type="text" placeholder="0.00" class="form-control money" value="{{ empty(old('received')) ? '' : old('received') }}" v-model="received">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="out_balance" class="col-sm-3 control-label">Out Balance</label>
                        <div class="col-sm-9">
                            <input name="out_balance" id="out_balance" type="text" placeholder="0.00" class="form-control" readonly v-model="out_balance | currency">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="reference" class="col-sm-3 control-label">R$ Reference</label>
                        <div class="col-sm-9">
                            <input name="reference" id="real" type="text" placeholder="0.00" class="form-control money" value="{{ empty(old('reference')) ? '' : old('reference') }}">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="received_value_rs" class="col-sm-3 control-label">Received Value R$</label>
                        <div class="col-sm-9">
                            <input name="received_value_rs" id="received_value_rs" type="text" placeholder="0.00" class="form-control money" value="{{ empty(old('received_value_rs')) ? '' : old('received_value_rs') }}">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="commission" class="col-sm-3 control-label">Commission</label>
                        <div class="col-sm-9">
                            <input name="commission" id="commission" type="text" placeholder="0.00" class="form-control money" value="{{ empty(old('commission')) ? '' : old('commission') }}">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="panel-footer clearfix p-t1">
            <div class="col-sm-12 p-t4 text-center">
                <label class="checkbox-inline">
                    <input type="checkbox" name="nautical_chart" value="1" {{ old('nautical_chart') ? 'checked':'' }}> Carta Náutica
                </label>
                <label class="checkbox-inline">
                    <input type="checkbox" name="join_request" value="1" {{ old('join_request') ? 'checked':'' }}> Solicitação de ingresso
                </label>
                <label class="checkbox-inline">
                    <input type="checkbox" name="feedback" value="1" {{ old('feedback') ? 'checked':'' }}> Feedback Form/Invoice
                </label>
                <label class="checkbox-inline">
                    <input type="checkbox" name="medicines" value="1" {{ old('medicines') ? 'checked':'' }}> Medicamentos
                </label>
                <label class="checkbox-inline">
                    <input type="checkbox" name="boat_vs" value="1" {{ old('boat_vs') ? 'checked':'' }}> Lancha via VS
                </label>
                <br>
                <label class="checkbox-inline">
                    <input type="checkbox" name="stowage_vs" value="1" {{ old('stowage_vs') ? 'checked':'' }}> Estiva via VS
                </label>
                <label class="checkbox-inline">
                    <input type="checkbox" name="boat_client" value="1" {{ old('boat_client') ? 'checked':'' }}> Lancha via agência
                </label>
                <label class="checkbox-inline">
                    <input type="checkbox" name="stowage_client" value="1" {{ old('stowage_client') ? 'checked':'' }}> Estiva via agência
                </label>
                <label class="checkbox-inline">
                    <input type="checkbox" name="other_remark" v-model="other" {{ old('other_remark') ? 'checked':'' }}> Outros
                </label>
                <div class="p-t1" v-if="other" transition="expand">
                    <textarea name="other" id="other-text" cols="30" rows="1" class="form-control" style="resize:none" placeholder="Tip your remark">{{ old('other') }}</textarea>
                </div>
                <div class="p-t1"></div>
                <textarea name="obs" id="info" cols="30" rows="3" class="form-control" placeholder="Observations">{{ old('obs') }}</textarea>
                <div class="p-t1"></div>
            </div>
            <div class="col-sm-offset-10 col-sm-2 col-xs-12">
                <input class="btn btn-primary btn-lg btn-block loading" type="submit" value="SAVE" >
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <span class="alert alert-danger" role="alert">* Require fields</span>
        </div>
    </div>
</form>
</div>

@include('footer')

@endsection
