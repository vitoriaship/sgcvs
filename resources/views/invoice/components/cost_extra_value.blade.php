
@if(!isset($mode) or $mode == 'edit')
    <div class="form-group row">
        <label for="transport" class="col-sm-3 control-label"> Extra Value</label>
        <div class="col-sm-5">
            <input name="cost_extra_value_label" type="text" placeholder="Description" class="form-control" value="{{ $invoice->value->cost_extra_value_label }}">
        </div>
        <div class="col-sm-4">
            <input v-model="cost_extra_value" name="cost_extra_value" type="text" placeholder="0.00" class="form-control moneyNegative" value="{{ $invoice->value->cost_extra_value }}">
        </div>
    </div>
@elseif(isset($mode) and $mode == 'show')
    @if($invoice->value->cost_extra_value)
        <div class="form-group row">
            <label for="" class="col-sm-3 control-label"> Extra Value</label>
            <div class="col-sm-9">
                @if($invoice->value->cost_extra_value_label)
                    <p class="form-control-static">{{ $invoice->value->cost_extra_value_label }}:</p>
                @endif
                <p class="form-control-static">$ {{ $invoice->value->cost_extra_value }}</p>
            </div>
        </div>
    @endif
@elseif(isset($mode) and $mode == 'create')
    <div class="form-group row">
        <label for="cost_extra_value_label" class="col-sm-3 control-label"> Extra Value</label>
        <div class="col-sm-5">
            <input name="cost_extra_value_label" type="text" placeholder="Description" class="form-control" value="{{ empty(old('cost_extra_value_label')) ? '' : old('cost_extra_value_label') }}">
        </div>
        <div class="col-sm-4">
            <input v-model="cost_extra_value" name="cost_extra_value" type="text" placeholder="0.00" class="form-control moneyNegative" value="{{ empty(old('cost_extra_value')) ? '' : old('cost_extra_value') }}">
        </div>
    </div>
@endif