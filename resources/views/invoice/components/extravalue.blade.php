
@if(!isset($mode) or $mode == 'edit')
    <div class="form-group row">
        <label for="transport" class="col-sm-3 control-label"> Extra Value</label>
        <div class="col-sm-5">
            <input name="extra_value_label" type="text" placeholder="Description" class="form-control" value="{{ $invoice->value->extra_value_label }}">
        </div>
        <div class="col-sm-4">
            <input v-model="extra_value" name="extra_value" type="text" placeholder="0.00" class="form-control moneyNegative" value="{{ $invoice->value->extra_value }}">
        </div>
    </div>
@elseif(isset($mode) and $mode == 'show')
    @if($invoice->value->extra_value)
        <div class="form-group row">
            <label for="corda" class="col-sm-3 control-label"> Extra Value</label>
            <div class="col-sm-9">
                @if($invoice->value->extra_value_label)
                    <p class="form-control-static">{{ $invoice->value->extra_value_label }}:</p>
                @endif
                <p class="form-control-static">$ {{ $invoice->value->extra_value }}</p>
            </div>
        </div>
    @endif
@elseif(isset($mode) and $mode == 'create')
    <div class="form-group row">
        <label for="transport" class="col-sm-3 control-label"> Extra Value</label>
        <div class="col-sm-5">
            <input name="extra_value_label" type="text" placeholder="Description" class="form-control" value="{{ empty(old('extra_value_label')) ? '' : old('extra_value_label') }}">
        </div>
        <div class="col-sm-4">
            <input v-model="extra_value" name="extra_value" type="text" placeholder="0.00" class="form-control moneyNegative" value="{{ empty(old('extra_value')) ? '' : old('extra_value') }}">
        </div>
    </div>
@endif