    <div class="panel-footer clearfix p-t1">
        <div class="col-sm-12 p-t4 text-center">
            <label class="checkbox-inline">
                <input type="checkbox" name="nautical_chart" value="1" {{ $invoice->remark->nautical_chart ? 'checked':'' }}> Carta Náutica
            </label>
            <label class="checkbox-inline">
                <input type="checkbox" name="join_request" value="1" {{ $invoice->remark->join_request ? 'checked':'' }}> Solicitação de ingresso
            </label>
            <label class="checkbox-inline">
                <input type="checkbox" name="feedback" value="1" {{ $invoice->remark->feedback ? 'checked':'' }}> Feedback Form/Invoice
            </label>
            <label class="checkbox-inline">
                <input type="checkbox" name="medicines" value="1" {{ $invoice->remark->medicines ? 'checked':'' }}> Medicamentos
            </label>
            <label class="checkbox-inline">
                <input type="checkbox" name="boat_vs" value="1" {{ $invoice->remark->boat_vs ? 'checked':'' }}> Lancha via VS
            </label>
            <br>
            <label class="checkbox-inline">
                <input type="checkbox" name="stowage_vs" value="1" {{ $invoice->remark->stowage_vs ? 'checked':'' }}> Estiva via VS
            </label>
            <label class="checkbox-inline">
                <input type="checkbox" name="boat_client" value="1" {{ $invoice->remark->boat_client ? 'checked':'' }}> Lancha via agência
            </label>
            <label class="checkbox-inline">
                <input type="checkbox" name="stowage_client" value="1" {{ $invoice->remark->stowage_client ? 'checked':'' }}> Estiva via agência
            </label>
            <label class="checkbox-inline">
                <input type="checkbox" name="billed" value="1" v-model="billed" {{ $invoice->remark->billed ? 'checked':'' }}> Faturado
            </label>
            <label class="checkbox-inline">
                <input type="checkbox" name="other_remark" v-model="other" {{ $invoice->remark->other ? 'checked':'' }}> Outros
            </label>
            <div class="p-t1" v-if="other" transition="expand">
                <textarea name="other" cols="30" rows="1" class="form-control" style="resize:none" placeholder="Tip your remark">{{ $invoice->remark->other }}</textarea>
            </div>
            <div class="p-t1"></div>
            <textarea name="obs" id="info" cols="30" rows="3" class="form-control" placeholder="Observations">{{ $invoice->obs }}</textarea>
            <div class="p-t1"></div>
        </div>
    </div>


