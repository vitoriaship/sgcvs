@extends('main')

@section('title', 'SGCVS - Sistema de Gestão Comercial Vitória Ship')

@section('navbar')

    @section('module')
        <h3 class="white"><span class="glyphicon glyphicon-time" aria-hidden="true"></span> Line-up</h3>
        @include('lineup-last-updated')
    @endsection

    @include('navbar')

@endsection

@section('content')

    <div class="container-fluid p-t2 m40">
    <div class="row">
        @include('messages')
        <div class="table-responsive">
            <table class="table table-bordered table-condensed table-hover data-table">
                <thead>
                    <tr class="active">
                        <th>Invoice</th>
                        <th>Groups</th>
                        <th>Vessel</th>
                        <th>Port</th>
                        <th>Agency</th>
                        <th>R.D.A</th>
                        <th>CO-R.D.A</th>
                        <th class="status">Remarks</th>
                        <th>ETA</th>
                        <th>ETB</th>
                        <th>ETS</th>
                        <th style="width:8%">Options</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($invoices as $invoice)
                    <tr {{ $invoice->remark->has_remarks ? 'class=info' : '' }}>
                        <td>
                            {{ $invoice->number }}
                            @if($invoice->novo == 1)
                                <button class="btn btn-success btn-xs">
                                    <i class="icon icon-file"></i>
                                    New
                                </button>
                            @endif
                        </td>
                        <td>{{ $invoice->groups }}</td>
                        <td>{{ $invoice->vessel->name }}</td>
                        <td>{{ $invoice->port->name }}</td>
                        <td>{{ $invoice->agency->name }}</td>
                        <td>{{ $invoice->rda->name }}</td>
                        <td>{{ $invoice->corda ? $invoice->corda->name : '' }}</td>
                        <td class="dropdown">
                        @if($invoice->remark->has_remarks)
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                <span class="glyphicon glyphicon-circle-arrow-down" aria-hidden="true"></span> See
                            </a>
                            <ul class="dropdown-menu p-t8">
                                @if($invoice->remark->nautical_chart) <li>Carta Náutica</li> @endif
                                @if($invoice->remark->join_request) <li>Solicitação de ingresso</li> @endif
                                @if($invoice->remark->feedback) <li>Feedback Form/Invoice</li> @endif
                                @if($invoice->remark->medicines) <li>Medicamentos</li> @endif
                                @if($invoice->remark->boat_vs) <li>Lancha via VS</li> @endif
                                @if($invoice->remark->stowage_vs) <li>Estiva via VS</li> @endif
                                @if($invoice->remark->boat_client) <li>Lancha via agência</li> @endif
                                @if($invoice->remark->stowage_client) <li>Estiva via agência</li> @endif
                                @if($invoice->remark->billed) <li>Faturado</li> @endif
                                @if($invoice->remark->other) <li>{{ $invoice->remark->other }}</li> @endif
                            </ul>
                        @endif
                        </td>
                        <td>{{ $invoice->date->eta }}</td>
                        <td>{{ $invoice->date->etb ? $invoice->date->etb : '' }}</td>
                        <td>{{ $invoice->date->ets ? $invoice->date->ets : '' }}</td>
                        <td>
                            <div class="btn-group btn-group-justified" role="group" aria-label="...">

                                {{--
                                <div class="btn-group btn-group-xs" role="group">
                                    <a href="{{ route('invoice.locale', Hashids::encode($invoice->id)) }}" class="btn btn-default iframe2 fancybox.iframe">
                                        <span class="glyphicon glyphicon-retweet" aria-hidden="true"></span>
                                    </a>
                                </div>
                                --}}

                                <div class="btn-group btn-group-xs" role="group">
                                    <a href="{{ route('invoice.edit', Hashids::encode($invoice->id)) }}" class="btn btn-default border-right-n loading">
                                        <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
                                    </a>
                                </div>

                                <div class="btn-group btn-group-xs" role="group">
                                    <a href="{{ route('invoice.dates', Hashids::encode($invoice->id)) }}" class="btn btn-default iframe2 fancybox.iframe">
                                        <span class="glyphicon glyphicon-time" aria-hidden="true"></span>
                                    </a>
                                </div>

                                {{--
                                @can('delete')
                                    <div class="btn-group btn-group-xs" role="group">
                                        <a href="{{ route('invoice.destroy', Hashids::encode($invoice->id)) }}" class="btn btn-default"
                                            data-token="{{ csrf_token() }}"
                                            data-request=""
                                            data-title="Are you sure?"
                                            data-message="Are you sure that you want delete this Invoice?"
                                            data-button-text="Yes, delete it!">
                                            <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
                                        </a>
                                    </div>
                                @endcan

                                @can('duplicate')
                                    <div class="btn-group btn-group-xs" role="group">
                                        <a href="{{ route('invoice.duplicate', Hashids::encode($invoice->id)) }}" class="btn btn-default"
                                            data-token="{{ csrf_token() }}"
                                            data-request=""
                                            data-type="info"
                                            data-method="POST"
                                            data-title="Duplicate"
                                            data-message="Are you sure that you want duplicate this Invoice?"
                                            data-button-text="Yes, duplicate it!">
                                            <span class="glyphicon glyphicon-duplicate" aria-hidden="true"></span>
                                        </a>
                                    </div>
                                @endcan
                                --}}
                            </div>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        <span class="alert alert-info" role="alert">* Remarks Assets</span>
    </div>
    </div>

@include('footer')

@endsection
