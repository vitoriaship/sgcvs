@extends('main')

@section('title', 'Settlement | SGCVS - Sistema de Gestão Comercial Vitória Ship')

@section('navbar')

    @section('module')
        <h3 class="white"><span class="glyphicon glyphicon-search" aria-hidden="true"></span> Settlement</h3>
    @endsection

    @include('navbar')

@endsection

@section('content')
    <form action="{{ route('settlement') }}" method="get">
        <div class="container p-t1">
            <div class="panel panel-default">
                <div class="panel-body p-t7">
                    <div class="row form-horizontal p-t4">
                        <div class="col-sm-6">
                            <div class="form-group row">
                                <label for="month" class="col-sm-4 control-label">Month</label>
                                <div class="col-sm-8">
                                    <select name="month" class="form-control">
                                        <option value="all" {{ Input::get('month') == "all" ? 'selected':'' }}>All</option>
                                        @foreach(range(1,12) as $index)
                                            <option value="{{$index}}" {{ Input::get('month') == $index ? 'selected':'' }}>
                                                {{ strtoupper(DateTime::createFromFormat('!m', $index)->format('M')) }}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="year" class="col-sm-4 control-label">Year</label>
                                <div class="col-sm-8">
                                    <select name="year" class="form-control">
                                        <option value="all" selected>All</option>
                                        @foreach($years as $y)
                                            <option value="{{ $y->year }}" {{ Input::get('year') == $y->year ? 'selected':''  }}>{{ $y->year }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="vessel" class="col-sm-4 control-label">Vessel</label>
                                <div class="col-sm-8">
                                    <select name="vessel" class="form-control">
                                        <option value="all" selected>All</option>
                                        @foreach($vessels as $v)
                                            <option value="{{ $v->id }}" {{ Input::get('vessel') == $v->id ? 'selected':''  }}>{{ $v->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="port" class="col-sm-4 control-label">Port</label>
                                <div class="col-sm-8">
                                    <select name="port" class="form-control">
                                        <option value="all" selected>All</option>
                                        @foreach($ports as $p)
                                            <option value="{{ $p->id }}" {{ Input::get('port') == $p->id ? 'selected':''  }}>{{ $p->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group row">
                                <label for="rda" class="col-sm-4 control-label">Customer</label>
                                <div class="col-sm-8">
                                    <select name="rda" class="form-control">
                                        <option value="all" selected>All</option>
                                        @foreach($rdas as $rda)
                                            <option value="{{ $rda->id }}" {{ Input::get('rda') == $rda->id ? 'selected':''  }}>{{ $rda->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="locale" class="col-sm-4 control-label">Locale</label>
                                <div class="col-sm-8">
                                    <select name="locale" class="form-control">
                                        <option value="all" selected>All</option>
                                        @foreach($locales as $l)
                                            <option value="{{ $l->id }}" {{ Input::get('locale') == $l->id ? 'selected':'' }}>{{ $l->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="status" class="col-sm-4 control-label">Status</label>
                                <div class="col-sm-8">
                                    <select name="status" class="form-control">
                                        <option value="all" {{ !Input::get('status') || Input::get('status') == 'all' ? 'selected':''  }}>All</option>
                                        <option value="supplied" {{ Input::get('status') == 'supplied' ? 'selected':''  }}>Supplied</option>
                                        <option value="not-supplied" {{ Input::get('status') == 'not-supplied' ? 'selected':''  }}>Not Supplied</option>
                                        <option value="paid" {{ Input::get('status') == 'paid' ? 'selected':''  }}>Paid</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="number_nfe" class="col-sm-4 control-label">Invoice/NF-e</label>
                                <div class="col-sm-8">
                                    <input name="number_nfe" type="text" class="form-control" value="{{ Input::get('number_nfe') }}">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2 col-md-offset-10 col-xs-12">
                            <input class="btn btn-primary btn-block loading" type="submit" value="Search">
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="container-fluid p-t2 m40">
            <div class="row">
                <div class="table-responsive">
                    <table class="table table-bordered table-condensed table-hover data-table">
                        <thead>
                            <tr class="active">
                                <th>Supply Date</th>
                                <th>Status</th>
                                <th>Invoice</th>
                                <th>NF-e</th>
                                <th>Locale</th>
                                <th>Vessel</th>
                                <th>Port</th>
                                <th>R.D.A</th>
                                <th>CO-R.D.A</th>
                                <th>Payment Terms</th>
                                <th>Due Date</th>
                                <th>Val. Gross(USD)</th>
                                <th>% Disc.</th>
                                <th>Val. Disc.(USD)</th>
                                <th>Val. Gross 2(USD)</th>
                                <th>% Disc.</th>
                                <th>Val. Disc.(USD)</th>
                                <th>USD Val. W/O Disc</th>
                                <th>Operational / Charges</th>
                                <th>Val. Liquid (USD)</th>
                                <th>Received Value</th>
                                <th>Out Balance</th>
                                <th>R$ Reference</th>
                                <th>Received Value R$</th>
                                <th>Commission</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($invoices as $invoice)
                            <tr>
                                <td>{{ $invoice->date->supply }}</td>
                                <td>{{ $invoice->getStatusName() }}</td>
                                <td>{{ $invoice->number }}</td>
                                <td>{{ $invoice->nfe }}</td>
                                <td>{{ $invoice->locale->name }}</td>
                                <td>{{ $invoice->vessel->name }}</td>
                                <td>{{ $invoice->port->name }}</td>
                                <td>{{ $invoice->rda->name }}</td>
                                <td>{{ $invoice->corda ? $invoice->corda->name : '' }}</td>
                                <td>{{ $invoice->date->payment }}</td>
                                <td>{{ $invoice->date->first_chat }}</td>
                                <td>{{ $invoice->value->gross_1 }}</td>
                                <td>{{ $invoice->value->percent_1 }}%</td>
                                <td>{{ $invoice->value->value_disc_1 }}</td>
                                <td>{{ $invoice->value->gross_2 }}</td>
                                <td>{{ $invoice->value->percent_2 }}%</td>
                                <td>{{ $invoice->value->value_disc_2 }}</td>
                                <td>{{ $invoice->value->wo }}</td>
                                <td>{{ $invoice->value->operational_charges }}</td>
                                <td>{{ $invoice->value->total }}</td>
                                <td>{{ $invoice->value->received }}</td>
                                <td>{{ $invoice->value->out_balance }}</td>
                                <td>{{ $invoice->value->reference }}</td>
                                <td>{{ $invoice->value->received_value_rs }}</td>
                                <td>{{ $invoice->value->commission }}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    <form>
@include('footer')

@endsection
