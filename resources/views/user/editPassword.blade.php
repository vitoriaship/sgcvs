@extends('main')

@section('title', 'Atualizar senha | SGCVS - Sistema de Gestão Comercial Vitória Ship')

@section('navbar')

    @section('module')
        <h3 class="white"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span> Atualizar Senha</h3>
    @endsection

    @include('navbar')

@endsection

@section('content')

    <div class="container m-01" id="app">
    <form action="{{ route('user.updatePassword', $user->id) }}" method="post">
        {!! csrf_field() !!}

        @include('messages')

        <div class="panel panel-primary">
            <div class="panel-heading">
                <h3 class="panel-title">Sua senha expirou</h3>
            </div>
            <div class="panel-body">
                <div class="row form-horizontal p-t4">
                    <div class="col-sm-6">
                        <div class="form-group row">
                            <label for="password" class="col-sm-3 control-label">Nova senha</label>
                            <div class="col-sm-9">
                                <input type="password" name="password" class="form-control">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password_confirmation" class="col-sm-3 control-label">Confirme a nova senha</label>
                            <div class="col-sm-9">
                                <input type="password" name="password_confirmation" class="form-control">
                            </div>
                        </div>


                    </div>
                </div>
            </div>
        </div>

        <div class="container">
            <div class="row">
                <div class="col-sm-10"></div>
                <div class="col-sm-2">
                    <input class="btn btn-primary btn-lg loading" type="submit" value="SALVAR">
                </div>
            </div>
        </div>
    </form>
    </div>

@include('footer')

@endsection
