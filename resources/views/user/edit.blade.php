@extends('main')

@section('title', 'Edit User')

@section('content')

    <div class="container p-t1">
        @include('messages')
        <div class="panel panel-default">
            <div class="panel-body">
                <div class="row form-horizontal">
                <form action="{{ route('user.update', $user->id) }}" method="post">
                    {!! csrf_field() !!}
                    <input name="_method" type="hidden" value="PUT">
                    <div class="panel-body">
                        <div class="row form-horizontal">
                            <div class="col-sm-6">
                                <div class="form-group row">
                                    <div class="col-xs-12">
                                        <label for="name" class="control-label">
                                            <span class="red">*</span> Name
                                        </label>
                                        <input name="name" type="text" class="form-control" value="{{ $user->name }}">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-xs-6">
                                        <label for="username" class="control-label">
                                            <span class="red">*</span> Login
                                        </label>
                                        <input name="username" type="text" class="form-control" value="{{ $user->username }}">
                                    </div>
                                    <div class="col-xs-6">
                                        <label for="locale_id" class="control-label">
                                            <span class="red">*</span> Locale
                                        </label>
                                        <select name="locale_id" class="form-control">
                                            @foreach($locales as $locale)
                                                <option value="{{ $locale->id }}" {{ ($locale->id == $user->locale_id ? "selected":"") }}>{{ $locale->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-6">
                                <div class="form-group row">
                                    <div class="col-xs-12">
                                        <label for="email" class="control-label">E-mail</label>
                                        <input name="email" type="email" class="form-control" value="{{ $user->email }}">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-xs-6">
                                        <label for="group_id" class="control-label">
                                            <span class="red">*</span> Group
                                        </label>
                                        <select name="group_id" class="form-control">
                                            @foreach($groups as $group)
                                                <option value="{{ $group->id }}" {{ ($group->id == $user->group_id ? "selected":"") }}>{{ $group->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-xs-6">
                                        <label for="password" class="control-label">
                                            <span class="red">*</span> Password
                                        </label>
                                        <input name="password" type="password" class="form-control">
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="row form-horizontal p-t4">
                            <div class="col-sm-6">
                            </div>
                            <div class="col-sm-6 text-right">
                                <input class="btn btn-primary loading" type="submit" value="SAVE" >
                            </div>
                        </div>
                    </div>
                </form>
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript">document.getElementsByTagName('body')[0].className+='nopad'</script>

@endsection
