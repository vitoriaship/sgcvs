@extends('main')

@section('title', 'Users | SGCVS - Sistema de Gestão Comercial Vitória Ship')

@section('navbar')

    @section('module')
        <h3 class="white"><span class="glyphicon glyphicon-briefcase" aria-hidden="true"></span> Activity by users</h3>
    @endsection

    @include('navbar')

@endsection

@section('content')

    <meta http-equiv="refresh" content="10" />

    <div class="container-fluid p-t2 m40">
        <div class="row">
            <div class="table-responsive">
                <table class="table table-bordered table-condensed table-hover">
                    <thead>
                        <tr class="active">
                            <th>Name</th>
                            <th>Alive Activity</th>
                            <th>Locale</th>
                        </tr>
                    </thead>
                    <tbody>
                        @forelse($sessions as $session)
                            @if(@$session->user)
                                <tr>
                                    <td>{{ $session->user->name }}</td>
                                    <td>{{ $session->total }}</td>
                                    <td>{{ $session->user->locale->name }}</td>
                                </tr>
                            @endif
                        @empty
                            <tr>
                                <td colspan="10">No alive users data</td>
                            </tr>
                        @endforelse
                    </tbody>
                </table>
            </div>
        </div>
    </div>

@include('footer')

@endsection
