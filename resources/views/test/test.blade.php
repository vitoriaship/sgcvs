<html>
<head>
    <title>teste</title>

    <script src="http://code.jquery.com/jquery-3.2.1.min.js"></script>
    <script src="/assets/js/moment.min.js"></script>

</head>
<body>
    Supply Date: <input type="text" name="supply" class="firstChat" value="01/01/2017"><br>
    Payment Terms: <input type="number" name="payment" class="firstChat" value="1"><br>
    Payment Date: <input type="text" name="payment_date" class="firstChat" value="12/03/2017"><br>
    First Chart: <input type="text" name="first_chart" value="12/03/2017"><br>



    <script type="text/javascript">
        $(document).ready(function() {
            $('.firstChat').change(function() {
                var supply = $('input[name=supply]').val();
                var payment = $('input[name=payment]').val();
                var payment_date = $('input[name=payment_date]').val();
                var first_chart = addDays(supply, payment);

                if(payment_date) {
                    $('input[name=first_chart]').val(payment_date);
                } else {
                    $('input[name=first_chart]').val(first_chart);
                }
            });
        });

        function addDays(date, days) {
            var moment_date = moment(date, "DD/MM/YYYY").add(days, 'days');
            return moment_date.format("DD/MM/YYYY");
        }
    </script>


</body>
</html>
