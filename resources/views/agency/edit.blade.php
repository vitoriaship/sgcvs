@extends('main')

@section('title', 'Edit Agency')

@section('content')

    <div class="container p-t1">
        <div class="panel panel-default">
            <div class="panel-body">
                <div class="row form-horizontal">
                <form action="{{ route('agency.update', $agency->id) }}" method="post">
                    {!! csrf_field() !!}
                    <input name="_method" type="hidden" value="PUT">
                    <div class="col-sm-4">
                        @include('messages')
                        <div class="form-group row">
                            <label for="nome_agencia" class="col-sm-4 control-label">
                                <span class= "red">*</span> Name
                            </label>
                            <div class="col-sm-8">
                                <input name="name" type="text" class="form-control" value="{{ $agency->name }}">
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-3 p-t4">
                        <div class="form-group row">
                            <label for="responsible" class="col-sm-5 control-label"> Responsible</label>
                            <div class="col-sm-7">
                                <input name="responsible" type="text" class="form-control" value="{{ $agency->responsible }}">
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-3 p-t4">
                        <div class="form-group row">
                            <label for="telephone" class="col-sm-2 control-label">Telephone</label>
                            <div class="col-sm-4">
                                <input name="telephone" type="text" class="form-control phone" value="{{ $agency->telephone }}">
                            </div>
                            <label for="cellphone" class="col-sm-2 control-label">Cellphone</label>
                            <div class="col-sm-4">
                                <input name="cellphone" type="text" class="form-control cell" value="{{ $agency->cellphone }}">
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <input class="btn btn-primary loading" type="submit" value="SAVE" >
                    </div>
                </form>
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript">document.getElementsByTagName('body')[0].className+='nopad'</script>

@endsection
