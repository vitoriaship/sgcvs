@extends('main')

@section('title', 'Agency | SGCVS - Sistema de Gestão Comercial Vitória Ship')

@section('navbar')

    @section('module')
        <h3 class="white">
            <span class="glyphicon glyphicon-floppy-save" aria-hidden="true"></span> Records
            <span class="font-l">/ Agency</span>
        </h3>
    @endsection

    @include('navbar')

@endsection

@section('content')

    <div class="container p-t1">
        @include('messages')
        <div class="panel panel-default">
            <form action="{{ route('agency.store') }}" method="post">
            {!! csrf_field() !!}
            <div class="panel-body p-t7">
                <div class="row form-horizontal p-t4">
                    <div class="col-sm-6">
                        <div class="form-group row">
                            <label for="name" class="col-sm-3 control-label">
                                <span class="red">*</span> Name
                            </label>
                            <div class="col-sm-9">
                                <input name="name" type="text" class="form-control" value="{{ old('name') }}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="responsible" class="col-sm-3 control-label">Responsible</label>
                            <div class="col-sm-9">
                                <input name="responsible" type="text" class="form-control" value="{{ old('responsible') }}">
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group row">
                            <label for="telephone" class="col-sm-3 control-label">Telephone</label>
                            <div class="col-sm-9">
                                <input name="telephone" type="text" class="form-control phone" value="{{ old('telephone') }}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="cellphone" class="col-sm-3 control-label">Cellphone</label>
                            <div class="col-sm-9">
                                <input name="cellphone" type="text" class="form-control cell" value="{{ old('cellphone') }}">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-2 col-md-offset-10 col-xs-12">
                        <input class="btn btn-primary btn-block loading" type="submit" value="SAVE" >
                    </div>
                </div>
            </div>
            </form>
        </div>
    </div>
    <div class="container">
        <div class="table-responsive">
            <table class="table table-bordered table-condensed table-hover data-table">
                <thead>
                    <tr class="active">
                        <th>Name</th>
                        <th>Responsable</th>
                        <th>Telephone</th>
                        <th>Cellphone</th>
                        <th class="opcoes">Options</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($agencies as $agency)
                    <tr>
                        <td>{{ $agency->name }}</td>
                        <td>{{ $agency->responsible }}</td>
                        <td>{{ $agency->telephone }}</td>
                        <td>{{ $agency->cellphone }}</td>
                        <td>
                            <div class="btn-group btn-group-justified" role="group" aria-label="...">
                                <div class="btn-group btn-group-xs" role="group">
                                    <a href="{{ route('agency.edit', $agency->id) }}" class="btn btn-default iframe fancybox.iframe loading border-right-n">
                                        <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
                                    </a>
                                </div>
                                <div class="btn-group btn-group-xs" role="group">
                                    <a href="{{ route('agency.destroy', $agency->id) }}" class="btn btn-default"
                                        data-token="{{ csrf_token() }}"
                                        data-request=""
                                        data-title="Are you sure?"
                                        data-message="Are you sure that you want delete the agency {{ $agency->name }}?"
                                        data-button-text="Yes, delete it!">
                                        <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
                                    </a>
                                </div>
                            </div>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            <div>Showing {!! $agencies->count() !!} of {!! $agencies->total() !!} results</div>
            {!! $agencies->render() !!}
        </div>
    </div>

@include('footer')

@endsection
