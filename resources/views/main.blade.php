<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="robots" content="noindex" />
        <link rel="icon" href="/dist/images/favicon.ico">
        <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,700' rel='stylesheet' type='text/css'>
        <title>@yield('title')</title>

        <!-- build:css(public) css/all.css -->
        <link rel="stylesheet" href="/assets/css/bootstrap.css">
        <link rel="stylesheet" href="/assets/css/jquery-ui-1.10.0.custom.css" />
        <link rel="stylesheet" href="/assets/css/font-awesome.min.css" />
        <link rel="stylesheet" href="/assets/css/ie10-viewport-bug-workaround.css">
        <link rel="stylesheet" href="/assets/css/fancy/jquery.fancybox.css"/>
        <link rel="stylesheet" href="/assets/css/style.css">
        <link rel="stylesheet" href="/assets/css/dataTables.bootstrap.min.css"/>
        <link rel="stylesheet" href="/assets/css/buttons.bootstrap.min.css"/>
        <link rel="stylesheet" href="/assets/css/buttons.dataTables.min.css"/>
        <link rel="stylesheet" href="/assets/css/sweetalert.css"/>
        <link rel="stylesheet" href="/assets/js/select2/css/select2.min.css"/>
        <!-- endbuild -->
        <!--<link rel="stylesheet" href="/dist/css/all.css">-->

    </head>
    <body>

        <div class="loader"></div>

        @yield('navbar')

        @yield('content')

        <!-- build:js(public) js/all.js -->
        <script src="/assets/js/jquery.min.js"></script>
        <script src="/assets/js/bootstrap.min.js"></script>
        <script src="/assets/js/jquery-ui-1.10.0.custom.min.js"></script>
        <script src="/assets/js/jquery.mask.js"></script>
        <script src="/assets/css/fancy/jquery.fancybox.js"></script>
        <script src="/assets/js/sweetalert.min.js"></script>
        <script src="/assets/js/jquery.dataTables.min.js"></script>
        <script src="/assets/js/moment.min.js"></script>
        <script src="/assets/js/datetime-moment.js"></script>
        <script src="/assets/js/dataTables.bootstrap.min.js"></script>
        <script src="/assets/js/dataTables.buttons.min.js"></script>
        <script src="/assets/js/jszip.min.js"></script>
        <script src="/assets/js/pdfmake.min.js"></script>
        <script src="/assets/js/vfs_fonts.js"></script>
        <script src="/assets/js/buttons.html5.min.js"></script>
        <script src="/assets/js/buttons.print.min.js"></script>
        <script src="/assets/js/buttons.colVis.min.js"></script>
        <script src="/assets/js/jquery.maskMoney.min.js"></script>
        <script src="/assets/js/ie10-viewport-bug-workaround.js"></script>
        <script src="/assets/js/vue.min.js"></script>
        <script src="/assets/js/alerts.js"></script>
        <script src="/assets/js/vue-app.js?v=2"></script>
        <script src="/assets/js/select2/js/select2.full.min.js"></script>
        <script src="/assets/js/select2/js/i18n/pt-BR.js"></script>
        <script src="/assets/js/SGCVS.js?v=2"></script>
        <!-- endbuild -->

        <!--<script src="/dist/js/all.js"></script>-->
    </body>
</html>
