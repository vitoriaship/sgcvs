@extends('main')

@section('title', 'Line-up | SGCVS - Sistema de Gestão Comercial Vitória Ship')

@section('navbar')

    @section('module')
        <h3 class="white">
            <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
            <span>Line-up</span>
        </h3>
    @endsection

    @include('navbar')

@endsection

@section('content')

    <div class="container p-t1">
        @include('messages')


        <div class="panel panel-danger">
            <div class="panel-body p-t7">
                <div class="row form-horizontal">
                    <div class="col-sm-6">
                        <div class="form-group row">
                            <label class="col-sm-6 control-label">Supply status:</label>
                            <div class="col-sm-3">
                                <p class="form-control-static"><span class="red">{{ $invoice->status->name }}</span></p>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 p-t4">
                        <div class="form-group row">
                            <label class="col-sm-6 control-label">Billing status:</label>
                            <div class="col-sm-3">
                                <p class="form-control-static"><span class="red">{{ $invoice->conclusion->name }}</span></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="panel panel-primary">
            <div class="panel-heading">
                <h3 class="panel-title">Informations</h3>
            </div>
            <div class="panel-body">
                <div class="row form-horizontal p-t4">
                    <div class="col-sm-6">
                        <div class="form-group row">
                            <label for="suply_date" class="col-sm-3 control-label">Supply Date</label>
                            <div class="col-sm-9">
                                <p class="form-control-static">{{ $invoice->date->supply }}</p>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="suply_date" class="col-sm-3 control-label">Hour</label>
                            <div class="col-sm-9">
                                <p class="form-control-static">{{ $invoice->date->hour }}</p>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="invoice" class="col-sm-3 control-label">Invoice</label>
                            <div class="col-sm-9">
                                <p class="form-control-static">{{ $invoice->number }}</p>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="nf" class="col-sm-3 control-label">NFe</label>
                            <div class="col-sm-9">
                                <p class="form-control-static">{{ $invoice->nfe }}</p>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="grupos" class="col-sm-3 control-label">Groups</label>
                            <div class="col-sm-9">
                                <p class="form-control-static">{{ $invoice->groups }}</p>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="vessel" class="col-sm-3 control-label">Vessel</label>
                            <div class="col-sm-9">
                                <p class="form-control-static">{{ $invoice->vessel->name }}</p>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="port" class="col-sm-3 control-label">Port</label>
                            <div class="col-sm-9">
                                <p class="form-control-static">{{ $invoice->port->name }}</p>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="agency" class="col-sm-3 control-label">Agency</label>
                            <div class="col-sm-9">
                                <p class="form-control-static">{{ $invoice->agency->name }}</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group row">
                            <label for="eta" class="col-sm-3 control-label">ETA</label>
                            <div class="col-sm-9">
                                <p class="form-control-static">{{ $invoice->date->eta }}</p>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="etb" class="col-sm-3 control-label">ETB</label>
                            <div class="col-sm-9">
                                <p class="form-control-static">{{ $invoice->date->etb }}</p>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="ets" class="col-sm-3 control-label">ETS</label>
                            <div class="col-sm-9">
                                <p class="form-control-static">{{ $invoice->date->ets }}</p>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="rda" class="col-sm-3 control-label">R.D.A</label>
                            <div class="col-sm-9">
                                <p class="form-control-static">{{ $invoice->rda->name }}</p>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="corda" class="col-sm-3 control-label">CO-R.D.A</label>
                            <div class="col-sm-9">
                                <p class="form-control-static">{{ $invoice->corda ? $invoice->corda->name : '' }}</p>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="pterms" class="col-sm-3 control-label">Payment terms</label>
                            <div class="col-sm-9">
                                <p class="form-control-static">{{ $invoice->date->payment }}</p>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="fchat" class="col-sm-3 control-label">Payment Date</label>
                            <div class="col-sm-9">
                                <p class="form-control-static">{{ $invoice->date->first_chat }}</p>
                            </div>
                        </div>


                        <div class="form-group row">
                            <label for="commission_paid" class="col-sm-3 control-label">Commission paid</label>
                            <div class="col-sm-9">
                                <p class="form-control-static">{{ $invoice->value->commission_paid ? 'Yes':'No' }}</p>
                            </div>
                        </div>



                    </div>
                </div>
            </div>
        </div>



        <div class="panel panel-primary">
            <div class="panel-heading">
                <h3 class="panel-title">Cost Values</h3>
            </div>
            <div class="panel-body">
                <div class="row form-horizontal p-t4">
                    <div class="col-sm-6">

                        <div class="form-group row">
                            <label class="col-sm-3 control-label">Observations</label>
                            <div class="col-sm-9">
                                <p class="form-control-static">{{ $invoice->obs ? $invoice->obs : '-' }}</p>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-3 control-label">Pending Costs</label>
                            <div class="col-sm-9">
                                <p class="form-control-static">{{ $invoice->value->pending_cost ? 'YES' : 'NO' }}</p>
                            </div>
                        </div>
                        @if($invoice->value->pending_cost)
                        <div class="form-group row">
                            <label class="col-sm-3 control-label">Pending Costs Obs.</label>
                            <div class="col-sm-9">
                                <p class="form-control-static">{{ $invoice->value->pending_cost_obs }}</p>
                            </div>
                        </div>
                        @endif
                    </div>
                </div>
            </div>


            <div class="panel-footer clearfix p-t1">
                <div class="col-sm-12 p-t4 text-center">
                    <label class="checkbox-inline">
                        <input disabled readonly type="checkbox" name="nautical_chart" value="1" {{ $invoice->remark->nautical_chart ? 'checked':'' }}> Carta Náutica
                    </label>
                    <label class="checkbox-inline">
                        <input disabled readonly type="checkbox" name="join_request" value="1" {{ $invoice->remark->join_request ? 'checked':'' }}> Solicitação de ingresso
                    </label>
                    <label class="checkbox-inline">
                        <input disabled readonly type="checkbox" name="feedback" value="1" {{ $invoice->remark->feedback ? 'checked':'' }}> Feedback Form/Invoice
                    </label>
                    <label class="checkbox-inline">
                        <input disabled readonly type="checkbox" name="medicines" value="1" {{ $invoice->remark->medicines ? 'checked':'' }}> Medicamentos
                    </label>
                    <label class="checkbox-inline">
                        <input disabled readonly type="checkbox" name="boat_vs" value="1" {{ $invoice->remark->boat_vs ? 'checked':'' }}> Lancha via VS
                    </label>
                    <br>
                    <label class="checkbox-inline">
                        <input disabled readonly type="checkbox" name="stowage_vs" value="1" {{ $invoice->remark->stowage_vs ? 'checked':'' }}> Estiva via VS
                    </label>
                    <label class="checkbox-inline">
                        <input disabled readonly type="checkbox" name="boat_client" value="1" {{ $invoice->remark->boat_client ? 'checked':'' }}> Lancha via agência
                    </label>
                    <label class="checkbox-inline">
                        <input disabled readonly type="checkbox" name="stowage_client" value="1" {{ $invoice->remark->stowage_client ? 'checked':'' }}> Estiva via agência
                    </label>
                    <label class="checkbox-inline">
                        <input disabled readonly type="checkbox" name="stowage_client" value="1" {{ $invoice->remark->billed ? 'checked':'' }}> Faturado
                    </label>
                    <label class="checkbox-inline">
                        <input disabled readonly type="checkbox" name="other_remark" v-model="other" {{ $invoice->remark->other ? 'checked':'' }}> Outros
                    </label>
                    <div class="p-t1" v-if="other" transition="expand">
                        <textarea disabled readonly  name="other" cols="30" rows="1" class="form-control" style="resize:none" placeholder="Others remarks">{{ $invoice->remark->other }}</textarea>
                    </div>
                    <div class="p-t1"></div>
                    <textarea disabled readonly  name="obs" id="info" cols="30" rows="3" class="form-control" placeholder="Observations">{{ $invoice->obs }}</textarea>
                    <div class="p-t1"></div>
                </div>
            </div>

        </div>


    </div>

@include('footer')

@endsection
