@extends('main')

@section('title', 'Edit Dates | SGCVS - Sistema de Gestão Comercial Vitória Ship')

@section('content')

    <div class="container p-t1">
        <h3>
            <span class="font-l">Vessel:</span> {{ $invoice->vessel->name }}
            <span class="font-l">/ Invoice:</span> {{ $invoice->number }}
        </h3>
        <form action="{{ route('finance.datesUpdate', $invoice->id) }}" method="post">
            {!! csrf_field() !!}
            <input name="_method" type="hidden" value="PUT">
            <input name="form" type="hidden" value="dates">
            @include('messages')
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="row form-horizontal">
                        <div class="col-sm-4">
                            <div class="form-group row">
                                <div class="col-sm-9">
                                    <label class="checkbox-inline">
                                        <input type="checkbox" name="billed" value="1" {{ $invoice->remark->billed ? 'checked':'' }}> Faturado
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="col-sm-4 p-t4">
                            <div class="form-group row">
                                <label for="nfe" class="col-sm-3 control-label">NF-e</label>
                                <div class="col-sm-9">
                                    <input name="nfe" type="text" class="form-control" value="{{ $invoice->nfe }}">
                                </div>
                            </div>
                        </div>


                    </div>
                </div>
                <div class="panel-footer clearfix p-t1">
                    <div class="col-sm-12 p-t4 text-center">

                        <div class="p-t1"></div>
                        <div class="col-sm-2">
                            <input class="btn btn-primary loading" type="submit" value="UPDATE" >
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>

    <script type="text/javascript">document.getElementsByTagName('body')[0].className+='nopad'</script>

@endsection
