@extends('main')

@section('title', 'Historic | SGCVS - Sistema de Gestão Comercial Vitória Ship')

@section('navbar')

    @section('module')
        <h3 class="white">
            <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
            <span>Historic</span>
        </h3>
    @endsection

    @include('navbar')

@endsection

@section('content')

    <div class="container p-t1">
        <div class="panel panel-danger">
            <div class="panel-body p-t7">
                <div class="row form-horizontal">
                    <div class="col-sm-6">
                        <div class="form-group row">
                            <label class="col-sm-6 control-label">Supply status:</label>
                            <div class="col-sm-3">
                                <p class="form-control-static"><span class="red">{{ $invoice->status->name }}</span></p>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 p-t4">
                        <div class="form-group row">
                            <label class="col-sm-6 control-label">Billing status:</label>
                            <div class="col-sm-3">
                                <p class="form-control-static"><span class="red">{{ $invoice->conclusion->name }}</span></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="panel panel-primary">
            <div class="panel-heading">
                <h3 class="panel-title">Informations</h3>
            </div>
            <div class="panel-body">
                <div class="row form-horizontal p-t4">
                    <div class="col-sm-6">
                        <div class="form-group row">
                            <label for="suply_date" class="col-sm-3 control-label">Supply Date</label>
                            <div class="col-sm-9">
                                <p class="form-control-static">{{ $invoice->date->supply }}</p>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="suply_date" class="col-sm-3 control-label">Hour</label>
                            <div class="col-sm-9">
                                <p class="form-control-static">{{ $invoice->date->hour }}</p>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="invoice" class="col-sm-3 control-label">Invoice</label>
                            <div class="col-sm-9">
                                <p class="form-control-static">{{ $invoice->number }}</p>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="nf" class="col-sm-3 control-label">NFe</label>
                            <div class="col-sm-9">
                                <p class="form-control-static">{{ $invoice->nfe }}</p>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="grupos" class="col-sm-3 control-label">Groups</label>
                            <div class="col-sm-9">
                                <p class="form-control-static">{{ $invoice->groups }}</p>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="vessel" class="col-sm-3 control-label">Vessel</label>
                            <div class="col-sm-9">
                                <p class="form-control-static">{{ $invoice->vessel->name }}</p>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="port" class="col-sm-3 control-label">Port</label>
                            <div class="col-sm-9">
                                <p class="form-control-static">{{ $invoice->port->name }}</p>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="agency" class="col-sm-3 control-label">Agency</label>
                            <div class="col-sm-9">
                                <p class="form-control-static">{{ $invoice->agency->name }}</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group row">
                            <label for="eta" class="col-sm-3 control-label">ETA</label>
                            <div class="col-sm-9">
                                <p class="form-control-static">{{ $invoice->date->eta }}</p>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="etb" class="col-sm-3 control-label">ETB</label>
                            <div class="col-sm-9">
                                <p class="form-control-static">{{ $invoice->date->etb }}</p>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="ets" class="col-sm-3 control-label">ETS</label>
                            <div class="col-sm-9">
                                <p class="form-control-static">{{ $invoice->date->ets }}</p>
                            </div>
                        </div>
                        

                        <div class="form-group row">
                            <label class="col-sm-3 control-label">Vehicle</label>
                            <div class="col-sm-9">
                                <p class="form-control-static">{{ $invoice->vehicle ? $invoice->vehicle->plaque : '' }}</p>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-3 control-label">Employers</label>
                            <div class="col-sm-9">
                                @if($invoice->employerOne || $invoice->employerTwo || $invoice->employerThree)
                                <ul class="lista-status">
                                    <li>{{ $invoice->employerOne ? $invoice->employerOne->name : '' }}</li>
                                    @if($invoice->employerTwo)
                                        <li>{{ $invoice->employerTwo->name }}</li>
                                    @endif
                                    @if($invoice->employerThree)
                                        <li>{{ $invoice->employerThree->name }}</li>
                                    @endif
                                </ul>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-3 control-label">Remarks</label>
                            <div class="col-sm-9">
                                <ul class="lista-status">
                                    @if($invoice->remark->nautical_chart) <li>Carta Náutica</li> @endif
                                    @if($invoice->remark->join_request) <li>Solicitação de ingresso</li> @endif
                                    @if($invoice->remark->feedback) <li>Feedback Form/Invoice</li> @endif
                                    @if($invoice->remark->medicines) <li>Medicamentos</li> @endif
                                    @if($invoice->remark->boat_vs) <li>Lancha via VS</li> @endif
                                    @if($invoice->remark->stowage_vs) <li>Estiva via VS</li> @endif
                                    @if($invoice->remark->boat_client) <li>Lancha via agência</li> @endif
                                    @if($invoice->remark->stowage_client) <li>Estiva via agência</li> @endif
                                    @if($invoice->remark->billed) <li>Faturado</li> @endif
                                    @if($invoice->remark->other) <li>{{ $invoice->remark->other }}</li> @endif
                                </ul>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-3 control-label">Observations</label>
                            <div class="col-sm-9">
                                <p class="form-control-static">{{ $invoice->obs }}</p>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>

@include('footer')

@endsection
