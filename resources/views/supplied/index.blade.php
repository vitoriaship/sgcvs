@extends('main')

@section('title', 'Historic | SGCVS - Sistema de Gestão Comercial Vitória Ship')

@section('navbar')

    @section('module')
        <h3 class="white">
            <span class="glyphicon glyphicon-check" aria-hidden="true"></span> 
            <span>Supplied</span>
        </h3>
    @endsection

    @include('navbar')

@endsection

@section('content')

    <div class="container-fluid p-t2 m40">
        <div class="row">
            <div class="table-responsive">
                <table id="table" class="table table-bordered table-condensed table-hover data-table">
                    <thead>
                        <tr class="active">
                            <th>Supply Date</th>
                            <th>Invoice</th>
                            <th>NFe</th>
                            <th>Vessel</th>
                            <th>Port</th>
                            <th class="opcoes">Details</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($invoices as $invoice)
                        <tr>
                            <td>{{ $invoice->date->supply }}</td>
                            <td>{{ $invoice->number }}</td>
                            <td>{{ $invoice->nfe }}</td>
                            <td>{{ $invoice->vessel->name }}</td>
                            <td>{{ $invoice->port->name }}</td>
                            <td>
                                <div class="btn-group btn-group-justified" role="group" aria-label="...">
                                    <div class="btn-group btn-group-xs" role="group">
                                        <a href="{{ route('supplied.show', Hashids::encode($invoice->id)) }}" class="btn btn-default loading">
                                            <span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span>
                                        </a>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>

@include('footer')

@endsection
