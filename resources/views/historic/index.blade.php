@extends('main')

@section('title', 'Historic | SGCVS - Sistema de Gestão Comercial Vitória Ship')

@section('navbar')

    @section('module')
        <h3 class="white"><span class="glyphicon glyphicon-briefcase" aria-hidden="true"></span> Historic</h3>
    @endsection

    @include('navbar')

@endsection

@section('content')
    <form action="{{ route('historic.index') }}" method="get">
        <div class="container p-t1">
            <div class="panel panel-default">
                <div class="panel-body p-t7">
                    <div class="row form-horizontal p-t4">
                        <div class="col-sm-6">
                            <div class="form-group row">
                                <label for="month" class="col-sm-4 control-label">Month</label>
                                <div class="col-sm-8">
                                    <select name="month" class="form-control">
                                        <option value="all" {{ Input::get('month') == "all" ? 'selected':'' }}>All</option>
                                        @foreach(range(1,12) as $index)
                                            <option value="{{$index}}" {{ Input::get('month') == $index ? 'selected':'' }}>
                                                {{ strtoupper(DateTime::createFromFormat('!m', $index)->format('M')) }}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="year" class="col-sm-4 control-label">Year</label>
                                <div class="col-sm-8">
                                    <select name="year" class="form-control">
                                        <option value="all" selected>All</option>
                                        @foreach($years as $y)
                                            <option value="{{ $y->year }}" {{ Input::get('year') == $y->year ? 'selected':''  }}>{{ $y->year }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="vessel" class="col-sm-4 control-label">Vessel</label>
                                <div class="col-sm-8">
                                    <select name="vessel" class="form-control">
                                        <option value="all" selected>All</option>
                                        @foreach($vessels as $v)
                                            <option value="{{ $v->id }}" {{ Input::get('vessel') == $v->id ? 'selected':''  }}>{{ $v->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group row">
                                <label for="rda" class="col-sm-4 control-label">Customer</label>
                                <div class="col-sm-8">
                                    <select name="rda" class="form-control">
                                        <option value="all" selected>All</option>
                                        @foreach($rdas as $rda)
                                            <option value="{{ $rda->id }}" {{ Input::get('rda') == $rda->id ? 'selected':''  }}>{{ $rda->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="locale" class="col-sm-4 control-label">Locale</label>
                                <div class="col-sm-8">
                                    <select name="locale" class="form-control">
                                        <option value="all" selected>All</option>
                                        @foreach($locales as $l)
                                            <option value="{{ $l->id }}" {{ Input::get('locale') == $l->id ? 'selected':'' }}>{{ $l->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="port" class="col-sm-4 control-label">Port</label>
                                <div class="col-sm-8">
                                    <select name="port" class="form-control">
                                        <option value="all" selected>All</option>
                                        @foreach($ports as $v)
                                            <option value="{{ $v->id }}" {{ Input::get('port') == $v->id ? 'selected':''  }}>{{ $v->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="number_nfe" class="col-sm-4 control-label">Invoice/NF-e</label>
                                <div class="col-sm-8">
                                    <input name="number_nfe" type="text" class="form-control" value="{{ Input::get('number_nfe') }}">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2 col-md-offset-10 col-xs-12">
                            <input class="btn btn-primary btn-block loading" type="submit" value="Search">
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="container-fluid p-t2 m40">
            <div class="row">
                <div class="table-responsive">
                    <table class="table table-bordered table-condensed table-hover data-table">
                        <thead>
                            <tr class="active">
                                <th>Supply Date</th>
                                <th>Invoice</th>
                                <th>NFe</th>
                                <th>Vessel</th>
                                <th>Port</th>
                                <th>R.D.A</th>
                                <th>CO-R.D.A</th>
                                <th>Status</th>
                                <th>Settled</th>
                                <th>Payment Date</th>
                                <th>R$ Value</th>
                                <th>Commission Paid</th>
                                <th class="opcoes">Options</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($invoices as $invoice)
                            <tr @if($invoice->value->pending_cost) class="alert alert-success" @endif>
                                <td>{{ $invoice->date->supply }}</td>
                                <td>{{ $invoice->number }}</td>
                                <td>{{ $invoice->nfe }}</td>
                                <td>{{ $invoice->vessel->name }}</td>
                                <td>{{ $invoice->port->name }}</td>
                                <td>{{ $invoice->rda->name }}</td>
                                <td>{{ $invoice->corda ? $invoice->corda->name : '' }}</td>
                                <td>{{ $invoice->getStatusName() }}</td>
                                <td>{{ $invoice->conclusion->name }}</td>
                                <td>{{ $invoice->date->first_chat }}</td>

                                <td align="right" class="dropdown">
                                    @if($invoice->value->pending_cost)
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">(obs)</a>
                                    &nbsp;
                                        <ul class="dropdown-menu p-t8">
                                            <li>{{ $invoice->value->pending_cost_obs }}</li>
                                        </ul>
                                    @endif
                                    {{ $invoice->value->received_value_rs }}
                                </td>

                                <td align="center">{{ $invoice->value->commission_paid ? 'Yes' : 'No' }}</td>

                                <td>
                                    <div class="btn-group btn-group-justified" role="group" aria-label="...">
                                        <div class="btn-group btn-group-xs" role="group">
                                            <a href="{{ route('historic.show', Hashids::encode($invoice->id)) }}" class="btn btn-default border-right-n loading">
                                                <span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span>
                                            </a>
                                        </div>
                                        <div class="btn-group btn-group-xs" role="group">
                                            <a href="{{ route('invoice.destroy', Hashids::encode($invoice->id)) }}" class="btn btn-default"
                                                data-token="{{ csrf_token() }}"
                                                data-request=""
                                                data-title="Are you sure?"
                                                data-message="Are you sure that you want delete this Invoice?"
                                                data-button-text="Yes, delete it!">
                                                <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
                                            </a>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    <div>Total: {!! $invoices->count() !!} results</div>
                </div>
            </div>
        </div>
    <form>
@include('footer')

@endsection
