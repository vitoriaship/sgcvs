@extends('main')

@section('title', 'Edit Dates | SGCVS - Sistema de Gestão Comercial Vitória Ship')

@section('content')

    <div class="container p-t1">
        <h3>
            <span class="font-l">Vessel:</span> {{ $invoice->vessel->name }}<br />
            <span class="font-l">Invoice:</span> {{ $invoice->number }}<br />
            <span class="font-l">NF-e:</span> {{ $invoice->nfe }}<br /><br />
        </h3>


            @include('messages')
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="row form-horizontal">

                        <div class="col-sm-4">
                            <div class="form-group row">
                                <label for="eta" class="col-sm-3 control-label">Pending Costs</label>
                                <div class="col-sm-9">
                                    Yes
                                </div>
                            </div>
                        </div>

                        <div class="col-sm-4">
                            <div class="form-group row">
                                <label for="eta" class="col-sm-3 control-label">Pending Costs Obs.</label>
                                <div class="col-sm-9">
                                    {{ $invoice->value->pending_cost_obs }}
                                </div>
                            </div>
                        </div>




                    </div>
                </div>

            </div>

    </div>

    <script type="text/javascript">document.getElementsByTagName('body')[0].className+='nopad'</script>

@endsection
