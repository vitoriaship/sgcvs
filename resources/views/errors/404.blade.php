@extends('main')

@section('title', '404: Page Not Found')

@section('content')
    <div class="container">
        <p class="text-center p-t3"><img src="/assets/images/logotipo.png" alt=""></p>
        <div class="row">
            <div class="col-md-6 col-md-offset-3">
                <div class="alert alert-danger center">
                    <h1><strong>404:</strong> Page Not Found.</h1>
                    <h4>The requested page could not found.</h4>
                </div>
                <a class="btn btn-lg btn-primary btn-block" href="{{ route('home') }}">Return to Home</a>
            </div>
        </div>
    </div>
    <script type="text/javascript">document.getElementsByTagName('body')[0].className+='bg-footer'</script>
@endsection
