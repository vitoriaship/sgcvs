// Show de Loading image in transitions
$(window).load(function() {
    // Call loading
    $('body').on('click', '.loading', function(e) {
        $(".loader").fadeIn("fast");
    });
    // Animate loader off screen
    $(".loader").fadeOut("fast");
    setTimeout(function() {
        $(".loader").fadeOut("fast")
    }, 8000);
});

$(document).ready(function() {

    // Return Port stevedore and boat information (on load)
    $(window).on('load', function() {
        var id = $('#port option:selected').val();
        if (!id) {
            $('#boat_cost').prop('disabled',true);
            $('#stevedore_cost').prop('disabled',true);

            return;
        }
        $.ajax({
            type: "GET",
            url: '/port/data/'+id,
        }).done(function(port){
            if(!port.boat) {
                $('#boat_cost').prop('disabled',true);
            } else {
                $('#boat_cost').prop('disabled',false);
            }

            if(!port.stevedore) {
                $('#stevedore_cost').prop('disabled',true);
            } else {
                $('#stevedore_cost').prop('disabled',false);
            }
        }).fail(function() {
            $('#boat_cost').prop('disabled',true);
            $('#stevedore_cost').prop('disabled',true);
        });
    });

    // Return Port stevedore and boat information
    $('body').on('change', '#port', function(e) {
        var id = $('#port option:selected').val();
        $.ajax({
            type: "GET",
            url: '/port/data/'+id,
        }).done(function(port){
            if(!port.boat) {
                $('#boat_cost').prop('disabled',true);
            } else {
                $('#boat_cost').prop('disabled',false);
            }

            if(!port.stevedore) {
                $('#stevedore_cost').prop('disabled',true);
            } else {
                $('#stevedore_cost').prop('disabled',false);
            }
        });
    });

    // E-mail confirmation checkbox
    $('body').on('change', 'input[name=supply]', function(e) {
        if($('input[name=supply]').val()) {
            $('#confirmation').attr('checked', true);
        }
    });


    // DataTabels instance
    $.fn.dataTable.moment( 'D/M/YYYY' );
    $('.data-table').DataTable({
        paging: false,
        stateSave: true,
        info: false,
        dom: 'Bfrtip',
        aaSorting: [], // o padrão é ordernar pela primeira coluna, desativei
        language: {
            emptyTable: "No results"
        },
        buttons: [
            'copy',
            'excel', {
                extend: 'pdfHtml5',
                orientation: 'landscape',
                pageSize: 'LEGAL'
            },
            'print',
            'colvis',
            {
                extend: "csv",
                text: "Charge format",
                fieldSeparator: ";"
            }
        ]
    });

    // Table for Reports to calculate the values on bottom
    $('#report').DataTable({
        stateSave: true,
        paging: false,
        dom: 'Bfrtip',
        language: {
            emptyTable: "No results"
        },
        buttons: [
            'copy',
            'excel', {
                extend: 'pdfHtml5',
                orientation: 'landscape',
                pageSize: 'LEGAL'
            },
            'print',
            'colvis'
        ],
        footerCallback: function ( row, data, start, end, display ) {
            var usd = this.api(), data;
            var brl = this.api(), data;

            // Remove the formatting to get integer data for summation
            var intVal = function ( i ) {
                return typeof i === 'string' ?
                    i.replace(/[\$,]/g, '')*1 :
                    typeof i === 'number' ?
                        i : 0;
            };

            // Total over all pages US$
            total = usd
                .column( 7 )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );

            // Total over this page US$
            pageTotal = usd
                .column( 7, { page: 'current'} )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );

            // Update footer US$
            $( usd.column( 11 ).footer() ).html(
                pageTotal.toFixed(2)
            );

            // Total over all pages R$
            total = brl
                .column( 14 )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );

            // Total over this page R$
            pageTotal = brl
                .column( 14, { page: 'current'} )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );

            // Update footer R$
            $( brl.column( 14 ).footer() ).html(
                pageTotal.toFixed(2)
            );
        }
    });

    $( ".date" ).datepicker({
        dateFormat: "dd/mm/yy",
        changeYear: true,
        changeMonth: true,
        numberOfMonths: 2,
        onClose: function( selectedDate ) {
            $( ".date" ).datepicker( "option", selectedDate );
        }
    });


    // Iframe
    $(".iframe").fancybox({
        closeBtn : true,
        padding : 0,
        tpl: {
            prev : '<a class="f-direita" href="javascript:;"><span></span></a>',
            next : '<a class="f-esquerda" href="javascript:;"><span></span></a>',
        },
        iframe : {
            scrolling : 'no'
        },
        maxWidth : 700,
        maxHeight : 400,
        mouseWheel : false,
        scrollOutside: true,
        fitToView : false,
        width : '100%',
        height : '100%',
        autoSize : false,
        closeClick : false,
        openEffect : 'fade',
        closeEffect : 'fade',
        helpers : {
            keyboard : null,
            overlay : {
                opacity : 0.8, // or the opacity you want
                css : { 'background': 'rgba(0, 0, 0, 0.8)' }// or your preferred hex color value
            } // overlay
        }, // helpers
        afterClose: function () { // reload de page to update results
            parent.location.reload(true);
        }
    });

    // Iframe
    $(".iframe2").fancybox({
        closeBtn : true,
        padding : 0,
        tpl: {
            prev : '<a class="f-direita" href="javascript:;"><span></span></a>',
            next : '<a class="f-esquerda" href="javascript:;"><span></span></a>',
        },
        iframe : {
            scrolling : 'no'
        },
        maxWidth : 700,
        maxHeight : 600,
        mouseWheel : false,
        scrollOutside: true,
        fitToView : false,
        width : '100%',
        height : '100%',
        autoSize : false,
        closeClick : false,
        openEffect : 'fade',
        closeEffect : 'fade',
        helpers : {
            keyboard : null,
            overlay : {
                opacity : 0.8, // or the opacity you want
                css : { 'background': 'rgba(0, 0, 0, 0.8)' }// or your preferred hex color value
            } // overlay
        }, // helpers
        afterClose: function () { // reload de page to update results
            parent.location.reload(true);
        }
    });

    // Máscaras
    $('.cpf').mask("999.999.999-99");
    $('.cell').mask('(99) 99999-9999');
    $('.phone').mask('(99) 9999-9999');
    $('.plaque').mask("AAA-9999");
    $('.date').mask("00/00/0000");
    $('.time').mask("00:00");

    $('.money').maskMoney({thousands:'', decimal:'.'});
    $('.moneyNegative').maskMoney({thousands:'', decimal:'.', allowNegative:true});

    $('body').on('keyup', '.uppercase', function() {
        this.value = this.value.toUpperCase();
    });


    // put select discount in bold if selected is 1
    var discount_1 = $("[name=discount_1]");
    var discount_2 = $("[name=discount_2]");

    discount_1.change(function() {
        verifyBold(discount_1);
    });

    discount_2.change(function() {
        verifyBold(discount_2);
    });

    function verifyBold(seletor) {
        if(seletor.val() == 1) {
            seletor.addClass('selectNegrito');
        } else {
            seletor.removeClass('selectNegrito');
        }
    }

    $(window).on('load', function() {

        // primeiro converto os selects para select2
        $("select:not(#discount_1):not(#discount_2)").select2({
            language: "pt-br",
            placeholder: 'Select...', // Place holder text to place in the select
            minimumResultsForSearch: 15, // Overrides default of 15 set above
            allowClear: true
        });

        // agora tento alterar a cor do desconto zero
        verifyBold(discount_1);
        verifyBold(discount_2);
    });


    // em invoice, irei preencher os valores sugeridos para payment_terms, discount_1 e discount_2
    $("#rda").change(function() {
        var rda_id = $(this).val();

        $.ajax({
            type: "GET",
            url: '/rda/json/'+rda_id,
        }).done(function(json){
            $("[name=payment]").val(json.payment_terms);
            if(json.discount_1) {
                $("#discount_1").val(json.discount_1).trigger('change');
            }

            if(json.discount_2) {
                $("#discount_2").val(json.discount_2).trigger('change');
            }
            console.log('rda json discount:', json);
        });
    });

    // select 2
    var pesquisa = function (params, data) {
        // If there are no search terms, return all of the data
        if ($.trim(params.term) === '') {
            return data;
        }

        // `params.term` should be the term that is used for searching
        // `data.text` is the text that is displayed for the data object
        if (data.text.toLowerCase().startsWith(params.term.toLowerCase())) {
            var modifiedData = $.extend({}, data, true);
            //modifiedData.text += ' (matched)';

            // You can return modified objects from here
            // This includes matching the `children` how you want in nested data sets
            return modifiedData;
        }

        // Return `null` if the term should not be displayed
        return null;
    };


    // on-line
    setInterval(function() {
        console.log('online');
        $.ajax({
            type: "GET",
            url: '/user/registerActivity',
        }).done(function(response) { });
    }, 5000);


    // checkbox do botão "comissão paga"
    // página /historic/{id}
    // página /billing/{id}
    $("#commission_paid").click(function() {
        $('#saveCommissionPaid').submit();
    })


    // tela billing, tem $, passou o mouse em cima mostra a obs de valor pendente
    $('.dropdown').hover(function() {
        $(this).find('.dropdown-menu').stop(true, true).delay(100).fadeIn(50);
    }, function() {
        $(this).find('.dropdown-menu').stop(true, true).delay(100).fadeOut(50);
    });

    // tela billing - alterou datas, calcular altomaticmente o first chat
    /**************************************************************************8*/
    $('.calculateFirstChat').change(function() {
        var supply = $('input[name=supply]').val();
        var payment = $('input[name=payment]').val();
        var payment_date = $('input[name=payment_date]').val();
        var first_chat = addDays(supply, payment);

        if(payment_date) {
            $('input[name=first_chat]').val(payment_date);
        } else {
            $('input[name=first_chat]').val(first_chat);
        }
    });

    function addDays(date, days) {
        var moment_date = moment(date, "DD/MM/YYYY").add(days, 'days');
        return moment_date.format("DD/MM/YYYY");
    }
    /**************************************************************************8*/
});
