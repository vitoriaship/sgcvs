new Vue ({
    el: '#app',
    data: {
        gross_1: 0.00,
        discount_1: 0.00,
        val_discount_1: 0.00,
        gross_2: 0.00,
        discount_2: 0.00,
        val_discount_2: 0.00,
        total: 0.00,
        wo: 0.00,
        boat_cost: 0.00,
        stevedore_cost: 0.00,
        transport: 0.00,
        received: 0.00,
        out_balance: 0.00,
        boat_cost_real: 0.00,
        stevedore_cost_real: 0.00,
        freight: 0.00,
        purch_value_nfe: 0.00,
        extra_value: 0.00,

        cost_extra_value: 0.00,
    },
    computed: {
        val_discount_1: function () {
            return this.gross_1 * this.discount_1;
        },
        val_discount_2: function () {
            return this.gross_2 * this.discount_2;
        },
        total: function () {
            return (this.val_discount_1 * 1) +
                   (this.val_discount_2 * 1) +
                   (this.boat_cost * 1) +
                   (this.stevedore_cost * 1) +
                   (this.transport * 1) +
                   (this.extra_value * 1) +
                   (this.wo * 1);
        },
        out_balance: function () {
            return (this.received * 1) - (this.total * 1);
        },
        total_approval: function () {
            return (this.boat_cost_real * 1) +
                   (this.stevedore_cost_real * 1) +
                   (this.freight * 1) +
                   (this.purch_value_nfe * 1) +
                   (this.cost_extra_value * 1);
        }
    },
});
