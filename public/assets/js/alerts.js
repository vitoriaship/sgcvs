var alert = {

    linkSelector          : "a[data-request]",
    modalTitle            : "Are you sure?",
    modalMessage          : "You will not be able to recover this entry?",
    modalConfirmButtonText: "Yes, delete it!",
    type                  : "warning",
    method                : "DELETE",
    token                 : null,
    url                   : "/",

    init: function() {
        $(this.linkSelector).on('click', {self:this}, this.handleClick);
    },

    handleClick: function(event) {
        event.preventDefault();

        var self = event.data.self;
        var link = $(this);

        self.modalTitle             = link.data('title') || self.modalTitle;
        self.modalMessage           = link.data('message') || self.modalMessage;
        self.modalConfirmButtonText = link.data('button-text') || self.modalConfirmButtonText;
        self.method                 = link.data('method') || self.method;
        self.type                   = link.data('type')  || self.type;
        self.url                    = link.attr('href');
        self.token                  = link.data('token');

        self.confirmRequest();
    },

    confirmRequest: function() {
        swal({
                title             : this.modalTitle,
                text              : this.modalMessage,
                type              : this.type,
                showCancelButton  : true,
                confirmButtonColor: this.method == "DELETE" ? "#DD6B55" : "#337AB7",
                confirmButtonText : this.modalConfirmButtonText,
                closeOnConfirm    : true
            },
            function() {
                this.makeRequest()
            }.bind(this)
        );
    },

    makeRequest: function() {
        var form =
            $('<form>', {
                'method': 'POST',
                'action': this.url
            });

        var token =
            $('<input>', {
                'type': 'hidden',
                'name': '_token',
                'value': this.token
            });

        var hiddenInput =
            $('<input>', {
                'name': '_method',
                'type': 'hidden',
                'value': this.method
            });

        return form.append(token, hiddenInput).appendTo('body').submit();
    }
};

alert.init();
