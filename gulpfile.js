'use strict'

let gulp = require('gulp')
let revReplace = require('gulp-rev-replace')
let useref = require('gulp-useref')
let filter = require('gulp-filter')
let uglify = require('gulp-uglify')
let minifyCss = require('gulp-minify-css')
let csso = require('gulp-csso')
let clean = require('gulp-clean')
let imagemin = require('gulp-imagemin')
let flatten = require('gulp-flatten')
let watch = require('gulp-watch')
let batch = require('gulp-batch')

const distDir = './public/dist'
const assetsDir = './public/assets'

gulp.task('bundle', ['clean'], () => {
    let jsFilter = filter("**/*.js", { restore: true })
    let cssFilter = filter("**/*.css", { restore: true })

    return gulp.src('./resources/views/main.blade.php')
        .pipe(useref({ searchPath: assetsDir }))
        .pipe(jsFilter)
        .pipe(uglify())
        .pipe(jsFilter.restore)
        .pipe(cssFilter)
        .pipe(csso())
        .pipe(cssFilter.restore)
        .pipe(gulp.dest(distDir))
})

// Copy and optimize the images
gulp.task('images', ['clean'], () => {
    gulp.src(`${assetsDir}/images/*`)
        .pipe(imagemin({ optimizationLevel: 5 }))
        .pipe(gulp.dest(`${distDir}/images`))

    gulp.src([
            `${assetsDir}/css/images/*`,
            `${assetsDir}/css/fancy/**/*.png`,
            `${assetsDir}/css/fancy/**/*.gif`
        ])
        .pipe(imagemin({ optimizationLevel: 5 }))
        .pipe(gulp.dest(`${distDir}/css`))

    gulp.src(`${assetsDir}/css/images/*`)
        .pipe(imagemin({ optimizationLevel: 5 }))
        .pipe(gulp.dest(`${distDir}/css/images`))
})

// Copy fonts
gulp.task('fonts', ['clean'], () => {
    return gulp.src([
        `${assetsDir}/**/fonts/*`,
        `${assetsDir}/css/font/*`,
    ])
      .pipe(flatten())
      .pipe(gulp.dest(`${distDir}/fonts`))
})

// Clean dist folder
gulp.task('clean', () => {
    return gulp.src(distDir, { read: false })
        .pipe(clean({ force: true }))
})

// Delete blade file
gulp.task('clean-blade', ['bundle'], () => {
    return gulp.src(`${distDir}/main.blade.php`, { read: false })
        .pipe(clean({ force: true }))
})

// Watch changes in script files
gulp.task('watch', () => {
    watch([
            'public/assets/**/*.js',
            'public/assets/**/*.css',
        ],
        batch((events, done) => {
            gulp.start('bundle', done)
        })
    )
})

// Default task
gulp.task('default', ['clean', 'bundle', 'images', 'fonts', 'clean-blade'])
