<?php

namespace App\Listeners;

use App\Events\InvoiceUpdated;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use Mail;

class SendEmails
{
    /**
     * Handle the event.
     *
     * @param  InvoiceUpdated  $event
     * @return void
     */
    public function handle(InvoiceUpdated $event)
    {
        $i = $event->invoice;

        // to e cc
        Mail::send('emails.confirm', ['invoice' => $i], function ($m) use($i) {
            // from
            if (auth()->user()->locale->id == 2) {
                $m->from('supplier.rio@vitoriaship.com.br', 'Vitoria Ship Supplier');
                $m->cc('supplier.rio@vitoriaship.com.br', 'Vitoria Ship Supplier');
            } else {
                $m->from('vitoriaship@vitoriaship.com.br', 'Vitoria Ship Supplier');
                $m->cc('vitoriaship@vitoriaship.com.br', 'Vitoria Ship Supplier');
                $m->bcc('sitevix@vitoriaship.com.br', 'Vitoria Ship Supplier');
            }

            // para - embarcação
            $m->to($i->vessel->email, $i->vessel->name);

            // com cópia para o rda
            $m->cc($i->rda->email, $i->rda->name);

            // bcc
            if ($i->corda) $m->bcc($i->corda->email, $i->corda->name);

            // titulo
            $m->subject('VITORIA SHIP CONFIRMATION OF SUPPLY MV '.$i->vessel->name);
        });
    }
}
