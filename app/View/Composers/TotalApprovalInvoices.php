<?php

namespace App\View\Composers;

use App\Models\Invoice;
use Illuminate\View\View;

class TotalApprovalInvoices
{
    public function compose(View $view)
    {
        $totalInvoiceLs = Invoice::where('stage', '=', 2)
            ->where('locale_id', '=', auth()->user()->locale_id)
            ->count();

        // Get all invoices to Billing approval
        $totalInvoiceBs = Invoice::where('stage', '=', 4)->count();

        $data = [
            'totalInvoiceLs' => $totalInvoiceLs,
            'totalInvoiceBs' => $totalInvoiceBs
        ];

        $view->with('totalApprovalInvoices', (object) $data);
    }
}
