<?php

namespace App\View\Composers;

use App\Models\Config;
use Illuminate\View\View;

class LineUpLastedUpdated
{
    public function compose(View $view)
    {
        $keyLineUpUpdatedIn = 'lineup-date-'.auth()->user()->locale_id;
        $lineup = Config::where('key', $keyLineUpUpdatedIn)->orderBy('created_at', 'desc')->first();
        $view->with(compact('lineup'));
    }
}
