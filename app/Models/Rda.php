<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Rda extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'email', 'type', 'payment_terms', 'discount_1', 'discount_2'];

    /**
     * Relationship with Invoice Model
     */
    public function invoice()
    {
        return $this->hasMany('App\Models\Invoice');
    }

    /**
     * Only user of group 1 and 2 can see the RDA/cord
     * name, another ones just see "SIGN" or "COB"
     */
    public function getNameAttribute($value)
    {
        if (in_array(auth()->user()->group->id, [1, 2, 5]))
        {
            return $value;
        }

        return $this->attributes['type'] == 1 ? 'SIGN' : 'COB';
    }

    /**
     * Set value to uppercase
     */
    public function setNameAttribute($value)
    {
        $this->attributes['name'] = strtoupper($value);
    }

    public function setEmailAttribute($value)
    {
        $this->attributes['email'] = strtolower($value);
    }
}
