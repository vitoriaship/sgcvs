<?php

namespace App\Models;

use App\Functions\Number;
use Illuminate\Database\Eloquent\Model;

class Value extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'invoice_id',
        'gross_1',
        'discount_1',
        'gross_2',
        'discount_2',
        'wo',
        'received',
        'reference',
        'received_value_rs',
        'commission',
        'commission_paid', // added in 25/07/2017
        'payment_remarks',
        'boat_cost',
        'stevedore_cost',
        'transport',
        'boat_cost_real',
        'stevedore_cost_real',
        'purch_value_nfe',
        'freight',
        'pending_cost',
        'pending_cost_obs',

        'extra_value_bool',
        'extra_value_label',
        'extra_value',

        'cost_extra_value_label',
        'cost_extra_value'


    ];

    /**
     * Relationship with Invoice Model
     */
    public function invoice()
    {
        return $this->belongsTo('App\Models\Invoice');
    }

    /**
     * format the numbers
     */

    public function getGross1Attribute($value)
    {
        return Number::getFormatNumber($value);
    }

    public function getGross2Attribute($value)
    {
        return Number::getFormatNumber($value);
    }

    public function getWoAttribute($value)
    {
        return Number::getFormatNumber($value);
    }

    public function getReceivedAttribute($value)
    {
        return Number::getFormatNumber($value);
    }

    public function getReferenceAttribute($value)
    {
        return Number::getFormatNumber($value);
    }

    public function getReceivedValueRsAttribute($value)
    {
        return Number::getFormatNumber($value);
    }

    public function getCommissionAttribute($value)
    {
        return Number::getFormatNumber($value);
    }

    public function getBoatCostAttribute($value)
    {
        return Number::getFormatNumber($value);
    }

    public function getStevedoreCostAttribute($value)
    {
        return Number::getFormatNumber($value);
    }

    public function getTransportAttribute($value)
    {
        return Number::getFormatNumber($value);
    }

    public function getBoatCostRealAttribute($value)
    {
        return Number::getFormatNumber($value);
    }

    public function getStevedoreCostRealAttribute($value)
    {
        return Number::getFormatNumber($value);
    }

    public function getPurchValueNfeAttribute($value)
    {
        return Number::getFormatNumber($value);
    }

    public function getFreightAttribute($value)
    {
        return Number::getFormatNumber($value);
    }

    public function getExtraValueAttribute($value)
    {
        return Number::getFormatNumber($value);
    }

    public function getCostExtraValueAttribute($value)
    {
        return Number::getFormatNumber($value);
    }




    /**
     * Return the sum of Operational Charges
     */
    public function getOperationalChargesAttribute($value)
    {
        $total = (
            $this->attributes['stevedore_cost'] +
            $this->attributes['boat_cost'] +
            $this->attributes['transport']

        );

        return Number::getFormatNumber($total);
    }

    /**
     * Return the sum of Operational Charges
     */
    public function getTotalRealAttribute($value)
    {
        $total = (
            $this->attributes['stevedore_cost_real'] +
            $this->attributes['boat_cost_real'] +
            $this->attributes['freight'] +
            $this->attributes['purch_value_nfe'] +
            $this->attributes['cost_extra_value']
        );

        return Number::getFormatNumber($total);
    }

    /**
     * Return the sum of Out Balance
     */
    public function getTotalAttribute($value)
    {
        $total_discount_1 = $this->attributes['gross_1'] * $this->attributes['discount_1'];
        $total_discount_2 = $this->attributes['gross_2'] * $this->attributes['discount_2'];

        $total = (
            $total_discount_1 +
            $total_discount_2 +
            $this->attributes['stevedore_cost'] +
            $this->attributes['boat_cost'] +
            $this->attributes['transport'] +
            $this->attributes['wo']+
            $this->attributes['extra_value']
            //+ $this->attributes['cost_extra_value'] // cost extra value nao entra aqui
        );

        return Number::getFormatNumber($total);
    }

    /**
     * Return the sum of Out Balance
     */
    public function getOutBalanceAttribute($value)
    {
        $total_discount_1 = $this->attributes['gross_1'] * $this->attributes['discount_1'];
        $total_discount_2 = $this->attributes['gross_2'] * $this->attributes['discount_2'];

        $total = (
            $total_discount_1 +
            $total_discount_2 +
            $this->attributes['stevedore_cost'] +
            $this->attributes['boat_cost'] +
            $this->attributes['transport'] +
            $this->attributes['wo'] +
            $this->attributes['extra_value']
            //+ $this->attributes['cost_extra_value'] // cost extra value nao entra aqui
        );

        $out_balance = $this->attributes['received'] - $total;

        //return number_format($out_balance, 2, '.', '');
        return Number::getFormatNumber($out_balance);
    }

    /**
     * Value of discounts
     */
     public function getValueDisc1Attribute($value)
     {
         $total = $this->attributes['gross_1'] * $this->attributes['discount_1'];

         return Number::getFormatNumber($total);
     }

     public function getValueDisc2Attribute($value)
     {
         $total = $this->attributes['gross_2'] * $this->attributes['discount_2'];

         return Number::getFormatNumber($total);
     }

     /**
      * Return percent discount
      */
     public function getPercent1Attribute($value)
     {
         $total = 100 - ($this->attributes['discount_1'] * 100);

         return $total;
     }

     public function getPercent2Attribute($value)
     {
         $total = 100 - ($this->attributes['discount_2'] * 100);

         return $total;
     }
}
