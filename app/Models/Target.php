<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Target extends Model
{
    public $table = 'targets';
    protected $fillable = [
        'locale_id', 'month', 'year', 'value'
    ];


    // locale
    public function locale()
    {
        return $this->hasOne(Locale::class, 'id', 'locale_id');
    }
    

}
