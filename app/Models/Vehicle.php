<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Vehicle extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['model', 'plaque', 'locale_id'];

    /**
     * Relationship with Invoice Model
     */
    public function invoice()
    {
        return $this->hasMany('App\Models\Invoice');
    }

    /**
     * Set value to uppercase
     */
    public function setPlaqueAttribute($value)
    {
        $this->attributes['plaque'] = strtoupper($value);
    }

    /**
     * Relationship with Invoice Locale
     */
    public function locale()
    {
        return $this->belongsTo('App\Models\Locale');
    }
}
