<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Conclusion extends Model
{
    /**
     * Relationship with Invoice Model
     */
    public function invoice()
    {
        return $this->hasMany('App\Models\Invoice');
    }
}
