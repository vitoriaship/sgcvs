<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Invoice extends Model
{
    /**
     * The relations to eager load on every query.
     *
     * @var array
     */
    protected $with = [
        'user',
        'date',
        'locale',
        'vessel',
        'rda',
        'corda',
        'agency',
        'port',
        'value',
        'conclusion',
        'status',
        'remark',
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'locale_id',
        'vessel_id',
        'rda_id',
        'corda_id',
        'payment',
        'agency_id',
        'port_id',
        'employer1_id',
        'employer2_id',
        'employer3_id',
        'vehicle_id',
        'state_id',
        'conclusion_id',
        'status_id',
        'stage',
        'number',
        'nfe',
        'groups',
        'lawyer',
        'under_process',
        'obs',
    ];

    /**
     * Relationship with User Model
     */
    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

    /**
     * Relationship with locale Model
     */
    public function locale()
    {
        return $this->belongsTo('App\Models\Locale');
    }

    /**
     * Relationship with User Model
     */
    public function agency()
    {
        return $this->belongsTo('App\Models\Agency');
    }

    /**
     * Relationship with Vessel Model
     */
    public function vessel()
    {
        return $this->belongsTo('App\Models\Vessel');
    }

    /**
     * Relationship with Rda Model
     */
    public function rda()
    {
        return $this->belongsTo('App\Models\Rda');
    }

    /**
     * Relationship with Rda Model (CO-R.D.A)
     */
    public function corda()
    {
        return $this->belongsTo('App\Models\Rda');
    }

    /**
     * Relationship with Port Model
     */
    public function port()
    {
        return $this->belongsTo('App\Models\Port');
    }

    /**
     * Relationship with Vehicle Model
     */
    public function vehicle()
    {
        return $this->belongsTo('App\Models\Vehicle');
    }

    /**
     * Relationship with Employer Model
     */
    public function employerOne()
    {
        return $this->belongsTo('App\Models\Employer', 'employer1_id');
    }

    /**
     * Relationship with Employer Model
     */
    public function employerTwo()
    {
        return $this->belongsTo('App\Models\Employer', 'employer2_id');
    }

    /**
     * Relationship with Employer Model
     */
    public function employerThree()
    {
        return $this->belongsTo('App\Models\Employer', 'employer3_id');
    }

    /**
     * Relationship with Remark Model
     */
    public function remark()
    {
        return $this->hasOne('App\Models\Remark');
    }

    /**
     * Relationship with Remark Model
     */
    public function value()
    {
        return $this->hasOne('App\Models\Value');
    }

    /**
     * Relationship with Date Model
     */
    public function date()
    {
        return $this->hasOne('App\Models\Date');
    }

    /**
     * Relationship with Status Model
     */
    public function status()
    {
        return $this->belongsTo('App\Models\Status');
    }

    /**
     * Relationship with Conclusion Model
     */
    public function conclusion()
    {
        return $this->belongsTo('App\Models\Conclusion');
    }

    /**
     * Set value to uppercase
     */
    public function setGroupsAttribute($value)
    {
        $this->attributes['groups'] = strtoupper($value);
    }

    /**
     * Set Id null if aren't sent values.
     */
    public function setCordaIdAttribute($value)
    {
        $this->attributes['corda_id'] = $value ?: null;
    }

    public function setEmployer1IdAttribute($value)
    {
        $this->attributes['employer1_id'] = $value ?: null;
    }

    public function setEmployer2IdAttribute($value)
    {
        $this->attributes['employer2_id'] = $value ?: null;
    }

    public function setEmployer3IdAttribute($value)
    {
        $this->attributes['employer3_id'] = $value ?: null;
    }

    public function setVehicleIdAttribute($value)
    {
        $this->attributes['vehicle_id'] = $value ?: null;
    }

    public function getStatusName()
    {
        // stages
        // 1- line-up
        // 2- approval line-up
        // 3- billing
        // 4- approval billing
        // 5- historic

        // status
        // 1- not supplied
        // 2- supplied
        // 3- cancelled

        // conclusions
        // 1- no
        // 2- yes
        // 3- yes with +
        // 4- yes with -
        // 5-defrauded

        if($this->status->id == 1) {
            return 'NOT SUPPLIED';
        } else if($this->status->id == 3) {
            return 'CANCELLED';
        } else {
            switch ($this->conclusion->id) {
                case 1: // conclusion no
                    return 'SUPPLIED';
                    break;
                case 2: // conclusion yes
                case 3: // conclusion yes with +
                case 4: // conclusion yes with -
                    return 'PAID';
                    break;
                case 5: // defrauded
                    return 'DEFRAUDED';
                    break;
            }
        }

        return '';
    }
}
