<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Config extends Model
{
    public $table = 'configs';
    public $timestamps = true;
    protected $fillable = [
        'user_id', 'key', 'value', 'created_at', 'updated_at'
    ];


    // user
    public function locale()
    {
        return $this->hasOne(User::class, 'id', 'user_id');
    }
    

}
