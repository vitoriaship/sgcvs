<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SessionDetail extends Model
{
    protected $table = 'sessions_details';
    protected $fillable = ['user_id', 'session_id', 'date'];
    public $timestamps = false;

    static $considerTimeActiveSession = 15; // seconds

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
