<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Locale extends Model
{
    /**
     * Relationship with User Model
     */
    public function user()
    {
        return $this->hasMany('App\Models\User');
    }

    /**
     * Relationship with Invoice Model
     */
    public function invoice()
    {
        return $this->hasMany('App\Models\Invoice');
    }

    /**
     * Relationship with Agency
     */
    public function agency()
    {
        return $this->hasMany('App\Models\Agency');
    }

    /**
     * Relationship with Port
     */
    public function port()
    {
        return $this->hasMany('App\Models\Port');
    }

    /**
     * Relationship with Employer
     */
    public function employer()
    {
        return $this->hasMany('App\Models\Employer');
    }

    /**
     * Relationship with Vehicle
     */
    public function vehicle()
    {
        return $this->hasMany('App\Models\Vehicle');
    }
}
