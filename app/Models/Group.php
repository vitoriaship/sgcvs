<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Group extends Model
{
    protected $fillable = ['id', 'name'];
    public $timestamps = false;


    /**
     * Relationship with User Model
     */
    public function user()
    {
        return $this->hasMany('App\Models\User');
    }
}
