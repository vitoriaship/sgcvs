<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Agency extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'responsible', 'telephone', 'cellphone', 'locale_id'];

    /**
     * Relationship with Invoice Model
     */
    public function invoice()
    {
        return $this->hasMany('App\Models\Invoice');
    }

    /**
     * Relationship with Invoice Locale
     */
    public function locale()
    {
        return $this->belongsTo('App\Models\Locale');
    }

    /**
     * Set value to uppercase
     */
    public function setNameAttribute($value)
    {
        $this->attributes['name'] = strtoupper($value);
    }

    public function setResponsibleAttribute($value)
    {
        $this->attributes['responsible'] = strtoupper($value);
    }

}
