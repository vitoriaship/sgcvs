<?php

namespace App\Functions;

class Number
{
    // funções para tratar numeros
    public static function getFormatNumber($value)
    {
        return number_format($value, 2, '.', '');
    }

    public static function getBrazillianFormatNumber($value)
    {
        return number_format($value, 2, ',', '.');
    }

    public static function setFormatNumber($value)
    {
        // aqui temos 2 opções:
        // vai vir um float do banco, nunca terá ,
        // vai vir formatado do front end, com virgula, assim sendo tem que remover a virgula
        if (strpos($value, ',')) {
            $value = str_replace('.', '', $value);
            $value = str_replace(',', '.', $value);

            return $value;
        } else {
            return $value;
        }
    }
}
