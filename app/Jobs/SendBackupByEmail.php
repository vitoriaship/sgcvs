<?php

namespace App\Jobs;

use App\Jobs\Job;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Bus\SelfHandling;
use Illuminate\Contracts\Queue\ShouldQueue;
use Mail;

class SendBackupByEmail extends Job implements SelfHandling, ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    protected $file;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($file)
    {
        $this->file = $file;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        Mail::send('emails.backup', ['file' => $this->file], function ($message) {
            $message->from('vitoriaship@vitoriaship.com.br', 'Vitoria Ship Supplier');
            $message->to('vitoriaship@vitoriaship.com.br', 'Vitoria Ship Supplier');
            $message->subject('Database Backup ' . date('Y-m-d', time()));
            $message->attach($this->file);
        });
    }
}
