<?php

namespace App\Http\Requests\Rda;

use App\Http\Requests\Request;
use App\Models\Rda;

class DeleteRdaRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $rda = Rda::findOrFail($this->route('rda'));

        if (!$rda->invoice->isEmpty())
        {
            return false;
        }

        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function forbiddenResponse()
    {
        return redirect()->back()->withErrors([
            'error' => 'You can not delete RDA/CO-RDA with ivoices. Please delete all invoices related to this RDA.'
        ]);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }
}
