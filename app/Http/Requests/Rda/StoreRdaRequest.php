<?php

namespace App\Http\Requests\Rda;

use App\Http\Requests\Request;

class StoreRdaRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'  => 'required|max:45|unique:rdas,name,'.$this->route('rda'),
            'email' => 'required|email',
            'type'  => 'required'
        ];
    }
}
