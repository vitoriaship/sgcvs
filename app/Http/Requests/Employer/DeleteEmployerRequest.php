<?php

namespace App\Http\Requests\Employer;

use App\Http\Requests\Request;
use App\Models\Employer;

class DeleteEmployerRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $employer = Employer::findOrFail($this->route('employer'));

        if (!$employer->invoice->isEmpty())
        {
            return false;
        }

        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function forbiddenResponse()
    {
        return redirect()->back()->withErrors([
            'error' => 'You can not delete employer with ivoices. Please delete all invoices related to this employer.'
        ]);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }
}
