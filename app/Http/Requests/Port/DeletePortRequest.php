<?php

namespace App\Http\Requests\Port;

use App\Http\Requests\Request;
use App\Models\Port;

class DeletePortRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $port = Port::findOrFail($this->route('port'));

        if (!$port->invoice->isEmpty()) {
            return false;
        }

        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function forbiddenResponse()
    {
        return redirect()->back()->withErrors([
            'error' => 'You can not delete port with invoices. Please delete all invoices related to this port.'
        ]);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }
}
