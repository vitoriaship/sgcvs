<?php

namespace App\Http\Requests\Port;

use App\Http\Requests\Request;

class StorePortRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'      => 'required|unique:ports,name,'.$this->route('port').',id,locale_id,'.auth()->user()->locale_id,
            'state_id'  => 'required',
            'stevedore' => 'required',
            'boat'      => 'required',
        ];
    }
}
