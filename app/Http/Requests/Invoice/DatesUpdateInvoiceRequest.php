<?php

namespace App\Http\Requests\Invoice;

use App\Http\Requests\Request;

class DatesUpdateInvoiceRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'eta' => 'required|date_format:d/m/Y',
            'etb' => 'date_format:d/m/Y',
            'ets' => 'date_format:d/m/Y',
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'eta.date_format' => 'Invalid date format, must be dd/mm/yyyy.',
            'etb.date_format' => 'Invalid date format, must be dd/mm/yyyy.',
            'ets.date_format' => 'Invalid date format, must be dd/mm/yyyy.',
        ];
    }
}
