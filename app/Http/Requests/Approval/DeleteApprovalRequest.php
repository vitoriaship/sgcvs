<?php

namespace App\Http\Requests\Approval;

use App\Http\Requests\Request;
use App\Models\Invoice;

class DeleteApprovalRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function forbiddenResponse()
    {
        return redirect()->back()->withErrors([
            'error' => 'You can not delete this invoice.'
        ]);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }
}
