<?php

namespace App\Http\Requests\Target;

use App\Http\Requests\Request;

class TargetStoreRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'month'  => 'required|integer|min:1|max:12',
            'year' => 'required|integer|min:2014|max:2055',
            'locale_id' => 'required|integer',
            'value' => 'required|numeric'
        ];
    }
}
