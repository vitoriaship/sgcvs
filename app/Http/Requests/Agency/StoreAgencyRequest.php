<?php

namespace App\Http\Requests\Agency;

use App\Http\Requests\Request;

class StoreAgencyRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|unique:agencies,name,'.$this->route('agency').',id,locale_id,'.auth()->user()->locale_id,
        ];
    }
}
