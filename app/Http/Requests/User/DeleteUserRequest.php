<?php

namespace App\Http\Requests\User;

use App\Http\Requests\Request;
use App\Models\User;

class DeleteUserRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        if ($this->route('user') == auth()->user()->id) {
            return false;
        }

        return true;
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function forbiddenResponse()
    {
        return redirect()->back()->withErrors([
            'error' => 'You can not delete yourself.'
        ]);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }
}
