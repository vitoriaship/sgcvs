<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Support\Facades\Auth;

class Authenticate
{
    /**
     * The Guard implementation.
     *
     * @var Guard
     */
    protected $auth;

    /**
     * Create a new filter instance.
     *
     * @param  Guard  $auth
     * @return void
     */
    public function __construct(Guard $auth)
    {
        $this->auth = $auth;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        if ($this->auth->guest()) {
            if ($request->ajax()) {
                return response('Unauthorized.', 401);
            } else {
                return redirect()->guest(route('auth.login'));
            }
        } else {
            // forçando logout em um usuário especifico
            // https://laravel.io/forum/04-16-2015-logout-a-user-by-their-id-not-necessarilly-the-one-currently-logged-in
            $user = Auth::user();
            if ($user->forceLogout == true) {
                $user->forceLogout = false;
                $user->save();

                // só redireciono o cidadão se ele já estiver dentro do sistema, se ele veio do login não é necessário
                if($request->is('/') == false) {
                    return redirect(route('auth.logout'));
                }
                //$this->auth->logout();

            }
        }

        return $next($request);
    }
}
