<?php

namespace App\Http\Controllers;

use App\Http\Requests\Vessel\DeleteVesselRequest;
use App\Http\Requests\Vessel\StoreVesselRequest;
use App\Models\Vessel;
use Gate;
use Illuminate\Http\Request;

class VesselController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if (Gate::denies('vessel')) {
            return redirect(route('home'));
        }

        $query = Vessel::query();

        if ($request->search) {
            $query->where('name', 'like', '%' . $request->search . '%')
                ->orWhere('email', 'like', '%' . $request->search . '%');
        }

        $vessels = $query->orderBy('name', 'ASC')->paginate(30);

        $vessels->appends($request->input());

        return view('vessel.index', compact('vessels', 'request'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreVesselRequest $request)
    {
        if (Gate::denies('vessel')) {
            return redirect(route('home'));
        }

        $vessel = new Vessel;

        $vessel->create($request->only('name', 'email'));

        return redirect(route('vessel.index'))->with('status', 'Vessel saved with success.');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int                       $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (Gate::denies('vessel')) {
            return redirect(route('home'));
        }

        $vessel = Vessel::findOrFail($id);

        return view('vessel.edit')->with('vessel', $vessel);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int                       $id
     * @return \Illuminate\Http\Response
     */
    public function update(StoreVesselRequest $request, $id)
    {
        if (Gate::denies('vessel')) {
            return redirect(route('home'));
        }

        $vessel = Vessel::findOrFail($id);

        $vessel->fill($request->only('name', 'email'))->save();

        return redirect()->back()->with('status', 'Vessel updated with success.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int                       $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(DeleteVesselRequest $request, $id)
    {
        if (Gate::denies('vessel')) {
            return redirect(route('home'));
        }

        $vessel = Vessel::findOrFail($id);

        $vessel->delete();

        return redirect()->back()->with('status', 'Vessel deleted with success.');
    }
}
