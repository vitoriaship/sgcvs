<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Models\Group;
use App\Models\Locale;
use App\Models\SessionDetail;
use App\Models\User;
use Carbon\Carbon;
use Gate;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (Gate::denies('user')) {
            return redirect(route('home'));
        }

        $users    = User::all();
        $locales  = Locale::all();
        $groups   = Group::all();

        return view('user.index', compact('users', 'locales', 'groups'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Requests\User\StoreUserRequest $request)
    {
        if (Gate::denies('user')) {
            return redirect(route('home'));
        }

        $user                   = new User;
        $user->password_expires = Carbon::now()->addMonth(6);
        $user->fill($request->only('name', 'username', 'email', 'group_id', 'locale_id', 'password'));
        $user->save();

        return redirect(route('user.index'))->with('status', 'User saved with success.');
        ;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int                       $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (Gate::denies('user')) {
            return redirect(route('home'));
        }

        $user     = User::findOrFail($id);
        $locales  = Locale::all();
        $groups   = Group::all();

        return view('user.edit', compact('user', 'locales', 'groups'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int                       $id
     * @return \Illuminate\Http\Response
     */
    public function update(Requests\User\UpdateUserRequest $request, $id)
    {
        if (Gate::denies('user')) {
            return redirect(route('home'));
        }

        $user = User::findOrFail($id);

        /**
         *  Check if the password was changed and update it if true
         */
        if (!empty($request->input('password'))) {
            // marco usuario para logout, conforme solicitação do lorran
            $user->fill($request->all())->save();
            $user->forceLogout      = true;
            $user->password_expires = Carbon::now()->addMonth(6);
            $user->save();
        } else {
            $user->fill($request->except('password'))->save();
        }

        return redirect()->back()->with('status', 'User updated with success.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int                       $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Requests\User\DeleteUserRequest $request, $id)
    {
        if (Gate::denies('user')) {
            return redirect(route('home'));
        }

        $user = User::findOrFail($id);

        if (!$user->invoice->isEmpty()) {
            return redirect()->back()->withErrors([
                'error' => 'This user has invoices related to him,
                you must delete all related invoices to proceed.'
            ]);
        }

        $user->delete();

        return redirect()->back()->with('status', 'User deleted with success.');
    }

    public function editPassword()
    {
        $user = auth()->user();

        return view('user.editPassword', compact('user'));
    }

    public function updatePassword(Requests\User\UpdateUserPasswordRequest $request)
    {
        $user                   = auth()->user();
        $user->password         = $request->password;
        $user->password_expires = Carbon::now(config('app.timezone'))->addMonth(6);
        $user->save();

        if ($user->group->id == 4) {
            return redirect(route('operational'))->with('status', 'Senha alterada com sucesso');
        } else {
            return redirect(route('invoice.index'))->with('status', 'Senha alterada com sucesso');
            ;
        }
    }

    public function registerActivity(Request $request)
    {
        // dados
        $session_id = $request->getSession()->getId();
        $user_id    = auth()->user()->id;
        $date       = Carbon::now();

        // deleto registros antigos
        $time = SessionDetail::$considerTimeActiveSession + 1;
        SessionDetail::whereRaw('(UNIX_TIMESTAMP(now()) - UNIX_TIMESTAMP(date)) > ?', [$time])->delete();

        // verifico se já registrei o acesso
        $session = SessionDetail::where('session_id', $session_id)->first();
        if (!$session) {
            $session = new SessionDetail();
        }
        $session->fill(compact('session_id', 'user_id', 'date'))->save();

        return ['ok'];
    }

    public function activity()
    {
        DB::connection()->enableQueryLog();

        $sessions = SessionDetail::select('user_id', DB::raw('count(*) as total'))
            ->whereRaw('(UNIX_TIMESTAMP(now()) - UNIX_TIMESTAMP(date)) < ?', [SessionDetail::$considerTimeActiveSession])
            ->groupBy('user_id')
            ->get();

        return view('user.activity', compact('sessions'));
    }

    public function activity2()
    {
        $sessions = DB::table('sessions')
            ->orderBy('last_activity', 'desc')
            ->get();

        $sessions_array = [];

        foreach ($sessions as $session) {
            // get data from payload
            $payload = unserialize(base64_decode($session->payload));
            $user_id = null;
            foreach ($payload as $key => $value) {
                if (strpos($key, 'login_') !== false) {
                    $user_id = $value;
                }
            }

            $sessions_array[] = (object) [
                'session_id'    => $session->id,
                'user_id'       => $user_id,
                'payload'       => unserialize(base64_decode($session->payload)),
                'last_activity' => Carbon::createFromTimestamp($session->last_activity),
                'user'          => User::find($user_id)
            ];
        }

        return view('user.activity', ['sessions'=>$sessions_array]);
    }
}
