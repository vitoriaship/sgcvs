<?php

namespace App\Http\Controllers;

use App\Jobs\SendBackupByEmail;
use Carbon\Carbon;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Storage;

class BackupController extends Controller
{
    public function index()
    {
        // validation: only users can be list backups
        if (Gate::denies('databaseBackup')) {
            return redirect(route('home'));
        }

        // diretorio com os arquivos dos backups
        $files = array_reverse(Storage::allFiles('database'));

        $arrayFiles = [];
        foreach ($files as $file) {
            $date = Carbon::createFromTimestamp(
                Storage::lastModified($file),
                config('app.timezone')
            )->toDateTimeString();

            $arrayFiles[] = (object) [
                'path' => $file,
                'name' => basename($file),
                'date' => $date,
                'size' => (int) (Storage::size($file) * 0.001)
            ];
        }

        return view('backup.index', ['files' => $arrayFiles]);
    }

    public function download($file)
    {
        $fullPath = sprintf('%s/app/%s', storage_path(), base64_decode($file));

        return response()->download($fullPath);
    }

    public function sendMail($file)
    {
        $fullPath = sprintf('%s/app/%s', storage_path(), base64_decode($file));

        try {
            dispatch(new SendBackupByEmail($fullPath));

            return redirect()->back()->with('status', 'E-mail sent with success.');
        } catch (\Exception $e) {
            return redirect()->back()->with('error', 'Something went wrong while trying send the e-mail.');
        }
    }
}
