<?php

namespace App\Http\Controllers;

use App\Models\Invoice;
use App\Models\Locale;
use App\Models\Rda;
use DB;
use Gate;
use Input;

class ReportController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (Gate::denies('report')) {
            return redirect(route('home'));
        }

        if (count(Input::all()) <= 1) {
            Input::merge([
                'year'  => date('Y'),
                'month' => date('n'),
            ]);
        }

        DB::connection()->enableQueryLog();

        // Get all the years stored
        $years = DB::table('dates')->distinct()
                    ->select(DB::raw('YEAR(supply) as year'))
                    ->where('supply', '>', 1900)
                    ->orderBy('supply', 'ASC')
                    ->get();

        // Get RDA's ordered by name
        $rdas = Rda::orderBy('name', 'ASC')->get();

        // Get Locales ordered by name
        $locales = Locale::orderBy('name', 'ASC')->get();

        // Creating the query based on user input choices
        $query = Invoice::query();
        $query->whereIn('stage', [2,3,4,5]);
        if (Input::has('rda') && Input::get('rda') != 'all') {
            $query->where('rda_id', Input::get('rda'));
        }

        if (Input::has('locale') && Input::get('locale') != 'all') {
            $query->where('locale_id', Input::get('locale'));
        }

        if (Input::has('year') && Input::get('year') != 'all') {
            $query->whereHas('date', function ($query) {
                $query->whereRaw('YEAR(supply) = '.Input::get('year'));
            });
        }

        if (Input::has('month') && Input::get('month') != 'all') {
            $query->whereHas('date', function ($query) {
                $query->whereRaw('MONTH(supply) = '.Input::get('month'));
            });
        }

        $invoices = $query->get();

        return view('report.index', compact('years', 'rdas', 'locales', 'invoices'));
    }
}
