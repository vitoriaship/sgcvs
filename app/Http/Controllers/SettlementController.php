<?php

namespace App\Http\Controllers;

use App\Models\Conclusion;
use App\Models\Invoice;
use App\Models\Locale;
use App\Models\Rda;
use App\Models\Vessel;
use App\Models\Port;
use DB;
use Gate;
use Input;

class SettlementController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (Gate::denies('settlement')) {
            return redirect(route('home'));
        }

        if (count(Input::all()) <= 1) {
            Input::merge([
                'year'  => date('Y'),
                'month' => date('n'),
            ]);
        }

        // Get all the years stored
        $years = DB::table('dates')->distinct()
                    ->select(DB::raw('YEAR(supply) as year'))
                    ->where('supply', '>', 1900)
                    ->orderBy('supply', 'ASC')
                    ->get();

        // Get RDA's ordered by name
        $rdas = Rda::orderBy('name', 'ASC')->get();

        // Get Vessles ordered by name
        $vessels = Vessel::orderBy('name', 'ASC')->get();

        // Get Vessles ordered by name
        $ports = Port::orderBy('name', 'ASC')->get();

        // Get Locales ordered by name
        $locales = Locale::orderBy('name', 'ASC')->get();

        // conclusions
        $conclusions = Conclusion::orderBy('id', 'asc')->get();

        // Creating the query based on user input choices
        $query = Invoice::query();

        // filtro por status
        switch (Input::get('status')) {
            case 'supplied':
                $query->whereIn('conclusion_id', [1]);
                $query->where('status_id', '=', 2);

                break;
            case 'not-supplied':
                $query->where('status_id', '=', 1);

                break;
            case 'paid':
                $query->whereIn('conclusion_id', [2, 3, 4]);

                break;
        }

        // filtro por customer
        if (Input::has('rda') && Input::get('rda') != 'all') {
            $rda_id = Input::get('rda');
            $query->where(function ($query) use ($rda_id) {
                $query->where('rda_id', $rda_id);
                $query->orWhere('corda_id', $rda_id);
            });
        }

        // filtro por Vessel
        if (Input::has('vessel') && Input::get('vessel') != 'all') {
            $vessel_id = Input::get('vessel');
            $query->where(function ($query) use ($vessel_id) {
                $query->where('vessel_id', $vessel_id);
            });
        }

        // filtro por locale
        if (Input::has('locale') && Input::get('locale') != 'all') {
            $query->where('locale_id', Input::get('locale'));
        }

        // filtro por ano
        if (Input::has('year') && Input::get('year') != 'all') {
            $query->whereHas('date', function ($query) {
                $query->whereRaw('YEAR(supply) = '.Input::get('year'));
            });
        }

        // filtro por mes
        if (Input::has('month') && Input::get('month') != 'all') {
            $query->whereHas('date', function ($query) {
                $query->whereRaw('MONTH(supply) = '.Input::get('month'));
            });
        }

        if (Input::has('number_nfe')) {
            $query->where('number', 'like', '%' . Input::get('number_nfe') . '%')
                ->orWhere('nfe', 'like', '%' . Input::get('number_nfe') . '%');
        }

        $invoices = $query->get();

        $data = compact('years', 'rdas', 'vessels', 'ports','locales', 'invoices', 'conclusions');

        return view('settlement.index', $data);
    }

    /**
     * Display all invoices.
     *
     * @return \Illuminate\Http\Response
     */
    public function all()
    {
        if (Gate::denies('settlement')) {
            return redirect(route('home'));
        }

        $invoices = Invoice::all();

        return view('settlement.index', compact('invoices'));
    }

    /**
     * Display all not supplied invoices.
     *
     * @return \Illuminate\Http\Response
     */
    public function notSupplied()
    {
        if (Gate::denies('settlement')) {
            return redirect(route('home'));
        }

        $invoices = Invoice::where('status_id', '=', 1)->get();

        return view('settlement.index', compact('invoices'));
    }

    /**
     * Display all supplied invoices.
     *
     * @return \Illuminate\Http\Response
     */
    public function supplied()
    {
        if (Gate::denies('settlement')) {
            return redirect(route('home'));
        }

        $invoices = Invoice::where('status_id', '=', 2)->get();

        return view('settlement.index', compact('invoices'));
    }

    /**
     * Display all paid invoices.
     *
     * @return \Illuminate\Http\Response
     */
    public function paid()
    {
        if (Gate::denies('settlement')) {
            return redirect(route('home'));
        }

        $invoices = Invoice::where('status_id', '=', 2)
            ->whereIn('conclusion_id', [2, 3, 4])
            ->get();

        return view('settlement.index', compact('invoices'));
    }
}
