<?php

namespace App\Http\Controllers;

use App\Models\Invoice;
use Gate;
use Vinkla\Hashids\Facades\Hashids;

class SuppliedController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (Gate::denies('supplied')) {
            return redirect(route('home'));
        }

        $invoices = Invoice::where('status_id', '=', 2)
            ->where('locale_id', '=', auth()->user()->locale->id)
            ->get();

        return view('supplied.index', compact('invoices'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int                       $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if (Gate::denies('supplied')) {
            return redirect(route('home'));
        }

        $invoice = Invoice::findOrFail(Hashids::decode($id)[0]);

        if ($invoice->status->id != 2) {
            return redirect()->back();
        }

        return view('supplied.show', compact('invoice'));
    }
}
