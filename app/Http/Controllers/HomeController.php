<?php

namespace App\Http\Controllers;

use App\Models;
use Carbon\Carbon;

class HomeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user   = auth()->user();
        $status = null;

        if (!isset($user->password_expires) || Carbon::now() >= $user->password_expires) {
            // se a senha "expirou", pedir p/ trocar a senha
            return redirect(route('user.editPassword'));
        } elseif ($user->group->id == 4) {
            return redirect(route('operational'));
        }

        // solicitação do lorran: notificar aos gerentes quando tiver usuários com senhas prestes à expirar
        // irei notificar 3 semanas antes
        if (in_array($user->group_id, [1,3])) {
            // é um gerente, então iremos verificar se tem usuários com senha expirando dentro de 3 semanas
            $total_users = Models\User::where('password_expires', '<=', Carbon::now()->addWeeks(3))->count();
            if ($total_users > 0) {
                $status = "{$total_users} users with passwords that will be expire. View in Records > Users'.";
            }
        }

        return redirect(route('invoice.index'))->with(compact('status'));
    }
}
