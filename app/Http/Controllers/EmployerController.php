<?php

namespace App\Http\Controllers;

use App\Http\Requests\Employer\StoreEmployerRequest;
use App\Models\Employer;
use Gate;

class EmployerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (Gate::denies('employer')) {
            return redirect(route('home'));
        }

        $employers = Employer::where('locale_id', '=', auth()->user()->locale_id)->get();

        return view('employer.index')->with('employers', $employers);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreEmployerRequest $request, Employer $employer)
    {
        if (Gate::denies('employer')) {
            return redirect(route('home'));
        }

        $employer->fill($request->only('name', 'cpf', 'rg', 'cellphone'));
        $employer->locale_id = auth()->user()->locale_id;
        $employer->save();

        return redirect(route('employer.index'))->with('status', 'Employer saved with success.');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int                       $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (Gate::denies('employer')) {
            return redirect(route('home'));
        }

        $employer = Employer::findOrFail($id);

        return view('employer.edit')->with('employer', $employer);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int                       $id
     * @return \Illuminate\Http\Response
     */
    public function update(StoreEmployerRequest $request, $id)
    {
        if (Gate::denies('employer')) {
            return redirect(route('home'));
        }

        $employer = Employer::findOrFail($id);
        $employer->fill($request->only('name', 'cpf', 'rg', 'cellphone'))->save();

        return redirect()->back()->with('status', 'Employer updated with success.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int                       $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (Gate::denies('employer')) {
            return redirect(route('home'));
        }

        $employer = Employer::findOrFail($id);
        $employer->delete();

        return redirect()->back()->with('status', 'Employer deleted with success.');
    }
}
