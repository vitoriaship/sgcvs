<?php

namespace App\Http\Controllers;

use App\Http\Requests\Approval\UpdateApprovalRequest;

use App\Models\Agency;
use App\Models\Conclusion;
use App\Models\Invoice;
use App\Models\Port;
use App\Models\Rda;
use App\Models\Status;
use App\Models\Vessel;
use Gate;
use Vinkla\Hashids\Facades\Hashids;

class ApprovalController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (Gate::denies('approval')) {
            return redirect(route('home'));
        }

        // Get all invoices to line-up approval
        $invoiceLs = Invoice::where('stage', '=', 2)
                            ->where('locale_id', '=', auth()->user()->locale_id)
                            ->get();

        // Get all invoices to Billing approval
        $invoiceBs = Invoice::where('stage', '=', 4)->get();

        return view('approval.index', compact('invoiceLs', 'invoiceBs'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int                       $id
     * @return \Illuminate\Http\Response
     */
    public function lineUp($id)
    {
        if (Gate::denies('approval')) {
            return redirect(route('home'));
        }

        $invoice = Invoice::findOrFail(Hashids::decode($id)[0]);

        if ($invoice->stage != 2) {
            return redirect()->back();
        }

        // Check if User locale is the same of the invoice
        if ($invoice->locale_id != auth()->user()->locale_id) {
            return redirect()->back();
        }

        $data = [
            'invoice'  => $invoice,
            'rdas'     => Rda::All()->sortBy('name'),
            'vessels'  => Vessel::All()->sortBy('name'),
            'agencies' => Agency::where('locale_id', '=', $invoice->locale_id)
                                ->orderBy('name')
                                ->get(),
            'ports'    => Port::where('locale_id', '=', $invoice->locale_id)
                              ->orderBy('name')
                              ->get(),
            'conclusions' => Conclusion::All(),
        ];

        return view('approval.line-up')->with($data);

        // solicitação do lorran, p/ padronizar os layouts
        // se invoice->rda->type == 0 (COB) já pode dar baixa diretamente, pula 1 etapa
        // então basta eu alterar apenas a view
        if ($invoice->rda->type == 0) {
            return view('billing.edit')->with($data);
        } else {
            return view('approval.line-up')->with($data);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int                       $id
     * @return \Illuminate\Http\Response
     */
    public function billing($id)
    {
        if (Gate::denies('approval')) {
            return redirect(route('home'));
        }

        $invoice = Invoice::findOrFail(Hashids::decode($id)[0]);

        // Send back if is wrong stage
        if ($invoice->stage != 4) {
            return redirect()->back();
        }

        $data = [
            'invoice'  => $invoice,
            'rdas'     => Rda::All()->sortBy('name'),
            'vessels'  => Vessel::All()->sortBy('name'),
            'agencies' => Agency::where('locale_id', '=', $invoice->locale_id)
                                ->orderBy('name')
                                ->get(),
            'ports'    => Port::where('locale_id', '=', $invoice->locale_id)
                               ->orderBy('name')
                               ->get(),
        ];

        return view('approval.billing')->with($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int                       $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateApprovalRequest $request, $id)
    {
        if (Gate::denies('approval')) {
            return redirect(route('home'));
        }

        $invoice = Invoice::findOrFail($id);
        $value   = $invoice->value;
        $date    = $invoice->date;

        // sempre vai salvar os dados
        $invoice->update($request->all());
        $value->update($request->all());
        $date->update($request->all());

        // altero o conclusion_id se for 2, 3 ou 4
        if (in_array($request->option, [2, 3, 4]) == true) {
            $invoice->conclusion_id = $request->option;
            $invoice->save();
        }

        // approval line-up
        if ($invoice->stage == 2) {
            switch ($request->option) {
                case 'x': // x - update
                    return redirect(route('approval.index'))->with('status', 'Invoice updated with success.');

                    break;
                case 'yes': // approved
                    $invoice->update([
                        'status_id' => 2, // supplied (pois pula processo)
                        'stage'     => 3
                    ]); // mando p/ billing
                    return redirect(route('approval.index'))
                        ->with('status', 'Invoice updated and send to billing with success.');

                    break;
                case 'no': // not approved
                    $invoice->update(['stage'=>1]); // mando p/ line-up
                    return redirect(route('approval.index'))
                        ->with('status', 'Invoice updated and send to line-up with success.');

                    break;
                case '2': //yes
                case '3': //yes with +
                case '4': //yes with -
                    $invoice->update([
                        'stage'     => 4, // mando p/ approval billing
                        'status_id' => 2 // supplied (pois pula processo)
                    ]);

                    return redirect(route('approval.index'))
                        ->with('status', 'Invoice updated and send to approval billing with success.');

                    break;
            }
        }

        // approval billing
        if ($invoice->stage == 4) {
            switch ($request->approval) {
                case 'yes': // approved
                    $invoice->update(['stage' => 5]); // mando p/ historic
                    return redirect(route('approval.index'))
                        ->with('status', 'Invoice updated and send to historic with success.');

                    break;
                case 'no': // not approved
                    $invoice->update(['stage' => 3]); // mando p/ billing
                    return redirect(route('approval.index'))
                        ->with('status', 'Invoice updated and send to billing with success.');

                    break;
                //case 'update': // apenas alterado
            }
        }

        return redirect(route('approval.index'))
            ->with('status', 'Invoice updated with success.');
    }
}
