<?php

namespace App\Http\Controllers;

use App\Models\Agency;
use App\Models\Employer;
use App\Models\Invoice;
use App\Models\Port;
use App\Models\Rda;
use App\Models\Status;
use App\Models\Vehicle;
use App\Models\Vessel;
use Gate;
use Vinkla\Hashids\Facades\Hashids;

class FinanceController extends Controller
{
    public function show($id)
    {
        if (Gate::denies('line-up')) {
            return redirect(route('home'));
        }

        $invoice = Invoice::findOrFail(Hashids::decode($id)[0]);

        if ($invoice->locale_id != auth()->user()->locale_id) {
            return redirect()->back();
        }

        $data = [
            'invoice'   => $invoice,
            'rdas'      => Rda::All()->sortBy('name'),
            'vessels'   => Vessel::All()->sortBy('name'),
            'agencies'  => Agency::where('locale_id', '=', auth()->user()->locale_id)
                ->orderBy('name', 'asc')
                ->get(),
            'ports'     => Port::where('locale_id', '=', auth()->user()->locale_id)
                ->orderBy('name', 'asc')
                ->get(),
            'vehicles'  => Vehicle::where('locale_id', '=', auth()->user()->locale_id)
                ->orderBy('plaque', 'asc')
                ->get(),
            'employers' => Employer::where('locale_id', '=', auth()->user()->locale_id)
                ->orderBy('name', 'asc')
                ->get(),
            'status'    => Status::All()->sortBy('name'),
        ];

        return view('finance.show')->with($data);
    }

    public function dates($id)
    {
        if (Gate::denies('line-up')) {
            return redirect(route('home'));
        }

        $invoice = Invoice::findOrFail(Hashids::decode($id)[0]);

        if ($invoice->locale_id != auth()->user()->locale_id) {
            return ['opperation not permited'];
        }

        return view('finance.dates')->with('invoice', $invoice);
    }

    public function datesUpdate($id, \Illuminate\Http\Request $request)
    {
        if (Gate::denies('line-up')) {
            return redirect(route('home'));
        }

        $invoice = Invoice::findOrFail($id);

        $invoice->remark->update($request->only(['billed']));
        $invoice->update($request->only(['nfe']));

        return redirect()->back()->with('status', 'Invoice updated with success.');
    }
}
