<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Models\Port;
use App\Models\State;
use Gate;

class PortController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (Gate::denies('port')) {
            return redirect(route('home'));
        }

        $ports  = Port::where('locale_id', '=', auth()->user()->locale_id)
                        ->orderBy('name', 'ASC')
                        ->paginate(50);

        $states = State::orderBy('abbr', 'ASC')->get();

        return view('port.index', compact('ports', 'states'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Requests\Port\StorePortRequest $request, Port $port)
    {
        if (Gate::denies('port')) {
            return redirect(route('home'));
        }

        $port->fill($request->only('name', 'state_id', 'stevedore', 'boat'));
        $port->locale_id = auth()->user()->locale_id;
        $port->save();

        return redirect(route('port.index'))->with('status', 'Port saved with success.');
        ;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int                       $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (Gate::denies('port')) {
            return redirect(route('home'));
        }

        $port   = Port::findOrFail($id);
        $states = State::orderBy('abbr', 'ASC')->get();

        return view('port.edit', compact('port', 'states'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int                       $id
     * @return \Illuminate\Http\Response
     */
    public function update(Requests\Port\StorePortRequest $request, $id)
    {
        if (Gate::denies('port')) {
            return redirect(route('home'));
        }

        $port = Port::findOrFail($id);

        $port->fill($request->only('name', 'state_id', 'stevedore', 'boat'))->save();

        return redirect()->back()->with('status', 'Port updated with success.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int                       $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Requests\Port\DeletePortRequest $request, $id)
    {
        if (Gate::denies('port')) {
            return redirect(route('home'));
        }

        $port = Port::findOrFail($id);

        $port->delete();

        return redirect()->back()->with('status', 'Port deleted with success.');
    }

    /**
     * Return if the port has Steavedore and/or Boat
     *
     * @param  int                       $id
     * @return \Illuminate\Http\Response
     */
    public function data($id)
    {
        if (!\Request::ajax()) {
            return redirect(route('home'));
        }

        $port = Port::findOrFail($id);
        $data = [
            'boat'      => $port->boat,
            'stevedore' => $port->stevedore,
        ];

        return $data;
    }
}
