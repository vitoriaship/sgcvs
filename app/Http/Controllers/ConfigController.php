<?php

namespace App\Http\Controllers;

use App\Models\Config;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;

class ConfigController extends Controller
{
    public function lineup()
    {
        if (Gate::denies('update-lineup-date')) {
            return redirect(route('home'));
        }

        // primeiro deleto registros antigos +7 dias
        Config::where('key', 'lineup-date')->whereRaw('created_at < DATE_SUB(NOW(), INTERVAL 7 DAY)')->delete();

        // pego a configuração
        $keyLineUpUpdatedIn = 'lineup-date-'.auth()->user()->locale_id;
        $config             = Config::where('key', $keyLineUpUpdatedIn)->orderBy('created_at', 'desc')->first();
        if (!$config) {
            $config = new Config();
        }

        return view('config.lineup', compact('config'));
    }

    public function lineupPost(Request $request, Config $config)
    {
        if (Gate::denies('update-lineup-date')) {
            return redirect(route('home'));
        }
        $keyLineUpUpdatedIn = 'lineup-date-'.auth()->user()->locale_id;

        $config->user_id = auth()->user()->id;
        $config->key     = $keyLineUpUpdatedIn;
        $data            = $request->date.' '.$request->time;
        $config->value   = Carbon::createFromFormat('d/m/Y H:i', $data)->format('Y-m-d H:i:s');
        $config->save();

        return redirect(route('config.lineup'))->with('status', 'Line-up date saved with success.');
    }
}
