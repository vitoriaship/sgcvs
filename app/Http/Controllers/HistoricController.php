<?php

namespace App\Http\Controllers;

use App\Models\Agency;

use App\Models\Conclusion;
use App\Models\Invoice;
use App\Models\Locale;
use App\Models\Port;
use App\Models\Rda;
use App\Models\Vessel;
use DB;
use Gate;
use Illuminate\Http\Request;
use Input;
use Vinkla\Hashids\Facades\Hashids;

class HistoricController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (Gate::denies('historic')) {
            return redirect(route('home'));
        }

        if (count(Input::all()) <= 1) {
            Input::merge([
                'year'  => date('Y'),
                'month' => date('n'),
            ]);
        }

        // Get all the years stored
        $years = DB::table('dates')
            ->distinct()
            ->select(DB::raw('YEAR(supply) as year'))
            ->where('supply', '>', 1900)
            ->orderBy('supply', 'ASC')
            ->get();

        // Get RDA's ordered by name
        $rdas = Rda::orderBy('name', 'ASC')->get();

        // Get Vessles ordered by name
        $vessels = Vessel::orderBy('name', 'ASC')->get();

        // Get Vessles ordered by name
        $ports = Port::orderBy('name', 'ASC')->get();

        // Get Locales ordered by name
        $locales = Locale::orderBy('name', 'ASC')->get();

        // Creating the query based on user input choices
        $query = Invoice::query();
        $query->where('stage', '=', 5);

        // Filters
        if (Input::has('rda') && Input::get('rda') != 'all') {
            $rda_id = Input::get('rda');
            $query->where('rda_id', $rda_id);
            $query->orWhere('corda_id', $rda_id);
        }

        if (Input::has('locale') && Input::get('locale') != 'all') {
            $query->where('locale_id', Input::get('locale'));
        }

        if (Input::has('year') && Input::get('year') != 'all') {
            $query->whereHas('date', function ($query) {
                $query->whereRaw('YEAR(supply) = '.Input::get('year'));
            });
        }

        if (Input::has('month') && Input::get('month') != 'all') {
            $query->whereHas('date', function ($query) {
                $query->whereRaw('MONTH(supply) = '.Input::get('month'));
            });
        }

        if (Input::has('vessel') && Input::get('vessel') != 'all') {
            $vessel_id = Input::get('vessel');
            $query->where(function ($query) use ($vessel_id) {
                $query->where('vessel_id', $vessel_id);
            });
        }

        if (Input::has('port') && Input::get('port') != 'all') {
            $port_id = Input::get('port');
            $query->where(function ($query) use ($port_id) {
                $query->where('port_id', $port_id);
            });
        }

        if (Input::has('number_nfe')) {
            $query->where('number', 'like', '%' . Input::get('number_nfe') . '%')
                ->orWhere('nfe', 'like', '%' . Input::get('number_nfe') . '%');
        }

        $invoices = $query->get();

        $data = compact('years', 'rdas', 'vessels', 'ports', 'locales', 'invoices');

        return view('historic.index', $data);
    }

    /**
     * Display the specified resource.
     *
     * @param  int                       $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if (Gate::denies('historic')) {
            return redirect(route('home'));
        }

        $invoice = Invoice::findOrFail(Hashids::decode($id)[0]);

        if ($invoice->stage != 5) {
            return redirect()->back();
        }

        return view('historic.show', compact('invoice'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int                       $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (Gate::denies('historic')) {
            return redirect(route('home'));
        }

        $invoice = Invoice::findOrFail(Hashids::decode($id)[0]);

        if ($invoice->stage != 5) {
            return redirect()->back();
        }

        if ($request->open) {
            $invoice->stage = 3;
            $invoice->update();

            $data = [
                'invoice'     => $invoice,
                'rdas'        => Rda::All()->sortBy('name'),
                'vessels'     => Vessel::All()->sortBy('name'),
                'agencies'    => Agency::All()->sortBy('name'),
                'ports'       => Port::All()->sortBy('name'),
                'conclusions' => Conclusion::All(),
            ];

            return redirect(route('billing.edit', $id))->with($data);
        }
    }

    public function commission(Request $request, $id)
    {
        if (Gate::denies('historic')) {
            return redirect(route('home'));
        }

        $invoice                         = Invoice::findOrFail($id);
        $invoice->value->commission_paid = $request->commission_paid;
        $invoice->value->save();

        return redirect()->back()->with('status', 'Commission paid updated with success.');
    }

    public function pendingCost($id)
    {
        if (Gate::denies('historic')) {
            return redirect(route('home'));
        }

        $invoice = Invoice::findOrFail(Hashids::decode($id)[0]);

        return view('historic.pending_cost', compact('invoice'));
    }
}
