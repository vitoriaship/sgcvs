<?php

namespace App\Http\Controllers;

use App\Events\InvoiceUpdated;
use App\Http\Requests\Invoice\DatesUpdateInvoiceRequest;
use App\Http\Requests\Invoice\DeleteInvoiceRequest;
use App\Http\Requests\Invoice\StoreInvoiceRequest;
use App\Http\Requests\Invoice\UpdateInvoiceRequest;
use App\Models\Agency;
use App\Models\Employer;
use App\Models\Invoice;
use App\Models\Locale;
use App\Models\Port;
use App\Models\Rda;
use App\Models\Status;
use App\Models\Vehicle;
use App\Models\Vessel;
use Gate;
use Illuminate\Support\Facades\DB;
use Vinkla\Hashids\Facades\Hashids;

class InvoiceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // restrição
        if (Gate::denies('line-up')) {
            return redirect(route('home'));
        }

        $invoices = Invoice::select([
                'invoices.*',
                DB::raw('CASE WHEN (UNIX_TIMESTAMP(now()) - UNIX_TIMESTAMP(invoices.created_at))/60 < 180 THEN 1 ELSE 0 END as novo')
            ])
            ->join('dates', 'dates.invoice_id', '=', 'invoices.id')
            ->where('stage', '=', 1)
            ->where('locale_id', '=', auth()->user()->locale_id)
            ->orderBy('novo', 'desc')
            ->orderBy('dates.etb', 'asc')
            ->get();

        // tem um line-up normal e um line-up resumido exibido aos usuários do grupo 'finances'
        if (auth()->user()->group_id != 5) {
            return view('invoice.index')->with(compact('invoices'));
        } else {
            return view('finance.index')->with(compact('invoices'));
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (Gate::denies('create-invoice')) {
            return redirect(route('home'));
        }

        $data = [
            'rdas'     => Rda::all()->sortBy('name'),
            'vessels'  => Vessel::all()->sortBy('name'),
            'agencies' => Agency::where('locale_id', '=', auth()->user()->locale_id)
                                  ->orderBy('name', 'asc')
                                  ->get(),
            'ports'    => Port::where('locale_id', '=', auth()->user()->locale_id)
                                  ->orderBy('name', 'asc')
                                  ->get(),
        ];

        return view('invoice.create')->with($data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreInvoiceRequest $request, Invoice $invoice)
    {
        if (Gate::denies('create-invoice')) {
            return redirect(route('home'));
        }

        $invoice->fill($request->all());

        // Getting user Id
        $invoice->user_id = auth()->user()->id;
        // Getting the locale of the invoice
        $invoice->locale_id = auth()->user()->locale_id;

        $invoice->save();
        $invoice->value()->create($request->all());
        $invoice->remark()->create($request->all());
        $invoice->date()->create($request->all());

        return redirect(route('invoice.index'))->with('status', 'Invoice created with success.');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int                       $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (Gate::denies('line-up')) {
            return redirect(route('home'));
        }

        $invoice = Invoice::findOrFail(Hashids::decode($id)[0]);

        if ($invoice->stage != 1 || $invoice->locale_id != auth()->user()->locale_id) {
            return redirect()->back();
        }

        $data = [
            'invoice'   => $invoice,
            'rdas'      => Rda::all()->sortBy('name'),
            'vessels'   => Vessel::all()->sortBy('name'),
            'agencies'  => Agency::where('locale_id', '=', auth()->user()->locale_id)
                                  ->orderBy('name', 'asc')
                                  ->get(),
            'ports'     => Port::where('locale_id', '=', auth()->user()->locale_id)
                                  ->orderBy('name', 'asc')
                                  ->get(),
            'vehicles'  => Vehicle::where('locale_id', '=', auth()->user()->locale_id)
                                  ->orderBy('plaque', 'asc')
                                  ->get(),
            'employers' => Employer::where('locale_id', '=', auth()->user()->locale_id)
                                  ->orderBy('name', 'asc')
                                  ->get(),
            'status'    => Status::all()->sortBy('name'),
        ];

        return view('invoice.edit')->with($data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int                       $id
     * @return \Illuminate\Http\Response
     */
    public function dates($id)
    {
        if (Gate::denies('line-up')) {
            return redirect(route('home'));
        }

        $invoice = Invoice::findOrFail(Hashids::decode($id)[0]);

        if ($invoice->stage != 1 || $invoice->locale_id != auth()->user()->locale_id) {
            return redirect()->back();
        }

        return view('invoice.dates')->with('invoice', $invoice);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int                       $id
     * @return \Illuminate\Http\Response
     */
    public function datesUpdate(DatesUpdateInvoiceRequest $request, $id)
    {
        if (Gate::denies('line-up')) {
            return redirect(route('home'));
        }

        $invoice = Invoice::findOrFail($id);

        $invoice->remark->update($request->only(['join_request','billed']));
        $invoice->date->update($request->all());
        $invoice->update($request->all());

        return redirect()->back()->with('status', 'Invoice updated with success.');
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int                       $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateInvoiceRequest $request, $id)
    {
        if (Gate::denies('line-up')) {
            return redirect(route('home'));
        }

        $invoice = Invoice::findOrFail($id);

        // If status_id 2 (supplied) or 3 (cancelled)
        // the stage is moved to next
        if (in_array($request->status_id, [2])) {
            $invoice->stage = 2; // approval line-up
        } elseif (in_array($request->status_id, [3])) {
            $invoice->stage = 5; // historic
        }

        // Updating
        $invoice->fill($request->all())->save();
        $invoice->value->fill($request->all())->save();
        $invoice->date->fill($request->all())->save();

        // Check if is a Commercial user to avoid the
        // Operacional Manager update an invoice and
        // setting null to all remarks.

        if (Gate::allows('create-invoice')) {
            $invoice->remark->fill($request->only([
                'nautical_chart',
                'join_request',
                'feedback',
                'medicines',
                'boat_vs',
                'stowage_vs',
                'boat_client',
                'stowage_client',
                'other',
                'billed'
            ]))->save();
        }

        // Refreshing the model
        $invoice = $invoice->fresh();

        // Send an e-mail if the Supply Dates was provided
        // and "Send E-mail Confirmation" is checked
        // se tiver supply date definido, e send email confirmation marcado, envia o e-ail
        if ($request->supply && $request->confirmation) {
            try {
                event(new InvoiceUpdated($invoice));
            } catch (Exception $e) {
                return redirect(route('invoice.index'))
                        ->with('error', 'Some error occured while try send the e-mail.');
            }
        }

        return redirect(route('invoice.index'))->with('status', 'Invoice update with success.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int                       $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(DeleteInvoiceRequest $request, $id)
    {
        if (Gate::denies('delete')) {
            return redirect(route('home'));
        }

        $invoice = Invoice::findOrFail(Hashids::decode($id)[0]);
        $invoice->value()->delete();
        $invoice->remark()->delete();
        $invoice->date()->delete();
        $invoice->delete();

        return redirect(route('invoice.index'))->with('status', 'Invoice deleted with success.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int                       $id
     * @return \Illuminate\Http\Response
     */
    public function duplicate($id)
    {
        if (Gate::denies('duplicate')) {
            return redirect(route('home'));
        }

        // Getting invoice and copying all data
        $invoice    = Invoice::findOrFail(Hashids::decode($id)[0]);
        $newInvoice = $invoice->replicate();

        // Saving
        $newInvoice->save();

        // Create relationships
        $newInvoice->value()->create($invoice->value->replicate()->toArray());
        $newInvoice->date()->create($invoice->date->replicate()->toArray());
        $newInvoice->remark()->create($invoice->remark->replicate()->toArray());

        return redirect(route('invoice.edit', Hashids::encode($newInvoice->id)));
    }

    public function locale($id)
    {
        if (Gate::denies('line-up')) {
            return redirect(route('home'));
        }

        $locales = Locale::orderBy('name', 'asc')->get();
        $invoice = Invoice::findOrFail(Hashids::decode($id)[0]);

        return view('invoice.locale', compact('invoice', 'locales'));
    }

    public function localeUpdate(\Illuminate\Http\Request $request, $id)
    {
        if (Gate::denies('line-up')) {
            return redirect(route('home'));
        }

        $invoice            = Invoice::findOrFail($id);
        $invoice->locale_id = $request->locale_id;
        $invoice->save();

        return redirect()->back()->with('status', 'Invoice updated with success.');
    }
}
