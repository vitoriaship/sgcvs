<?php

namespace App\Http\Controllers;

use App\Functions\Number;
use App\Http\Requests;
use App\Models\Locale;
use App\Models\Target;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Gate;

class TargetController extends Controller
{
    public function index()
    {
        if (Gate::denies('target')) {
            return redirect(route('home'));
        }

        $locales = Locale::all();

        $targets = Target::orderBy('year', 'desc')
            ->orderBy('month', 'desc')
            ->orderBy('locale_id', 'asc')
            ->paginate(50);

        return view('target.index')->with(compact('locales', 'targets'));
    }

    public function store(Requests\Target\TargetStoreRequest $request, Target $target)
    {
        if (Gate::denies('target')) {
            return redirect(route('home'));
        }

        $exists = Target::where('locale_id', $request->locale_id)
            ->where('year', $request->year)
            ->where('month', $request->month)
            ->first();
        if ($exists) {
            return redirect()->back()->withInput()->with('status', 'Target already exists');
        }

        $target->create($request->all());

        return redirect(route('target.index'))->with('status', 'Target saved with success.');
    }

    public function edit($id)
    {
        if (Gate::denies('target')) {
            return redirect(route('home'));
        }

        $locales = Locale::all();
        $target  = Target::findOrFail($id);

        $status = 'Target updated with success.';

        return view('target.edit')->with(compact('locales', 'target', 'status'));
    }

    public function update(Request $request, $id)
    {
        if (Gate::denies('target')) {
            return redirect(route('home'));
        }

        $target = Target::findOrFail($id);
        $target->fill($request->all())->save();

        return redirect()->back()->with('status', 'Target updated with success.');
    }

    public function destroy(Request $request, $id)
    {
        if (Gate::denies('target')) {
            return redirect(route('home'));
        }

        $target = Target::findOrFail($id);
        $target->delete();

        return redirect()->back()->with('status', 'Target deleted with success.');
    }

    public function show()
    {
        if (Gate::denies('target')) {
            return redirect(route('home'));
        }

        $inicio = Carbon::now()->subMonth(12)->startOfMonth();
        $fim    = Carbon::now()->endOfMonth();

        DB::connection()->enableQueryLog();

        $result = DB::select("
            select
                l.id as locale_id,
                l.name as locate,
                DATE_FORMAT(d.supply,'%m/%Y') as period,
                DATE_FORMAT(d.supply,'%b/%Y') as period2,
                t.value as target,
                sum(
                    (v.gross_1 * v.discount_1) +
                    (v.gross_2 * v.discount_2) +
                    v.stevedore_cost +
                    v.boat_cost +
                    v.transport +
                    v.wo+
                    v.extra_value
                ) as total_usd
            from `invoices` as i
            inner join `dates` as d on d.invoice_id=i.id
            inner join `values` as v on v.invoice_id=i.id
            inner join `locales` as l on l.id=i.locale_id
            left join `targets` as t
                on t.locale_id=l.id
                and t.`month` = month(d.supply)
                and t.`year` = year(d.supply)
            where
                i.stage in (2,3,4,5)
                and d.supply between ? and ?
            group by
                l.id,
                l.name,
                DATE_FORMAT(d.supply,'%m/%Y'),
                t.value
            having
                period is not null
            order by
                STR_TO_DATE(concat('01/', period), '%d/%m/%Y') asc
        ", [$inicio, $fim]);

        // labels - tenho que pegar os meses, mas sem repetir
        // vitoria - pego a meta e vlr apurado
        // rio de janeiro - pego a meta e vlr apurado
        $exists          = [];
        $labels          = [];
        $vitoria_targets = [];
        $vitoria_reports = [];
        $rio_targets     = [];
        $rio_reports     = [];
        foreach ($result as $line) {
            if (!isset($exists[$line->period2])) {
                $labels[]               = $line->period2;
                $exists[$line->period2] = true;
            }

            // vitoria
            if ($line->locale_id == 1) {
                $vitoria_targets[] = Number::getFormatNumber($line->target ?? 0);
                $vitoria_reports[] = Number::getFormatNumber($line->total_usd);
            } elseif ($line->locale_id == 2) { // rio de janeiro
                $rio_targets[] = Number::getFormatNumber($line->target ?? 0);
                $rio_reports[] = Number::getFormatNumber($line->total_usd);
            }
        }

        return view('target.chart', compact('labels', 'vitoria_targets', 'vitoria_reports', 'rio_targets', 'rio_reports'));
    }
}
