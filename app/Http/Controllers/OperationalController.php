<?php

namespace App\Http\Controllers;

use App\Models\Invoice;
use Illuminate\Support\Facades\DB;

class OperationalController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $invoices =  Invoice::join('vessels', 'invoices.vessel_id', '=', 'vessels.id')
            ->join('dates', 'invoices.id', '=', 'dates.id')
            ->select(['*', DB::raw('
                CASE
                    WHEN dates.supply IS NOT NULL THEN dates.supply
                    ELSE dates.estimated
                END as data
            ')])
            ->where('invoices.locale_id', '=', auth()->user()->locale_id)
            ->where('invoices.stage', '=', 1)
            ->where(function ($query) {
                $query->whereNotNull('dates.estimated')
                    ->orWhereNotNull('dates.supply');
            })
            ->orderBy('data', 'asc')
            ->orderBy('vessels.name', 'asc')
            ->get();

        return view('operational.index')->with('invoices', $invoices);
    }
}
