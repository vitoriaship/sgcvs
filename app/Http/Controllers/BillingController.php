<?php

namespace App\Http\Controllers;

use App\Models\Agency;
use App\Models\Conclusion;
use App\Models\Invoice;
use App\Models\Port;
use App\Models\Rda;
use App\Models\Vessel;
use Carbon\Carbon;
use Gate;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Vinkla\Hashids\Facades\Hashids;

class BillingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if (Gate::denies('billing')) {
            return redirect(route('home'));
        }

        // no billing, existe um filtro de quais dias existem cobranças
        // então irei agrupar as datas e a quantidade de cobranças
        // first-chat e duo-date são a mesma coisa
        $dates = DB::select('
            select
                case
                    when d.payment_date then d.payment_date
                    else DATE_ADD(d.supply, INTERVAL d.payment DAY)
                end as first_chat,
                count(*) as total
            from invoices as i
            inner join dates as d on d.invoice_id=i.id
            where
                i.stage in (2, 3, 4)
            group by
                first_chat
            order by 1 desc
        ');

        DB::connection()->enableQueryLog();

        $invoicesQuery = Invoice::with(['date'])
            ->where(function ($query) {
                $query->where('stage', '=', 3)// billing
                ->orWhere('stage', '=', 2)// approval line-up (solicitação lorran)
                ->orWhere('stage', '=', 4); // approval billing (solicitação lorran)
            });

        // filtro da data
        if (!isset($request->date)) {
            $request->date = Carbon::now()->format('Y-m-d');
        }
        if ($request->date != 'all') {
            $invoicesQuery->whereRaw('exists(select 1 from dates where invoice_id = invoices.id and (case when dates.payment_date then dates.payment_date else DATE_ADD(dates.supply, INTERVAL dates.payment DAY) end) = ?)', [$request->date]);
        }
        $invoices = $invoicesQuery->get();

        $querys = DB::getQueryLog();

        return view('billing.index', compact('dates', 'invoices', 'request'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int                       $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (Gate::denies('billing')) {
            return redirect(route('home'));
        }

        $invoice = Invoice::findOrFail(Hashids::decode($id)[0]);

        $data = [
            'invoice'     => $invoice,
            'rdas'        => Rda::All()->sortBy('name'),
            'vessels'     => Vessel::All()->sortBy('name'),
            'agencies'    => Agency::where('locale_id', '=', $invoice->locale_id)->orderBy('name')->get(),
            'ports'       => Port::where('locale_id', '=', $invoice->locale_id)->orderBy('name')->get(),
            'conclusions' => Conclusion::All(),
        ];

        return view('billing.edit')->with($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int                       $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (Gate::denies('billing')) {
            return redirect(route('home'));
        }

        $invoice = Invoice::findOrFail($id);
        $value   = $invoice->value;
        $date    = $invoice->date;
        $remark  = $invoice->remark;

        if ($invoice->stage == 3 and $request->conclusion_id != 1) {
            $invoice->stage = 4;
        } elseif ($invoice->stage == 2 and $invoice->rda->type == 0 and $request->conclusion_id != 1) {
            $invoice->stage = 4;
        }

        $invoice->update($request->all());
        $value->update($request->all());
        $date->update($request->all());
        $remark->update($request->only([
            'nautical_chart', 'join_request', 'feedback',
            'medicines', 'boat_vs', 'stowage_vs',
            'boat_client', 'stowage_client', 'other'
        ]));

        return redirect(route('billing.index'))->with('status', 'Invoice updated with success.');
    }

    public function dates($id)
    {
        if (Gate::denies('billing')) {
            return redirect(route('home'));
        }

        $invoice = Invoice::findOrFail(Hashids::decode($id)[0]);

        return view('billing.dates')->with('invoice', $invoice);
    }

    public function datesUpdate(Request $request, $id)
    {
        if (Gate::denies('billing')) {
            return redirect(route('home'));
        }

        $invoice = Invoice::findOrFail($id);
        $invoice->value->update($request->only(['payment_remarks']));
        $invoice->date->update($request->only(['first_charge','second_charge','third_charge']));

        return redirect()->back()->with('status', 'Invoice updated with success.');
    }
}
