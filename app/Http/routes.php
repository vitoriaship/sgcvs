<?php

// Home
Route::get('/', 'HomeController@index')->name('home');

// Invoice CRUD
Route::resource('/invoice', 'InvoiceController');
Route::get('/invoice/date/{id}', 'InvoiceController@dates')->name('invoice.dates');
Route::put('/invoice/date/{id}', 'InvoiceController@datesUpdate')->name('invoice.datesUpdate');
Route::get('/invoice/locale/{id}', 'InvoiceController@locale')->name('invoice.locale');
Route::post('/invoice/locale/{id}', 'InvoiceController@localeUpdate')->name('invoice.localeUpdate');
Route::post('/invoice/duplicate/{id}', 'InvoiceController@duplicate')->name('invoice.duplicate');

// invoice - finance
Route::get('/finance/show/{id}', 'FinanceController@show')->name('finance.show');
Route::get('/finance/date/{id}', 'FinanceController@dates')->name('finance.dates');
Route::put('/finance/date/{id}', 'FinanceController@datesUpdate')->name('finance.datesUpdate');

// Authentication routes
Route::get('login', 'Auth\AuthController@getLogin')->name('auth.login');
Route::post('login', 'Auth\AuthController@postLogin')->name('auth.login');
Route::get('logout', 'Auth\AuthController@getLogout')->name('auth.logout');

// Password reset link request routes.
Route::get('password/email', 'Auth\PasswordController@getEmail')->name('password.email');
Route::post('password/email', 'Auth\PasswordController@postEmail')->name('password.email');

// Password reset routes
Route::get('password/reset/{token}', 'Auth\PasswordController@getReset')->name('reset');
Route::post('password/reset', 'Auth\PasswordController@postReset')->name('reset');

// Vessels routes
Route::resource('vessel', 'VesselController');

// RDAs routes
Route::resource('rda', 'RdaController');
Route::get('rda/json/{id}', 'RdaController@json')->name('rda.json');

// Targets routes
Route::resource('target', 'TargetController');
Route::get('target/chart', 'TargetController@chart')->name('target.chart');

// Agencies routes
Route::resource('agency', 'AgencyController');

// Ports routes
Route::resource('port', 'PortController');
Route::get('port/data/{id}', 'PortController@data')->name('port.data');

// Employers routes
Route::resource('employer', 'EmployerController');

// Vehicles routes
Route::resource('vehicle', 'VehicleController');

// Users routes
Route::get('user/password', 'UserController@editPassword')->name('user.editPassword');
Route::post('user/password', 'UserController@updatePassword')->name('user.updatePassword');
Route::get('user/activity', 'UserController@activity')->name('user.activity');
Route::get('user/registerActivity', 'UserController@registerActivity')->name('user.registerActivity');
Route::resource('user', 'UserController');

// Approval routes
Route::resource('approval', 'ApprovalController');
Route::get('approval/line-up/{id}', 'ApprovalController@lineUp')->name('approval.lineUp');
Route::get('approval/billing/{id}', 'ApprovalController@billing')->name('approval.billing');

// Billing routes
Route::resource('billing', 'BillingController');
Route::get('/billing/date/{id}', 'BillingController@dates')->name('billing.dates');
Route::put('/billing/date/{id}', 'BillingController@datesUpdate')->name('billing.datesUpdate');

// Historic routes
Route::resource('historic', 'HistoricController');
Route::get('/historic/pendingCost/{id}', 'HistoricController@pendingCost')->name('historic.pendingCost');
Route::post('/historic/commission/{id}', 'HistoricController@commission')->name('historic.commission');

// Supplied routes
Route::resource('supplied', 'SuppliedController');

// Settlement routes
Route::get('settlement', 'SettlementController@index')->name('settlement');

// Report routes
Route::get('report', 'ReportController@index')->name('report');

// Operational list routes
Route::get('programacao', 'OperationalController@index')->name('operational');

// Database backups route
Route::get('backup', 'BackupController@index')->name('backup');
Route::get('backup/{file}', 'BackupController@download')->name('backup.download');
Route::get('backup/mail/{file}', 'BackupController@sendMail')->name('backup.mail');

// alter line-up date
Route::get('config/lineup', 'ConfigController@lineup')->name('config.lineup');
Route::post('config/lineup', 'ConfigController@lineupPost')->name('config.lineupPost');
