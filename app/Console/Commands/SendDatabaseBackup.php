<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Storage;
use Mail;

class SendDatabaseBackup extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'backup:send';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send Database Backup by E-mail';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $files = array_reverse(Storage::allFiles('database'));
        if ($files) {
            $file = sprintf('%s/app/%s', storage_path(), $files[0]);

            Mail::send('emails.backup', ['file' => $file], function ($message) use ($file) {
                $message->from('backup@vitoriaship.com.br', 'Weekly Database Backup');
                $message->to('vitoriaship@vitoriaship.com.br', 'Vitoria Ship Supplier');
                $message->subject('Database Backup ' . date('Y-m-d', time()));
                $message->attach($file);
            });
        }
    }
}
