<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\View\Composers;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        // messages.blade.php
        $this->app['view']->composer('messages', Composers\AddStatusMessage::class);

        // navbar.blade.php
        $this->app['view']->composer('navbar', Composers\TotalApprovalInvoices::class);

        // line up lasted updated
        $this->app['view']->composer('lineup-last-updated', Composers\LineUpLastedUpdated::class);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
