<?php

namespace App\Providers;

use Illuminate\Contracts\Auth\Access\Gate as GateContract;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * Register any application authentication / authorization services.
     *
     * @param  \Illuminate\Contracts\Auth\Access\Gate  $gate
     * @return void
     */
    public function boot(GateContract $gate)
    {
        /**
         * Access Type and groups
         * - 1 Commercial Manager
         * - 2 Commercial
         * - 3 Commercial Management
         * - 4 Operacional
         * - 5 Finances
         */
        $permissions = [
            'line-up'        => [1, 2, 3, 5],
            'update-lineup-date' => [1, 2, 3],

            'create-invoice' => [1, 2, 3],

            'records'        => [1, 2, 3, 5],
            'delete'         => [1, 2],
            'duplicate'      => [1, 2],

            'approval'       => [1],
            'billing'        => [1, 2],
            'settlement'     => [1, 2],
            'historic'       => [1, 2],
            'supplied'       => [2, 3, 5], // commercial management não precisa ver supplied, telefone 28/07/17
            'report'         => [1, 2],
            'user'           => [1, 2],
            'vessel'         => [1, 2],
            'rda'            => [1, 2],
            'port'           => [1, 2, 3, 5],
            'agency'         => [1, 2, 3, 5],
            'employer'       => [1, 2, 3, 5],
            'vehicle'        => [1, 2, 3, 5],
            'databaseBackup' => [1],
            'target'         => [1, 2],
        ];

        parent::registerPolicies($gate);

        foreach ($permissions as $module => $permission) {
            $gate->define($module, function ($user) use ($permission) {
                return in_array(auth()->user()->group->id, $permission);
            });
        }

    }
}
