# Projecto SGCVS Vitória Ship Supplier

_Desenvolvido com Laravel 5.1_

### Docker Setup
- [Docker para Mac](https://docs.docker.com/docker-for-mac/)
- [Docker para Linux](https://docs.docker.com/engine/installation/linux/)
- [Docker para Windows](https://docs.docker.com/docker-for-windows/)

### Rodando o projeto
Com Docker instalado rode o comando abaixo:
```bash
docker-compose up -d
```

### Configure as variáveis de ambiente
Crie um arquivo `.env` na raiz com o conteúdo
_Obs: Substitua as variáveis como driver de e-mail e etc..._
```
APP_ENV=local
APP_DEBUG=true
APP_URL=http://localhost
APP_KEY=APaXExYrzF33KPTBJkEeroYG1IOvbxtV

DB_CONNECTION=mysql
DB_HOST=vitoriaship-db
DB_PORT=3306
DB_DATABASE=homestead
DB_USERNAME=homestead
DB_PASSWORD=secret

BROADCAST_DRIVER=log
CACHE_DRIVER=redis
SESSION_DRIVER=redis
QUEUE_DRIVER=sync

REDIS_HOST=vitoriaship-redis
REDIS_PASSWORD=null
REDIS_PORT=6379

MAIL_DRIVER=smtp
MAIL_HOST=mailtrap.io
MAIL_PORT=2525
MAIL_ENCRYPTION=tls
MAIL_USERNAME=f075005e4b51ae
MAIL_PASSWORD=eb68efc8be80dc

MAILGUN_DOMAIN=mg.petronetto.com.br
MAILGUN_KEY=key-95935d3a589c994688548d017515b4c4
MAIL_ADDRESS=petronetto@xdevel.com.br
MAIL_NAME=Petronetto Development
```

### Instalando as dependências
```bash
docker exec -it vitoriaship-php composer install
```

### Rodando as migrations
```bash
docker exec -it vitoriaship-php php artisan migrate --seed
```

Acesse: http://localhost:8888