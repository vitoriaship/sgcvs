FROM alpine:3.8

LABEL maintainer="Juliano Petronetto <juliano@petronetto.com.br>"

ENV TZ="America/Sao_Paulo" \
# PHP errors config
    PHP_DISPLAY_ERRORS="Off" \
    PHP_DISPLAY_STARTUP_ERRORS="Off" \
    PHP_ERROR_REPORTING="E_ALL & ~E_DEPRECATED & ~E_STRICT" \
    PHP_CGI_FIX_PATHINFO=0

COPY ./docker/php /
COPY ./ /app

RUN set -ex; \
    apk --update upgrade --no-cache; \
    apk add --no-cache \
        tzdata \
        nginx \
        curl \
        bash \
        vim \
        ca-certificates \
        openssl \
        supervisor \
        php7 \
        php7-cgi \
        php7-ctype \
        php7-curl \
        php7-dom \
        php7-fileinfo \
        php7-fpm \
        php7-json \
        php7-mbstring \
        php7-mcrypt \
        php7-opcache \
        php7-openssl \
        php7-pdo \
        php7-pdo_mysql \
        php7-pdo_sqlite \
        php7-phar \
        php7-session \
        php7-tokenizer \
        php7-xdebug \
        php7-zlib; \
    rm -rf /var/cache/apk/*; \
    curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/bin --filename=composer; \
    adduser -D -u 1000 -g 'www' www; \
    chown -R www:www /var/lib/nginx; \
    rm -rf /app/docker; \
    chmod +x /start.sh; \
# Fixing permissions
    chown -Rf www:www /app;

WORKDIR /app

ENTRYPOINT ["sh"]

# Start Supervisord
CMD ["/start.sh"]
