<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table("users")->insert([
            [
                'name'           => 'Petronetto',
                'username'       => 'petronetto',
                'email'          => 'juliano.petronetto@gmail.com',
                'password'       => bcrypt('secret'),
                'remember_token' => '',
                'locale_id'      => 1,
                'group_id'       => 1
            ],
            [
                'name'           => 'Commercial',
                'username'       => 'commercial',
                'email'          => 'commercial@vitoriaship.com',
                'password'       => bcrypt('secret'),
                'remember_token' => '',
                'locale_id'      => 1,
                'group_id'       => 2
            ],
            [
                'name'           => 'Operational Manager',
                'username'       => 'moperational',
                'email'          => 'moperational@vitoriaship.com',
                'password'       => bcrypt('secret'),
                'remember_token' => '',
                'locale_id'      => 2,
                'group_id'       => 3
            ],
            [
                'name'           => 'Operational',
                'username'       => 'operational',
                'email'          => 'operational@vitoriaship.com',
                'password'       => bcrypt('secret'),
                'remember_token' => '',
                'locale_id'      => 2,
                'group_id'       => 4
            ]
        ]);

        $this->command->info('Table users seeded!');
    }
}
