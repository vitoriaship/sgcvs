<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class AgencyTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
    	foreach (range(1,10) as $index) {
            DB::table("agencies")->insert([
                [
                    'name'          =>  $faker->lastName,
                    'responsible'   =>  $faker->name,
                    'telephone'     =>  '('.$faker->numberBetween($min = 11, $max = 99).') '
                                        .$faker->numberBetween($min = 1111, $max = 9999).'-'
                                        .$faker->numberBetween($min = 1111, $max = 9999),
                    'cellphone'     =>  '('.$faker->numberBetween($min = 11, $max = 99).') '
                                        .$faker->numberBetween($min = 11111, $max = 99999).'-'
                                        .$faker->numberBetween($min = 1111, $max = 9999),
                    'locale_id'     =>    rand(1,2)
                ]
            ]);
        }

        $this->command->info('Table agencies seeded!');
    }
}
