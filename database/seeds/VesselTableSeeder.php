<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class VesselTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
    	foreach (range(1,10) as $index) {
            DB::table("vessels")->insert([
                [
                    'name'  => $faker->name,
                    'email' => 'juliano.petronetto@gmail.com'
                ]
            ]);
        }

        $this->command->info('Table vessels seeded!');
    }
}
