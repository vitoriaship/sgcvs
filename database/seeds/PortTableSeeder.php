<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class PortTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
    	foreach (range(1,10) as $index) {
            DB::table("ports")->insert([
                [
                    'name'      => $faker->lastName,
                    'stevedore' => $faker->boolean,
                    'boat'      => $faker->boolean,
                    'state_id'  => $faker->numberBetween($min = 1, $max = 27),
                    'locale_id' => rand(1,2)
                ]
            ]);
        }

        $this->command->info('Table ports seeded!');
    }
}
