<?php

use Illuminate\Database\Seeder;

class StateTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table("states")->insert([
            [
                "name" => "Rondônia",
                "abbr" => "RO"
            ],
            [
                "name" => "Acre",
                "abbr" => "AC"
            ],
            [
                "name" => "Amazonas",
                "abbr" => "AM"
            ],
            [
                "name" => "Roraima",
                "abbr" => "RR"
            ],
            [
                "name" => "Pará",
                "abbr" => "PA"
            ],
            [
                "name" => "Amapá",
                "abbr" => "AP"
            ],
            [
                "name" => "Tocantins",
                "abbr" => "TO"
            ],
            [
                "name" => "Maranhão",
                "abbr" => "MA"
            ],
            [
                "name" => "Piauí",
                "abbr" => "PI"
            ],
            [
                "name" => "Ceará",
                "abbr" => "CE"
            ],
            [
                "name" => "Rio Grande do Norte",
                "abbr" => "RN"
            ],
            [
                "name" => "Paraíba",
                "abbr" => "PB"
            ],
            [
                "name" => "Pernambuco",
                "abbr" => "PE"
            ],
            [
                "name" => "Alagoas",
                "abbr" => "AL"
            ],
            [
                "name" => "Sergipe",
                "abbr" => "SE"
            ],
            [
                "name" => "Bahia",
                "abbr" => "BA"
            ],
            [
                "name" => "Minas Gerais",
                "abbr" => "MG"
            ],
            [
                "name" => "Espírito Santo",
                "abbr" => "ES"
            ],
            [
                "name" => "Rio de Janeiro",
                "abbr" => "RJ"
            ],
            [
                "name" => "São Paulo",
                "abbr" => "SP"
            ],
            [
                "name" => "Paraná",
                "abbr" => "PR"
            ],
            [
                "name" => "Santa Catarina",
                "abbr" => "SC"
            ],
            [
                "name" => "Rio Grande do Sul",
                "abbr" => "RS"
            ],
            [
                "name" => "Mato Grosso do Sul",
                "abbr" => "MS"
            ],
            [
                "name" => "Mato Grosso",
                "abbr" => "MT"
            ],
            [
                "name" => "Goiás",
                "abbr" => "GO"
            ],
            [
                "name" => "Distrito Federal",
                "abbr" => "DF"
            ],
        ]);

		$this->command->info('Table state seeded!');
    }
}
