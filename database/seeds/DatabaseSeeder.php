<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        $this->call(StateTableSeeder::class);
        $this->call(GroupTableSeeder::class);
        $this->call(LocaleTableSeeder::class);
        $this->call(UserTableSeeder::class);
        $this->call(AgencyTableSeeder::class);
        $this->call(EmployerTableSeeder::class);
        $this->call(PortTableSeeder::class);
        $this->call(RdaTableSeeder::class);
        $this->call(VehicleTableSeeder::class);
        $this->call(VesselTableSeeder::class);
        $this->call(ConclusionTableSeeder::class);
        $this->call(StatusTableSeeder::class);
        $this->call(InvoiceTableSeeder::class);

        Model::reguard();
    }
}
