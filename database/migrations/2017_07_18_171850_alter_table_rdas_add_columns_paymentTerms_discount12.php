<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableRdasAddColumnsPaymentTermsDiscount12 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('rdas', function (Blueprint $table) {
            // valores opcionais para facilitar inserção em "new"
            $table->string('payment_terms', 255)->nullable();
            $table->float('discount_1', 4, 2)->nullable();
            $table->float('discount_2', 4, 2)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('rdas', function (Blueprint $table) {
            //
            $table->dropColumn('payment_terms');
            $table->dropColumn('discount_1');
            $table->dropColumn('discount_2');
        });
    }
}
