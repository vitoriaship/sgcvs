<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableVehiclesAddLocaleIdColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('vehicles', function (Blueprint $table) {
            $table->unsignedInteger('locale_id')->before('created_at');
            // $table->foreign('locale_id', 'fk_vehicle_locale')
            //       ->references('id')
            //       ->on('locales')
            //       ->onUpdate('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('vehicles', function (Blueprint $table) {
            $table->dropColumn('locale_id');
            //$table->dropForeign('fk_vehicle_locale');
        });
    }
}
