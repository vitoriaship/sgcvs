<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateDatesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('dates', function(Blueprint $table)
		{
			$table->increments('id');
			$table->unsignedInteger('invoice_id');
			$table->date('eta');
			$table->date('etb')->nullable();
			$table->date('ets')->nullable();
			$table->date('supply')->nullable();
			$table->date('estimated')->nullable();
			$table->date('first_charge')->nullable();
			$table->date('second_charge')->nullable();
			$table->date('third_charge')->nullable();
			$table->date('payment_date')->nullable();
			$table->string('hour', 5)->nullable();
			$table->integer('payment')->unsigned();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('dates');
	}

}
