<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableValuesAddExtraValues extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('values', function (Blueprint $table) {
            //
            $table->boolean('extra_value_bool')->default(0);
            $table->string('extra_value_label')->nullable();
            $table->float('extra_value', 10, 2)->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('values', function (Blueprint $table) {
            //
            $table->dropColumn(['extra_value_bool','extra_value_label','extra_value']);
        });
    }
}
