<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToRemarksTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('remarks', function(Blueprint $table)
		{
			$table->foreign('invoice_id', 'fk_remarks_invoice')->references('id')->on('invoices')->onUpdate('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('remarks', function(Blueprint $table)
		{
			$table->dropForeign('fk_remarks_invoice');
		});
	}

}
