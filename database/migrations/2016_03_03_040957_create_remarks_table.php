<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateRemarksTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('remarks', function(Blueprint $table)
		{
			$table->increments('id');
			$table->unsignedInteger('invoice_id');
			$table->boolean('nautical_chart')->default(0);
			$table->boolean('join_request')->default(0);
			$table->boolean('feedback')->default(0);
			$table->boolean('medicines')->default(0);
			$table->boolean('boat_vs')->default(0);
			$table->boolean('stowage_vs')->default(0);
			$table->boolean('boat_client')->default(0);
			$table->boolean('stowage_client')->default(0);
			$table->string('other')->nullable();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('remarks');
	}

}
