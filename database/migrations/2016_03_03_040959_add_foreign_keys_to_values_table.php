<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToValuesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('values', function(Blueprint $table)
		{
			$table->foreign('invoice_id', 'fk_values_invoice')->references('id')->on('invoices')->onUpdate('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('values', function(Blueprint $table)
		{
			$table->dropForeign('fk_values_invoice');
		});
	}

}
